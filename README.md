#堀江薬局　2017.07.11  
#静的モック用リポジトリ  
■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■  
  
・スマホファーストのレスポンシブです  
・sassとimgをgulpで監視しています  
・各ページJSON-LDでのパンくず構造化対応をお願いします    
・title、description、OGP等のmetaタグやcanonicalタグは、コンテンツリストをご確認いただき設定をお願いします  
・動的ページはWordPressでの構築を想定してコーディングしてください（CMS導入予定ページについてはコンテンツリストをご確認ください）  
  
  
---
##Gitについて  
Bitbucketに専用のリポジトリを作成しましたのでクローンをお願いいたします。  
https://bitbucket.org/di_kusano/funin-kanpo/  
  
・ブランチはdevelopをご使用ください  
・弊社と同時並行での作業になりますので、既存ファイルを編集する際は事前にチャットワークでご連絡ください  
  
  
---
##gulpについて  
※監視開始のコマンドが「gulp watch」→「gulp」に変更になりました。  
※cssプロパティの並び替えが保存と同時に実行されるようになりました。  
※browserSyncでlocalhostを起動するのでMAMP等でサーバーを立ち上げてください。  
  
  
[repository]：ローカルリポジトリ  
    │  
    ├[public]：吐き出し先用ディレクトリ  
    │    ├[common]  
    │    │    ├[css]：resourceディレクトリで作業したファイルがここに吐き出されます  
    │    │    ├[img]：resourceディレクトリで作業したファイルがここに吐き出されます  
    │    │    └[inc]  
    │    │    └[js]  
    │    │  
    │    └index.html  
    │  
    ├[map]：ソースマップディレクトリ  
    │  
    ├[node_modules]：プラグインディレクトリ  
    │  
    ├[resource]：gulp監視対象用ディレクトリ  
    │    ├[img]  
    │    └[sass]  
    │          ├[setting]  
    │          │    ├cmn_layoutのパーシャルファイル  
    │          │    ├_mixin.scss  
    │          │    └_variable.scss  
    │          │  
    │          ├cmn_layout.scss  
    │          ├cmn_style.scss  
    │          └import.scss  
    │  
    ├[temporary]：一時置き場用ディレクトリ（sassファイル並べ替え用）  
    │    └[sass]  
    │          ├[setting]  
    │          │    ├cmn_layoutのパーシャルファイル  
    │          │    ├_mixin.scss  
    │          │    └_variable.scss  
    │          │  
    │          ├cmn_layout.scss  
    │          ├cmn_style.scss  
    │          └import.scss  
    │  
    ├.csscomb.json：プロパティの並び順やインデントなどの指示  
    ├.gitignore：gitにコミットしないファイルを記載  
    ├gulpfile.js：このファイル  
    └package.json：プラグインインストールの指示  
  
  
  
---  
##publicディレクトリマップ  
[public]  
    │  
    ├index.php：トップページ  
    │  
    ├[common]  
    │    ├[css]  
    │    ├[img]  
    │    ├[inc]  
    │    │  ├custom.php：サイト共通で使用する変数の設定  
    │    │  ├header.php：グローバルナビゲーション  
    │    │  ├footer.php：フッター  
    │    │  ├head_analytics.php：head内に記述する解析タグ  
    │    │  ├foot_analytics.php：</body>前に記述する解析タグ  
    │    │  └js.php：サイト共通で使用するjs  
    │    │  
    │    └[js]  
    │  
    └[info]  
         ├index.php：一覧  
         ├index_2.php：一覧2ページ目  
         ├index_3.php：一覧3ページ目  
         └yyyyMMddHHmmss.php：記事  
  
  
---  
##画像について    
・共通画像は「/resource/img/」内に格納してください。  
・各ページ用画像は「/resource/img/ディレクトリ名/」内に格納してください。  
・「/resource/img/upload/」内の画像はWPでの投稿が想定される仮画像です。実際の出力パスはWPの仕様に則ったパスになります。  
  
  
---  
##CSSについて  
・各ページ用CSSは「/resource/ディレクトリ名.scss」に記述してください。  
・共通パーツのCSS「/resource/sass/cmn_layout.scss」（＋setting内の各ファイル）を編集する際は事前にご一報ください。  
　※共通パーツの一覧は以下をご確認ください  
　http://funin-kanpo.bizproject.biz/parts.php  
　funin / kanpo  
  
  
  
  
ご不明点等ありましたらチャットワークにて萩原宛てにご連絡ください。