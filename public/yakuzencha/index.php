<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>ご縁授茶｜堀江薬局の漢方・薬膳茶｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="堀江薬局の漢方・薬膳茶（ご縁授茶）をご紹介しています。ご縁授茶は「血」を大切にする婦人科漢方理論に基づいた漢方・薬膳茶で、非常に高品質な和漢植物を6種類もブレンドしています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/yakuzencha/">
<meta property="og:type" content="article">
<meta property="og:title" content="ご縁授茶｜堀江薬局の漢方・薬膳茶｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="堀江薬局の漢方・薬膳茶（ご縁授茶）をご紹介しています。ご縁授茶は「血」を大切にする婦人科漢方理論に基づいた漢方・薬膳茶で、非常に高品質な和漢植物を6種類もブレンドしています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/yakuzencha/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/yakuzencha.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "ご縁授茶｜堀江薬局の漢方・薬膳茶"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g01">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li>漢方・薬膳茶</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
		<div class="h1_basic01 inner01 no_border">
			<h1>漢方・薬膳茶</h1>
			<span>Japanese traditional ・ Medicinal herb tea</span>
		<!-- /.h1_basic01 --></div>

		<div class="yakuzencha_mainimg01">
			<section class="inner01">
				<h2><img src="/common/img/yakuzencha/mainimg01.png" alt="漢方・薬膳茶「ご縁授茶（ごえんさずかりちゃ）」 「血流がすべて解決する」著者 堀江昭佳 プロデュース" width="1047" height="494"></h2>
			</section>
		<!-- /.about_mainimg01 --></div>

		<div class="anchor01">
			<ul>
				<li><a href="#concept">コンセプト</a></li>
				<li><a href="#goods">商品紹介</a></li>
				<li><a href="#worried">こんな方へ</a></li>
				<li><a href="#voice">お客様の声</a></li>
				<li><a href="#price">料金・お申し込み</a></li>
			</ul>
		<!-- /.anchor01 --></div>

		<section id="concept">
			<div class="h2_basic01">
				<h2>コンセプト</h2>
				<span>Concept</span>
			<!-- /.h2_basic01 --></div>
			<div class="inner02">
				<div class="lead_concept01">
					<p>「血」を大切にする<br>婦人科漢方理論に基づいた<br>漢方・薬膳茶</p>
				<!-- /.lead_concept01 --></div>
				<div class="txt_concept01">
					<div class="img"><img src="/common/img/yakuzencha/img_concept01.png" alt="" width="220" height="220"></div>
					<div class="txt">
						<p>全国からたくさんの女性のかたがさまざまな相談に訪れられます。年間5千件を超えるご予約のカウンセリングを受ける中で、女性がしあわせであるためには、“からだ”がすこやかであり、“こころ”がやすらかであること。そして、そのためには、ポカポカが欠かせないことです。</p>
						<p>忙しい女性の方が、日々の暮らしの中で簡単に取り入れることができて、続けやすいものがあるといい。そのために試作を重ね、苦労の末に作り上げたブレンドが、手軽に続けられてぽっかぽかになれるご縁授茶です。あなたにも幸せのご縁がとどきますように。</p>
					<!-- /.txt --></div>
				<!-- /.txt_concept01 --></div>
			<!-- /.inner02 --></div>
		</section>

		<section id="goods">
			<div class="h2_basic01">
				<h2>商品紹介</h2>
				<span>Goen-sazukari-cya</span>
			<!-- /.h2_basic01 --></div>
			<div class="inner01">
				<p><img src="/common/img/yakuzencha/img_goods01.png" alt="漢方・薬膳茶「ご縁授茶（ごえんさずかりちゃ）」 「紅茶」ベース・「三年番茶」b−ス" width="898" height="378"></p>
			<!-- /.inner01 --></div>
			<div class="inner02">
				<section class="sec_goods01">
					<div class="tit">
						<h3>調合へのこだわり</h3>
					<!-- /.tit --></div>
					<div class="txt">
						<p>多くの女性の悩みを知る婦人科専門薬局の調合力を集結しました。</p>
						<p>あたたかさとめぐりを高めることで、“こころ”と“からだ”のバランスをとります。</p>
						<p>年間5000件もの予約カウンセリングを行う婦人科専門の漢方薬局だからこそ、和漢植物の特性を活かす調合が可能なのです。</p>
					<!-- /.txt --></div>
				</section>

				<section class="sec_goods01">
					<div class="tit">
						<h3>素材へのこだわり</h3>
					<!-- /.tit --></div>
					<div class="txt">
						<p>使用している和漢植物は、漢方薬局だから手に入れられる生薬メーカーへの特注品です。非常に高品質なものを6種類ブレンドしています。</p>
						<dl class="dl_basic01">
							<dt>6種の和漢植物</dt>
							<dd>
								<div>
									<div class="img"><img src="/common/img/yakuzencha/img_goods_material01.png" alt="リコリスの写真" width="100" height="100"></div>
									<dl>
										<dt>リコリス</dt>
										<dd>東洋でも西洋でも使われているハーブ。甘みがあり、和らげ、ときほぐします。</dd>
									</dl>
								</div>
							</dd>
							<dd>
								<div>
									<div class="img"><img src="/common/img/yakuzencha/img_goods_material02.png" alt="なつめの写真" width="100" height="100"></div>
									<dl>
										<dt>なつめ</dt>
										<dd>古来より五果といい、薬膳で大切にされてきた5つの果物の1つです。「1日3粒食べれば年を取らない」ということわざがあるほどエイジングケアにも。</dd>
									</dl>
								</div>
							</dd>
							<dd>
								<div>
									<div class="img"><img src="/common/img/yakuzencha/img_goods_material03.png" alt="アンジェリカの写真" width="100" height="100"></div>
									<dl>
										<dt>アンジェリカ</dt>
										<dd>ヨーロッパでは「天使のハーブ」と呼ばれ、女性の悩みの解消に用いられます。毎月訪れる“こころ”とからだのゆらぎにバランスを整えます。</dd>
									</dl>
								</div>
							</dd>
							<dd>
								<div>
									<div class="img"><img src="/common/img/yakuzencha/img_goods_material04.png" alt="ニッキの写真" width="100" height="100"></div>
									<dl>
										<dt>ニッキ</dt>
										<dd>独特の甘みと香りがあり、ポカポカうれしい和漢植物です。停滞しているものを動かし発散させる働きがあります。</dd>
									</dl>
								</div>
							</dd>
							<dd>
								<div>
									<div class="img"><img src="/common/img/yakuzencha/img_goods_material05.png" alt="グローブの写真" width="100" height="100"></div>
									<dl>
										<dt>グローブ</dt>
										<dd>エキゾチックでスパイシーな香りがあります。停滞しているものを動かしたり、発散させてくれます。</dd>
									</dl>
								</div>
							</dd>
							<dd>
								<div>
									<div class="img"><img src="/common/img/yakuzencha/img_goods_material06.png" alt="出雲産 漢方生姜の写真" width="100" height="100"></div>
									<dl>
										<dt>出雲産 漢方生姜</dt>
										<dd>ご縁授茶で使用する生姜は、出雲産で日本国内でもわずか数％しか生産されていない、希少な生姜を中“こころ”にブレンドして使用しています。主となる出雲生姜の持つあたため力「ジンゲロール」は一般的な生姜（大生姜）の倍以上。さらに生姜を一度蒸してから乾燥・発酵させることで、イキイキに繋がる力が普通の倍以上に引き上がります。</dd>
									</dl>
								</div>
							</dd>
						</dl>
					<!-- /.txt --></div>
				</section>

				<section class="sec_goods01">
					<div class="tit">
						<h3>味へのこだわり</h3>
					<!-- /.tit --></div>
					<div class="txt">
						<p>「苦いお茶」なんてとても続きません！<br>毎日続けられる自然な甘さにこだわりました。 6種類の和漢植物と出雲市を中“こころ”に島根県内で育てられた国産茶葉のハーモニーをお味わいください。</p>
						<p>ニッキの香りもふんわりと香り、とっても穏やかな気持ちになれます。</p>
						<dl class="dl_basic01">
							<dt>選べる2種類の茶葉</dt>
							<dd>
								<div>
									<div class="img"><img src="/common/img/yakuzencha/img_goods_taste01.png" alt="出雲地方の国産紅茶の写真" width="100" height="100"></div>
									<dl>
										<dt>出雲地方の国産紅茶</dt>
										<dd>出雲地方で愛情いっぱいに育てられ渋みが少なくやさしい味わいが特徴です。</dd>
									</dl>
								</div>
							</dd>
							<dd>
								<div>
									<div class="img"><img src="/common/img/yakuzencha/img_goods_taste02.png" alt="奈良県大和高原の三年番茶の写真" width="100" height="100"></div>
									<dl>
										<dt>奈良県大和高原の三年番茶</dt>
										<dd>自然農法で育てられ、丁寧に焙じられたのち熟成された三年番茶です。</dd>
									</dl>
								</div>
							</dd>
						</dl>
					<!-- /.txt --></div>
				</section>

				<section class="sec_goods01">
					<div class="tit">
						<h3>手軽さへのこだわり</h3>
					<!-- /.tit --></div>
					<div class="txt">
						<p>ティーパックにお湯を注ぐだけ。<br>三角リーフティーバッグを採用しているので茶葉が広がりやすく、抽出が早いので茶葉本来の味と香りが十分に引き出されます。</p>
					<!-- /.txt --></div>
				</section>

				<section class="sec_goods01">
					<div class="tit">
						<h3>ご縁を願うこだわり</h3>
					<!-- /.tit --></div>
					<div class="txt">
						<p>縁結びの神様 出雲大社の表参道・神門通りにある 堀江薬局からお届けします。<br>縁結びのシンボルである赤い絹の水引を結んでいます。結び方は花結び、「昔から子宝に恵まれ何度でも子を産めることを願う」という意味があります。ひとつひとつ丁寧に願いを込めて結んでお届けします。</p>
					<!-- /.txt --></div>
				</section>

				<div class="txt_important01">
					<p>多くの女性が幸せになってもらえるよう、<br class="pc">高品質なものだけを厳選して使用しています。</p>
				<!-- /.txt_important01 --></div>
			<!-- /.inner02 --></div>
		</section>

		<section id="worried">
			<div class="h2_basic01">
				<h2>こんなお悩みのある方へ</h2>
				<span>Worried about something</span>
			<!-- /.h2_basic01 --></div>
			<div class="worried_inner01">
				<div class="inner01">
					<ul>
						<li>“からだ”が冷えている</li>
						<li>毎月の女性のリズムが不安定</li>
						<li>自信が無く落ち込みやすい</li>
						<li>余裕が無く疲れている</li>
						<li>笑顔が減った気がする</li>
					</ul>
					<div class="txt">
						<p>実は、これらの悩みはひとつながり…</p>
						<p class="txt_lilne01"><span>原因はポカポカする実感がないこと。</span></p>
						<p>さらに毎月の女性のリズムが整わず、<br class="pc">それが他のお悩みを引き起こす原因に。</p>
					<!-- /.txt --></div>
				<!-- /.inner01 --></div>
			<!-- /.worried_inner01 --></div>
			<div class="worried_inner02">
				<div class="inner01">
					<p>漢方・薬膳茶の「<ruby>ご縁授茶<rt>ごえんさずかりちゃ</rt></ruby>」で<br>“こころ”と“からだ”を<br>ポカポカにしましょう。</p>
					<p><img src="/common/img/yakuzencha/img_warried01.png" alt="" width="551" height="356"></p>
				<!-- /.inner01 --></div>
			<!-- /.worried_inner02 --></div>

			<div class="txt_important01">
				<div class="inner01">
					<p>婦人科専門の漢方薬局代表である堀江昭佳が<br class="pc">こだわったのはめぐる力をサポートし、<br class="pc">毎月の女性のリズムを整えることで<br class="pc">“こころ”も“からだ”も元気になることを目指しました。</p>
				<!-- /.inner01 --></div>
			<!-- /.txt_important01 --></div>

		</section>

		<section id="voice">
			<div class="h2_basic01">
				<h2>お客様の声</h2>
				<span>Voice</span>
			<!-- /.h2_basic01 --></div>
			<div class="inner01">
				<section class="voice_inner01">
					<div class="img"><img src="/common/img/yakuzencha/img_voice01.png" alt="" width="244" height="236"></div>
					<div class="txt">
						<h3>2袋飲み終わる頃、授かることができました！</h3>
						<div class="data">30歳｜東京都｜Ｈ.Ｍ様</div>
						<p class="sub_tit">赤ちゃんを授かりたく、色々なことを行ったりサプリメントを飲んでました。</p>
						<p>知人より「出雲大社の近くに子宝のお茶が売ってるよ！」と教えていただき、インターネットで見つけて購入できました★とても美味しく、ほっとする味です。</p>
					<!-- /.txt --></div>
				</section>
				<section class="voice_inner01">
					<div class="img"><img src="/common/img/yakuzencha/img_voice01.png" alt="" width="244" height="236"></div>
					<div class="txt">
						<h3>2袋飲み終わる頃、授かることができました！</h3>
						<div class="data">30歳｜東京都｜Ｈ.Ｍ様</div>
						<p class="sub_tit">赤ちゃんを授かりたく、色々なことを行ったりサプリメントを飲んでました。</p>
						<p>知人より「出雲大社の近くに子宝のお茶が売ってるよ！」と教えていただき、インターネットで見つけて購入できました★とても美味しく、ほっとする味です。</p>
					<!-- /.txt --></div>
				</section>
				<section class="voice_inner01">
					<div class="img"><img src="/common/img/yakuzencha/img_voice01.png" alt="" width="244" height="236"></div>
					<div class="txt">
						<h3>2袋飲み終わる頃、授かることができました！</h3>
						<div class="data">30歳｜東京都｜Ｈ.Ｍ様</div>
						<p class="sub_tit">赤ちゃんを授かりたく、色々なことを行ったりサプリメントを飲んでました。</p>
						<p>知人より「出雲大社の近くに子宝のお茶が売ってるよ！」と教えていただき、インターネットで見つけて購入できました★とても美味しく、ほっとする味です。</p>
					<!-- /.txt --></div>
				</section>
				<p class="note01">※お客様個人の感想であり、効果効能を保証するものではありません</p>

				<div class="txt_important01">
					<p>あなたも体験してみませんか？</p>
				<!-- /.txt_important01 --></div>
			<!-- /.inner01 --></div>
		</section>

		<section id="price">
			<div class="h2_basic01">
				<h2>料金・お申し込み</h2>
				<span>Price</span>
			<!-- /.h2_basic01 --></div>
			<div class="price_single01">
				<div class="inner01">
					<div class="price_inner01">
						<div class="img">
							<p><img src="/common/img/yakuzencha/img_price_single01.png" alt="まずは1ヶ月分 「紅茶」ベースご縁緑茶・「三年番茶」ベースご縁緑茶" width="408" height="331"></p>
						<!-- /.img --></div>
						<div class="txt">
							<p><img src="/common/img/yakuzencha/txt_price_single01.png" alt="「紅茶」または「三年番茶」ベース ご縁授茶 30包入り 単品 4,500円（税抜） ゆうメール送料無料" width="380" height="199"></p>
						<!-- /.txt --></div>
					<!-- /.price_inner01 --></div>

					<div class="btn_price01">
						<form method="post" action="https://ssl.087biz.com/horie/shop/products/detail.php?product_id=122">
							<input type="hidden" name="quantity" value="1" />
							<input type="hidden" name="mode" value="cart" />
							<input type="hidden" name="product_id" value="122" />
							<input type="hidden" name="product_class_id" value="130" />
							<button type="submit" formtarget="_blank"><img src="/common/img/yakuzencha/btn_price_single01.png" alt="「紅茶」ベース 単品のお申し込みはこちら" width="440" height="106"></button>
						</form>
					<!-- /.btn_single01 --></div>

					<div class="btn_price02">
						<form method="post" action="https://ssl.087biz.com/horie/shop/products/detail.php?product_id=5">
							<input type="hidden" name="quantity" value="1" />
							<input type="hidden" name="mode" value="cart" />
							<input type="hidden" name="product_id" value="5" />
							<input type="hidden" name="product_class_id" value="13" />
							<button type="submit" formtarget="_blank"><img src="/common/img/yakuzencha/btn_price_single02.png" alt="「三年番茶」ベース 単品のお申し込みはこちら" width="440" height="106"></button>
						</form>
					<!-- /.btn_single02 --></div>
				<!-- /.inner01 --></div>
			<!-- /.price_single01 --></div>

			<div class="price_regular01">
				<div class="inner01">
					<div class="price_inner01">
						<div class="img">
							<p><img src="/common/img/yakuzencha/img_price_regular01.png" alt="毎日継続したい方へ 「紅茶」ベースご縁緑茶・「三年番茶」ベースご縁緑茶" width="408" height="333"></p>
						<!-- /.img --></div>
						<div class="txt">
							<p><img src="/common/img/yakuzencha/txt_price_regular01.png" alt="「紅茶」または「三年番茶」ベース ご縁授茶 30包をお得に継続 初回のみ4,500円（税抜） 2回目以降ずーっと520円OFF 3,980円（税抜） ゆうメール送料無料" width="380" height="261"></p>
						<!-- /.txt --></div>
					<!-- /.price_inner01 --></div>

					<div class="btn_price01">
						<form method="post" action="https://ssl.087biz.com/horie/shop/products/detail.php?product_id=121">
							<input type="hidden" name="quantity" value="1" />
							<input type="hidden" name="mode" value="cart" />
							<input type="hidden" name="product_id" value="121" />
							<input type="hidden" name="product_class_id" value="129" />
							<button type="submit" formtarget="_blank"><img src="/common/img/yakuzencha/btn_price_regular01.png" alt="「紅茶」ベース お得な継続コースのお申し込みはこちら" width="440" height="106"></button>
						</form>
					<!-- /.btn_regular01 --></div>

					<div class="btn_price02">
						<form method="post" action="https://ssl.087biz.com/horie/shop/products/detail.php?product_id=9">
							<input type="hidden" name="quantity" value="1" />
							<input type="hidden" name="mode" value="cart" />
							<input type="hidden" name="product_id" value="9" />
							<input type="hidden" name="product_class_id" value="17" />
							<button type="submit" formtarget="_blank"><img src="/common/img/yakuzencha/btn_price_regular02.png" alt="「三年番茶」ベース お得な継続コースのお申し込みはこちら" width="440" height="106"></button>
						</form>
					<!-- /.btn_regular02 --></div>
				<!-- /.inner01 --></div>
			<!-- /.price_regulare01 --></div>

			<div class="price_trial01">
				<div class="inner01">
					<div class="price_inner01">
						<div class="img">
							<p><img src="/common/img/yakuzencha/img_price_trial01.png" alt="15日間お試し！ 「紅茶」ベースご縁緑茶・「三年番茶」ベースご縁緑茶" width="408" height="340"></p>
						<!-- /.img --></div>
						<div class="txt">
							<p><img src="/common/img/yakuzencha/txt_price_trial01.png" alt="「紅茶」または「三年番茶」ベース ご縁授茶 15包入り トライアル 2,500円（税抜） ゆうメール送料無料" width="380" height="204"></p>
						<!-- /.txt --></div>
					<!-- /.price_inner01 --></div>

					<div class="btn_price01">
						<form method="post" action="https://ssl.087biz.com/horie/shop/products/detail.php?product_id=123">
							<input type="hidden" name="quantity" value="1" />
							<input type="hidden" name="mode" value="cart" />
							<input type="hidden" name="product_id" value="123" />
							<input type="hidden" name="product_class_id" value="131" />
							<button type="submit" formtarget="_blank"><img src="/common/img/yakuzencha/btn_price_trial01.png" alt="「紅茶」ベース まずは15日間試しのお申し込みはこちら" width="440" height="106"></button>
						</form>
					<!-- /.btn_trial01 --></div>

					<div class="btn_price02">
						<form method="post" action="https://ssl.087biz.com/horie/shop/products/detail.php?product_id=4">
							<input type="hidden" name="quantity" value="1" />
							<input type="hidden" name="mode" value="cart" />
							<input type="hidden" name="product_id" value="4" />
							<input type="hidden" name="product_class_id" value="12" />
							<button type="submit" formtarget="_blank"><img src="/common/img/yakuzencha/btn_price_trial02.png" alt="「三年番茶」ベース まずは15日間試しのお申し込みはこちら" width="440" height="106"></button>
						</form>
					<!-- /.btn_trial02 --></div>
				<!-- /.inner01 --></div>
			<!-- /.price_triale01 --></div>
		</section>

		<section id="delivery">
			<div class="inner02">
				<h2>ご連絡事項</h2>
				<dl class="dl_basic01">
					<dt>送料：</dt>
					<dd>ゆうメールのお届けで送料無料です。<br>佐川急便でお届けの場合送料540円がかかります。</dd>

					<dt>お支払い：</dt>
					<dd>クレジットカード、NP後払い<br>NP後払いの場合は、後払い手数料205円がかかります。<br>NP後払いの場合は、商品とは別に請求書・払込票が発送されますので、到着後14日以内にお支払下さい。</dd>

					<dt>お届け：</dt>
					<dd>ご注文を受け付けてから7日前後といたします。</dd>

					<dt>返品・解約：</dt>
					<dd>納品日より7日以内。<br>食料品という商品の性質上、お客さまのご都合による返品・交換はお断りさせていただきます。<br>定期購入を解約される場合は、次回商品発送の10日前までに、電話・メールにてお問い合わせをお願い致します。</dd>
				</dl>
			<!-- /.inner02 --></div>
		</section>

		<div id="cv_fix" class="cv_fix_yakuzencha">
			<div class="cv_fix_inner01">
				<div class="txt">
					<img src="/common/img/yakuzencha/cv_txt01.png" alt="堀江昭佳プロデュース 「ご縁授茶（ごえんさずかりちゃ）」" width="365" height="76">
				<!-- /.txt --></div>
				<div class="btn">
					<a href="#price"><img src="/common/img/yakuzencha/cv_btn01.png" alt="料金・お申し込みはこちら" width="300" height="56"></a>
				<!-- /.img --></div>
			<!-- /.cv_fix_inner01 --></div>
		<!-- /.cv_fix01 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
