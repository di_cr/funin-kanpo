<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>個人情報の保護について｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="個人情報保護についてのページです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/privacy/">
<meta property="og:type" content="article">
<meta property="og:title" content="個人情報の保護について｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="個人情報保護についてのページです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/privacy/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/com.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "個人情報の保護について"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li>個人情報の保護について</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
		<div class="h1_basic01 inner01">
			<h1>個人情報の保護について</h1>
			<span>Privacy</span>
		<!-- /.h1_basic01 --></div>

		<div class="inner02">
			<p>不妊漢方.com（堀江薬局）は、お客様の個人情報の取り扱いには万全の注意を払っており、お客様の同意無しには、決して社外にもらすことはございません。<br>このプライバシーポリシーは「不妊漢方.com」の利用者に対する、プライバシーポリシー保護のために行っている内容を開示しています。</p>
			<ul class="list_basic01">
				<li>ユーザーの特定を目的として収集される個人情報について</li>
				<li>ユーザー情報の受け取りに及び管理について</li>
				<li>ユーザー情報の利用について</li>
				<li>第三者とのユーザー情報の共有について</li>
				<li>情報法の収集・使用・配布に関するユーザーのオプト・インについて</li>
				<li>当サイトにおける、情報の消失・乱用・改ざんに対する対策について</li>
				<li>ユーザーが可能な登録情報の修正について</li>
			</ul>

			<section>
				<div class="h2_basic02">
					<h2>メールアドレス登録と確認のお願い</h2>
				<!-- /.h2_basic03 --></div>
				<p>お客様が間違ったメールアドレスを登録されたことにより、弊社からの発送案内や重要連絡・解答が届かないことにより、お客様の要望にこたえられず、発送に支障がでた場合でも、弊社はその責を負いません。正しいアドレスをお知らせいただくようにご注意くださるようお願いいたします。<br>また、携帯メールアドレスの場合は、情報が途中で切れますが、ご了承ください。</p>
			</section>

			<section>
				<div class="h2_basic02">
					<h2>お客様の声に関する情報収集について</h2>
				<!-- /.h2_basic03 --></div>
				<p>eメールによるお客様の声投稿では、商品の感想および今後の商品改善、弊社の対応改善に関する情報を募るために収集を行っております。これらの情報は、プライベートな部分をすべて除いたものを、ご本人様より許可をいただいた上で、サイトページ上にて公開をさせていただきます。これらの情報を無断で二次利用したりすることは一切ありません。</p>
				<div class="list_basic02">
					<dl>
						<dt>収集情報</dt>
						<dd>
							<ul>
								<li>氏名（ご本人様の許可があれば、公開いたします）</li>
								<li>メールアドレス（非公開）</li>
								<li>電話番号（非公開）</li>
								<li>ニックネーム（ご本人様の許可があれば、公開いたします）</li>
								<li>商品の感想（ご本人様の許可があれば、公開いたします）</li>
							</ul>
						</dd>
					</dl>
				</div>
			</section>

			<section>
				<div class="h2_basic02">
					<h2>お問い合わせ送信フォーム</h2>
				<!-- /.h2_basic03 --></div>
				<p>不妊漢方.comは、「お問い合わせ送信フォーム」を使ってご質問あるいはご意見を受け、直接お答えいたしております。お客様からのメッセージ全てに目を通し、できるだけ２営業日以内にお返事さしあげるようにいたしております。このような形でいただいた情報はご質問、ご意見に直接お答えするために使われます。任意でいただいておりますEメールアドレスを別のマーケティング目的で使用することはありません。</p>
			</section>

			<section>
				<div class="h2_basic02">
					<h2>Cookie</h2>
				<!-- /.h2_basic03 --></div>
				<p>不妊漢方.comは、自社および第三者によるクッキーを閲覧利用者様ＰＣに送付し、Ｗｅｂサイトの利用状況の把握・分析等のために使用しますが、これはどれもお客様個人を識別するためのものではありません。なお、クッキーの関知や使用を許可するかどうかは、お客様のwebブラウザで設定できます。必要に応じて設定をご確認ください。</p>
			</section>

			<section>
				<div class="h2_basic02">
					<h2>ログ解析</h2>
				<!-- /.h2_basic03 --></div>
				<p>不妊漢方.comは、当サイトの業務状況を見るためにアクセスログを収集しています。これはお買い物傾向の分析、サイトの管理､検索キーワード、お客様の動向の探知をすることにより、よりユーザビリティの高い店舗設計をする目的であり、決して個人情報を収集し特定する目的ではありません。また、同様の目的で第三者のプログラムを利用し、アクセス解析を行っていますが、ここでも個人情報を収集したり特定することはありません。 </p>
			</section>

			<section>
				<div class="h2_basic02">
					<h2>連絡先</h2>
				<!-- /.h2_basic03 --></div>
				<p>プライバシー保護に関するご質問やご意見がございましたら、こちらよりご連絡くださるようお願いいたします。 </p>
				<p>有限会社堀江薬局<br>〒699-0711　島根県出雲市大社町杵築南1370-2<br>メールアドレス：&#105;&#110;&#102;o&#64;&#102;&#117;&#110;&#105;&#110;-ka&#110;p&#111;.com<br>TEL：0853-25-8704</p>
			</section>
		<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<script type="text/javascript" src="/common/js/map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCe-6qErsU9FfCecbcK6rgpd8Et5UyfS4A&callback=initMap1"></script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
