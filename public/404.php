<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>お探しのページは見つかりませんでした。｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="お探しのページは見つかりませんでした。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/404.php">
<meta property="og:type" content="article">
<meta property="og:title" content="お探しのページは見つかりませんでした。｜堀江薬局の漢方・薬膳茶｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="お探しのページは見つかりませんでした。">
<meta property="og:site_name" content="お探しのページは見つかりませんでした。｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/404.php/">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/yakuzencha.css" media="all">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g01">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="contents" class="inner02">
		<main id="main" role="main">
			<div class="h2_basic01" style="margin-top:60px;">
				<h2>お探しのページは見つかりませんでした。</h2>
				<span>404 Not found</span>
			<!-- /.h2_basic01 --></div>
			<div class="btn_basic02">
				<p class="mb60"><img src="/common/img/ill_notfound_01.png" width="238" height="272"></p>
				<a href="/">トップページへ戻る</a>
			<!-- /.btn_basic02 --></div>
		</main>
	<!-- /#contents --></div>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
