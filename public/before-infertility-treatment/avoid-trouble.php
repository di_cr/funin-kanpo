<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>婦人科トラブルを治すための生活習慣｜不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="「婦人科トラブルを治すための生活習慣」のページです。堀江薬局ではカウンセリングをもとに、体質に合わせた漢方やサプリメント、食事・生活の養生法により、「妊娠しやすい体作り」をサポートしています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/before-infertility-treatment/avoid-trouble.php">
<meta property="og:type" content="article">
<meta property="og:title" content="婦人科トラブルを治すための生活習慣｜不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="「婦人科トラブルを治すための生活習慣」のページです。堀江薬局ではカウンセリングをもとに、体質に合わせた漢方やサプリメント、食事・生活の養生法により、「妊娠しやすい体作り」をサポートしています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/before-infertility-treatment/avoid-trouble.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "不妊治療を始める前に"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "婦人科トラブルを治すための生活習慣"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g03">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/before-infertility-treatment/">不妊治療を始める前に</a></li>
			<li>婦人科トラブルを治すための生活習慣</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1>食事・生活の養生法により、<br class="pc">婦人科トラブルを減らそう</h1>
		<!-- /.h1_basic01 --></div>

		<div id="avoid-trouble">
			<p>現代の不妊治療は、以前では考えられないような進歩を遂げたすばらしい医療技術です。<br>でも、あなたの体は妊娠する準備ができていますか？<br><span class="gray">妊娠する力、妊娠力が弱っている今のままの体の状態では、あかちゃんを授かることが難しいのです。</span></p>
			<p><span class="yellow">体の内側に目を向けてください。</span><br>まずは、基本的な体づくりをしていかなければ、いくら高度な治療を受けたとしても、失敗を繰り返すことになりかねないんです。</p>
			<p>私は、おひとりおひとりのお話をうかがい、カウンセリングをもとに、体質に合わせた漢方やサプリメント、そして食事・生活の養生法により、婦人科トラブルを減らし、「妊娠しやすい体作り」をサポートしています。</p>
			<p>女性が本来持っているからだの力を取り戻すお手伝いをする方法なので、余計な負担をかけず、今までよりも良い状態に体を導きます。</p>
			<p>不妊の原因がわからない方、<br>病院で治療を受けている方、<br>子宮内膜症をお持ちの方など<br>たくさんの方々の妊娠しやすい体作りをお手伝いしてきました。<br>今まで体外受精をなんどやってもうまくいかなかったのに、きちんと体づくりをしたら妊娠したというケースも多くあります。</p>
			<p>妊娠の経過も順調で<br>生まれた赤ちゃんもとっても元気で<br>産後もおっぱいがたくさん出る。<br>そんなお話を聞くたびに、うれしくなります。</p>
			<p>人間の体ってすごいです。<br>体が変わってくるとトラブルも減って、どんどん健康になっていかれます。</p>
			<p>でも、誤解してほしくないことがあります。<br>漢方やサプリメントを飲めばそれでいい。<br>決してそうではありません。</p>
			<p>生活の仕方、食事、考え方・・・<br>ひとつひとつにきちんと取り組んでいかなければ、体づくりはうまくいかないのです。<br>そして、体づくりをしたからといって、確実に妊娠できるというわけでもありません。<br>しかし、妊娠しなかった場合でも、体調は今より良くなるのは確かです。<br>女性の体のリズムを整える方法として、不妊治療のみならず、将来の妊娠のため、あるいは今後の婦人病予防や更年期を快適に乗り切るためも役立ちます。</p>
			<p>「こころ」と「からだ」がすこやかであること。自分らしく笑顔でいられること。<br>このことがあかちゃんを授かるためにも、そしてあなたの人生のためにも、もっとも大切なことだと思います。</p>
			<p>いろいろなご相談をこれまでに<?php echo $report_num; ?>件以上お受けしています。<br>悩んでいるのは、決してあなただけではありません。<br>少しでもあなたのお役に立てれば幸いです。</p>
			<p>参考図書：文芸社「心と体にやさしい不妊治療」</p>
		<!-- /#avoid-trouble --></div>

		<div class="category_link01">
			<ul>
				<li><a href="/before-infertility-treatment/possible-at-hospital.php">病院でできること。できないこと。</a></li>
				<li><a href="/before-infertility-treatment/avoid-trouble.php">食事・生活の改善で、婦人科トラブルを治そう</a></li>
				<li><a href="/before-infertility-treatment/check.php">自分の体をチェックしてみませんか？</a></li>
				<li><a href="/before-infertility-treatment/basal-temparature.php">基礎体温って何？</a></li>
				<li><a href="/before-infertility-treatment/basal-temparature-type.php">あなたはどのタイプ？いろいろな基礎体温表</a></li>
			</ul>
		</div><!-- /.category_link01 -->

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
