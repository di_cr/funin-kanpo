<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>不妊治療について｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="不妊治療を始める前にお読みいただきたいコンテンツ一覧です。不妊症は、赤ちゃんを産み育てる力「妊娠力」が、不足している状態というのが正解だと思います。正しい知識を知って、あなたの妊娠力を高める方法を知ってください。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/before-infertility-treatment/">
<meta property="og:type" content="article">
<meta property="og:title" content="不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="不妊治療を始める前にお読みいただきたいコンテンツ一覧です。不妊症は、赤ちゃんを産み育てる力「妊娠力」が、不足している状態というのが正解だと思います。正しい知識を知って、あなたの妊娠力を高める方法を知ってください。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/before-infertility-treatment/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "不妊治療について"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g03">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li>不妊治療について</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1>不妊治療について</h1>
			<span>Before you start the infertility treatment</span>
		<!-- /.h1_basic01 --></div>

		<div id="before-infertility-treatment">
			<div class="txt_lead02">
				<p>そもそも赤ちゃんができないことって病気でしょうか？<br>「不妊症」とはいうけれど、「不妊病」とはいいませんよね。</p>
			<!-- /.txt_lead02 --></div>
			<p>病気というよりは、<span class="red">赤ちゃんを産み育てる力「妊娠力」</span>が、なんらかの理由で不足した状態というのが正解だと思います。<span class="red">この妊娠力を妊娠できるところまで、補ってあげたり、高めてあげる必要があります。</span><br>そのための一つの方法が、病院で行う不妊治療や「妊娠しやすい体づくり」です。<br>妊娠力がある程度あれば、少しの努力で赤ちゃんの笑顔に会えるでしょう。<br>でも、妊娠力が全然足りなかったら、つらい不妊治療に何度挑戦しても、良い結果につながらないかもしれません・・・<br>どうか、あなたの妊娠力を高める方法を知ってください。</p>

			<section class="box_basic01">
				<div class="contents_link01">
					<h2>CONTENTS</h2>
					<ul>
						<li><a href="/before-infertility-treatment/possible-at-hospital.php">病院でできること。<br>できないこと。</a></li>
						<li><a href="/before-infertility-treatment/avoid-trouble.php">食事・生活の改善で、<br>婦人科トラブルを治そう</a></li>
						<li><a href="/before-infertility-treatment/check.php">自分の体をチェックしてみませんか？ </a></li>
						<li><a href="/before-infertility-treatment/basal-temparature.php">基礎体温って何？</a></li>
						<li><a href="/before-infertility-treatment/basal-temparature-type.php">あなたはどのタイプ？<br>いろいろな基礎体温表</a></li>
					</ul>
				<!-- /.contents_link01 --></div>
			</section>

			<section class="box_notebook01">
				<h2>不妊でお悩みの方へ<br>治療がすべてではありません</h2>
				<div class="notebook_inner01">
					<p><span class="gray">「赤ちゃんはまだ？」</span><br>どうか無邪気な質問や期待に傷つかないでください。<br>ひとりで悩みを抱え込まないでほしいです。</p>
					<p><span class="gray">結婚したら子供ができてあたりまえ。</span><br>そんな常識に責められてひとりで悩みを抱えこまないでほしいのです。<br>世間でいう「普通であること」って、時としてとても大変なこと。<br>少しでも普通からずれてしまうと、普通であることを求められるのは、とても辛かったりするんですよね・・・</p>
					<p><span class="yellow">妊娠することは、あなたにとってどんなことでしょう？</span><br><span class="yellow">ちょっと一度立ち止まって、考えてみてください。</span></p>
					<p class="img01"><img src="/common/img/before/txt_before01.png" alt="まわりの期待にこたえるため つらくて長い不妊治療のゴール あかちゃんのいる新しい生活のスタート" width="547" height="180"></p>
					<p class="mt00">ホルモン剤や排卵誘発剤などによる治療、人工授精、体外受精・・・<br>妊娠にいたるまで、次々と高度な治療を行っていくという方法は現代の不妊治療の大きな流れです。<br>このような治療を続け、道が開けるカップルがいる一方で、次第にステップアップしていく治療に不安やとまどいを抱える方とも、たくさんお会いしてきました。</p>
					<p>自分が何をしていて、どういう状態にあるかわからず、インターネットや本などから必死に情報を集めたり、治療をしているのに期待が裏切られるのが怖くて、次の治療をどうするか考えてしまう・・・</p>
					<p class="img02"><img src="/common/img/before/txt_before02.png" alt="毎月、小さな流産をしているみたい" width="247" height="128"></p>
					<p class="mt00">そう話された方もいらっしゃいました。</p>
					<p>だからこそ考えてみてください。<br><span class="yellow">あなたにとって、妊娠がどういう意味を持つかを。</span><br><span class="yellow">そして、自分にとって必要な治療を受けているかどうかを。</span></p>
				<!-- /.notebook_inner01 --></div>
			</section>
		<!-- /#before-infertility-treatment --></div>

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
							<span class="label_new"><img src="/common/img/label_new01.png" alt="NEW"></span>
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
