<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>基礎体温の動きからみるあなたのタイプ｜不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="「基礎体温の動きからみるあなたのタイプ」のページです。基礎体温のグラフにはいくつかの傾向やタイプがあります。この傾向により、自分が妊娠しやすいのか、あるいは妊娠しにくいのかを知ることができます。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/before-infertility-treatment/basal-temparature-type.php">
<meta property="og:type" content="article">
<meta property="og:title" content="基礎体温の動きからみるあなたのタイプ｜不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="「基礎体温の動きからみるあなたのタイプ」のページです。基礎体温のグラフにはいくつかの傾向やタイプがあります。この傾向により、自分が妊娠しやすいのか、あるいは妊娠しにくいのかを知ることができます。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/before-infertility-treatment/basal-temparature-type.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "不妊治療を始める前に"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "基礎体温の動きからみるあなたのタイプ"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g03">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/before-infertility-treatment/">不妊治療を始める前に</a></li>
			<li>基礎体温の動きからみるあなたのタイプ</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1>あなたはどのタイプ？いろいろな基礎体温表</h1>
		<!-- /.h1_basic01 --></div>

		<div id="basal-temparature-type">
			<section>
				<div class="h2_basic01">
					<h2>①正常型</h2>
				<!-- /.h2_basic01 --></div>
				<p>正常な基礎体温は以下のグラフのように①高温期が12&#xFF5E;14日続く ②高温期と低温期の差が0.3&#xFF5E;0.5℃ある ③低温から高温へ1&#xFF5E;2日以内に移行するという3つの条件が揃っています。</p>
				<p><img src="/common/img/before/img_type01.png" alt="基礎体温表（正常型）" width="740" height="270"></p>
			</section>
			<section>
				<div class="h2_basic01">
					<h2>②低温期が長く、高温期が短い</h2>
				<!-- /.h2_basic01 --></div>
				<p>赤ちゃんができにくい方に比較的良く見られるタイプです。卵子の成熟が悪く排卵が遅れ、その結果黄体ホルモンの分泌が悪くなります。西洋医学では黄体機能不全、軽度の排卵障害などが考えられます。<br>このタイプは低温期に陰血が不足して陰が長じることができないために、排卵期にスムーズに転化できず、高温期に陽が不足して短くなります。低温期にしっかりと陰血を養いながら少し陽を補うことが大切です。</p>
				<p><img src="/common/img/before/img_type02.png" alt="基礎体温表（低温期が長く、高温期が短い）" width="740" height="270"></p>
			</section>
			<section>
				<div class="h2_basic01">
					<h2>③高温期が不安定</h2>
				<!-- /.h2_basic01 --></div>
				<p>高温期の途中で体温が下がったり、生理が近づくと体温が下がるタイプです。<br>西洋医学では黄体機能不全が考えられます。<br>このタイプは陽気の不足により高温を維持することができません。よって高温期に陽気を補い体温を維持することが大切です。</p>
				<p><img src="/common/img/before/img_type03.png" alt="基礎体温表（高温期が不安定）" width="740" height="270"></p>
			</section>
			<section>
				<div class="h2_basic01">
					<h2>④体温上昇が遅い</h2>
				<!-- /.h2_basic01 --></div>
				<p>このタイプも不妊症の方に比較的良く見られます。西洋医学では黄体機能不全、排卵障害、高プロラクチン血症などが考えられます。<br>このタイプは排卵期に①陽気不足により体温が上がらないケース ②血などが体温の上昇を妨げるケースが考えられます。プロラクチンが高いことが原因になるケースも見られます。全身症状より弁証して適切な漢方薬を組み合わせます。</p>
				<p><img src="/common/img/before/img_type04.png" alt="基礎体温表（体温上昇が遅い）" width="740" height="270"></p>
			</section>
			<section>
				<div class="h2_basic01">
					<h2>⑤波動が激しい</h2>
				<!-- /.h2_basic01 --></div>
				<p>ストレスが多く、自律神経が不安定な方に良くみられます。西洋医学では高プロラクチン血症、月経前緊張症候群、自律神経失調症などが考えられます。<br>このタイプはストレスにより肝気の疎泄作用が乱れ、体温も不安定になっています。肝気を整えてストレスを和らげることが大切です。</p>
				<p><img src="/common/img/before/img_type05.png" alt="基礎体温表（波動が激しい）" width="740" height="270"></p>
			</section>
			<section>
				<div class="h2_basic01">
					<h2>⑥一相性で高温期がない</h2>
				<!-- /.h2_basic01 --></div>
				<p>いわゆる無排卵です。西洋医学では卵巣機能不全、多嚢胞性卵巣、高プロラクチン血症などが考えられます。<br>このタイプは排卵がないので、周期調節法を行うことが難しいです。無月経の場合は補腎＋養血＋活血の漢方薬を症状により加減して服用します</p>
				<p><img src="/common/img/before/img_type06.png" alt="基礎体温表（一相性で高温期がない）" width="740" height="270"></p>
			</section>
		<!-- /#basal-temparature-type --></div>

		<div class="category_link01">
			<ul>
				<li><a href="/before-infertility-treatment/possible-at-hospital.php">病院でできること。できないこと。</a></li>
				<li><a href="/before-infertility-treatment/avoid-trouble.php">食事・生活の改善で、婦人科トラブルを治そう</a></li>
				<li><a href="/before-infertility-treatment/check.php">自分の体をチェックしてみませんか？</a></li>
				<li><a href="/before-infertility-treatment/basal-temparature.php">基礎体温って何？</a></li>
				<li><a href="/before-infertility-treatment/basal-temparature-type.php">あなたはどのタイプ？いろいろな基礎体温表</a></li>
			</ul>
		</div><!-- /.category_link01 -->

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
