<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>妊娠力セルフチェック｜不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="「妊娠力セルフチェック」のページです。生活習慣などに関する24個の項目をご用意しました。チェックを付けた数から、あなたの今の妊娠力をセルフチェックすることができます。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/before-infertility-treatment/check.php">
<meta property="og:type" content="article">
<meta property="og:title" content="妊娠力セルフチェック｜不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="「妊娠力セルフチェック」のページです。生活習慣などに関する24個の項目をご用意しました。チェックを付けた数から、あなたの今の妊娠力をセルフチェックすることができます。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/before-infertility-treatment/check.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "不妊治療を始める前に"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "妊娠力セルフチェック"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g03">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/before-infertility-treatment/">不妊治療を始める前に</a></li>
			<li>妊娠力セルフチェック</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1>自分の体をチェックしてみませんか？</h1>
		<!-- /.h1_basic01 --></div>

		<div id="check">
			<p>毎日、家事に仕事にお疲れさまです。がんばっているからこそ、体にも負担がきてしまいます。無理しちゃうからこそ、ついつい自分のことが後回しになったりしてませんか？家族にとって、あなたはとっても大切な人。だからこそ元気でいることを、毎日をすっきり楽に過ごすことを、あたりまえのことのように感じてください。</p>

			<div class="check_outer01">
				<div class="check_inner01">
					<p>チェックリストを使って自分の体のチェックができます。</p>
					<ul>
						<li><input type="checkbox" id="check01"><label for="check01">生理の時にレバー状の塊が混じる</label></li>
						<li><input type="checkbox" id="check02"><label for="check02">ふとんに入って30分以上寝つけない</label></li>
						<li><input type="checkbox" id="check03"><label for="check03">朝、すっきり起きれない</label></li>
						<li><input type="checkbox" id="check04"><label for="check04">生理痛がある</label></li>
						<li><input type="checkbox" id="check05"><label for="check05">特に病気はないのにやる気が出ない、疲れる</label></li>
						<li><input type="checkbox" id="check06"><label for="check06">手足がいつも冷たい</label></li>
						<li><input type="checkbox" id="check07"><label for="check07">常に肩がこっていて、頭痛がよく起こる</label></li>
						<li><input type="checkbox" id="check08"><label for="check08">部屋の電気をつけたままでも、つい眠ってしまう</label></li>
						<li><input type="checkbox" id="check09"><label for="check09">眠っても熟睡感がなく、疲れが取れない</label></li>
						<li><input type="checkbox" id="check10"><label for="check10">夢をよく見て、眠りが浅い</label></li>
						<li><input type="checkbox" id="check11"><label for="check11">昔と同じダイエットをしても、やせにくくなった</label></li>
						<li><input type="checkbox" id="check12"><label for="check12">白髪やしわが多く、年の割に老けて見られる</label></li>
						<li><input type="checkbox" id="check13"><label for="check13">生理の周期が整わない</label></li>
						<li><input type="checkbox" id="check14"><label for="check14">むくみやすい。くつ下の跡がくっきりつく</label></li>
						<li><input type="checkbox" id="check15"><label for="check15">生理前に胸やお腹がはったり、ニキビが出る</label></li>
						<li><input type="checkbox" id="check16"><label for="check16">3日以上便秘をしたり、コロコロや硬いうんち</label></li>
						<li><input type="checkbox" id="check17"><label for="check17">のどに物が詰まったような不快感がある</label></li>
						<li><input type="checkbox" id="check18"><label for="check18">お腹をさわると冷たい</label></li>
						<li><input type="checkbox" id="check19"><label for="check19">いきなり体が冷えたり、のぼせたりする</label></li>
						<li><input type="checkbox" id="check20"><label for="check20">髪の毛のボリュームが減り、抜け毛が増えた</label></li>
						<li><input type="checkbox" id="check21"><label for="check21">イライラしたり、落ち込んだり気持が不安定</label></li>
						<li><input type="checkbox" id="check22"><label for="check22">周りは平気なのに自分だけが寒く感じる</label></li>
						<li><input type="checkbox" id="check23"><label for="check23">生理中、腰や足がだるい</label></li>
						<li><input type="checkbox" id="check24"><label for="check24">赤ちゃんができにくい </label></li>
					</ul>
					<div class="check_count01">
						<dl>
							<dt>チェック合計：</dt>
							<dd><span class="check_count_num01"></span>コ</dd>
						</dl>
					<!-- /.check_count01 --></div>
				<!-- /.check_inner01 --></div>
			<!-- /.check_outer01 --></div>

			<div class="box_description02">
				<div class="dl_basic01">
					<dl>
						<dt class="mt00">＜チェックの数＞</dt>
						<dd>0&#xFF5E;3個　基本的な妊娠力は高いようです。</dd>
						<dd>4&#xFF5E;7個　妊娠力が下がり始めています。</dd>
						<dd>8個以上　根本的な体づくりをして、妊娠力を高める必要があります。</dd>
					</dl>
				<!-- /.dl_basic01 --></div>
			<!-- /.box_description02 --></div>
		<!-- /#check --></div>

		<div class="category_link01">
			<ul>
				<li><a href="/before-infertility-treatment/possible-at-hospital.php">病院でできること。できないこと。</a></li>
				<li><a href="/before-infertility-treatment/avoid-trouble.php">食事・生活の改善で、婦人科トラブルを治そう</a></li>
				<li><a href="/before-infertility-treatment/check.php">自分の体をチェックしてみませんか？</a></li>
				<li><a href="/before-infertility-treatment/basal-temparature.php">基礎体温って何？</a></li>
				<li><a href="/before-infertility-treatment/basal-temparature-type.php">あなたはどのタイプ？いろいろな基礎体温表</a></li>
			</ul>
		</div><!-- /.category_link01 -->

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
