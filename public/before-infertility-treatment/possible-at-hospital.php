<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>病院でできること・できないこと｜不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="「病院でできること・できないこと」のページです。「妊娠力」を高める治療について、病院でできることとできないことがあります。堀江薬局は漢方などの力を活用して「妊娠しやすい体づくり」や「習慣づくり」をお手伝いしています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/before-infertility-treatment/possible-at-hospital.php">
<meta property="og:type" content="article">
<meta property="og:title" content="病院でできること・できないこと｜不妊治療を始める前に｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="「病院でできること・できないこと」のページです。「妊娠力」を高める治療について、病院でできることとできないことがあります。堀江薬局は漢方などの力を活用して「妊娠しやすい体づくり」や「習慣づくり」をお手伝いしています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/before-infertility-treatment/possible-at-hospital.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "不妊治療を始める前に"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "病院でできること・できないこと"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g03">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/before-infertility-treatment/">不妊治療を始める前に</a></li>
			<li>病院でできること・できないこと</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1>病院でできること。できないこと。</h1>
		<!-- /.h1_basic01 --></div>

		<div id="possible-at-hospital">
			<div class="txt_lead02">
				<p>「不妊症」とは、実際のところは、赤ちゃんを産む力「妊娠力」が、なんらかの理由で不足した状態というのが正解だと思います。</p>
			<!-- /.txt_lead02 --></div>
			<p>この妊娠力を妊娠できるところまで、補ってあげたり、高めてあげる必要があります。<br>それには、</p>
			<div class="list_basic01 mt00">
				<ul>
					<li>病院で治療する</li>
					<li>食事や生活を変える</li>
					<li>漢方やサプリメント、鍼灸などで体質改善する</li>
				</ul>
			<!-- /.list_basic01 --></div>
			<p class="mt15">といった方法が考えられます。</p>
			<p>でも、これだけ不妊治療の技術が進んでいるから、病院に行ってれば大丈夫じゃないの？そう思われるかもしれません。<br>不妊治療は素晴らしい技術であることは間違いないですし、妊娠への大きな可能性をもたらしてくれます。ただ、決して受ければ誰もが赤ちゃんに恵まれる夢の治療ではないんですね。つらい面もたくさんあります。</p>
			<div class="box_quote01">
				<p>妊娠率も人工授精で5&#xFF5E;15％、体外受精で15&#xFF5E;25％、顕微授精ですら20&#xFF5E;25％です。</p>
			<!-- /.box_quote01 --></div>
			<p class="mt20">肉体的、精神的な負担はもちろんのこと、高額な費用もかかります。<br>（だからこそ、1回1回の治療を後悔のないように大切に受けてほしいと思います。）</p>
			<p>そしてまた、実は妊娠に関して、治療できること、できないことがあります。</p>

			<div class="box_col_2">
				<div class="box_description01">
					<dl>
						<dt class="tit">病院でできること</dt>
						<dd>
							<ul>
								<li>不妊の原因をみつけること</li>
								<li>排卵を誘発すること</li>
								<li>女性ホルモンを一時的に補充すること</li>
								<li>妊娠のタイミングをあわせること</li>
								<li>子宮ポリープや卵管癒着などを治療すること</li>
								<li>受精の手伝いをすること</li>
							</ul>
						</dd>
					</dl>
				<!-- /.box_description01 --></div>
				<div class="box_description02">
					<dl>
						<dt class="tit">病院ではどうしてもできないこと</dt>
						<dd>
							<ul>
								<li>卵子の質を高めること</li>
								<li>着床しやすい子宮内環境を整えること</li>
								<li>女性ホルモンのバランスを整えること</li>
							</ul>
						</dd>
					</dl>
				<!-- /.box_description02 --></div>
			<!-- /.box_col_2 --></div>
			<p>病院でできないことは、あなた自身がしていかなくてはいけません。<br>そして、それこそ、食事や生活を変え、漢方やサプリメントなどの力を利用していく「妊娠しやすい体づくり」なのです。</p>
			<p>ちょっとわかりにくいかもしれないので、下の図を見てみてください。</p>
			<p><img src="/common/img/before/img_before_hospital01.png" alt="a：自分の体質（妊娠力高）→妊娠可能、b：自分の体質（妊娠力中）→妊娠可能ラインに届かない、c：自分の体質（妊娠力低）→妊娠可能ラインに届かない、d：自分の体質（妊娠力中）＋体作り→妊娠可能、e：自分の体質（妊娠力低）＋病院の治療→妊娠可能ラインに届かない、f：自分の体質（妊娠力低）＋体作り→妊娠可能ラインに届かない、g：自分の体質（妊娠力低）＋体作り＋病院の治療→妊娠可能" width="740" height="330"></p>
			<p>aは、妊娠力が高いので、自然妊娠できます。<br>ｂは少し妊娠力が下がっているので、ｄのように病院の治療か体づくりで妊娠力を補う必要があります。<br>ｃは、かなり妊娠力が低くなってしまったので、どちらかだけの方法では、残念ながらe、fのように妊娠可能ラインまで届きません。<span class="yellow">なので病院の治療と体づくりの両方をしたほうがいいですね。</span></p>

			<section class="movies01">
				<div class="h2_basic03">
					<h2>おすすめ動画</h2>
				<!-- /.h2_basic03 --></div>
				<div class="box_movie01">
					<iframe width="740" height="416" src="https://www.youtube.com/embed/hAqw67fN3qM?rel=0&amp;controls=0&amp;showinfo=1" frameborder="0" allowfullscreen></iframe>
				<!-- /.box_movie01 --></div>
				<div class="box_movie01">
					<iframe width="740" height="416" src="https://www.youtube.com/embed/C7Go4MnPiws?rel=0&amp;controls=0&amp;showinfo=1" frameborder="0" allowfullscreen></iframe>
				<!-- /.box_movie01 --></div>
				<div class="box_movie01">
					<iframe width="740" height="416" src="https://www.youtube.com/embed/U315Xu6IO48?rel=0&amp;controls=0&amp;showinfo=1" frameborder="0" allowfullscreen></iframe>
				<!-- /.box_movie01 --></div>
			</section>
		<!-- /#possible-at-hospital --></div>

		<div class="category_link01">
			<ul>
				<li><a href="/before-infertility-treatment/possible-at-hospital.php">病院でできること。できないこと。</a></li>
				<li><a href="/before-infertility-treatment/avoid-trouble.php">食事・生活の改善で、婦人科トラブルを治そう</a></li>
				<li><a href="/before-infertility-treatment/check.php">自分の体をチェックしてみませんか？</a></li>
				<li><a href="/before-infertility-treatment/basal-temparature.php">基礎体温って何？</a></li>
				<li><a href="/before-infertility-treatment/basal-temparature-type.php">あなたはどのタイプ？いろいろな基礎体温表</a></li>
			</ul>
		</div><!-- /.category_link01 -->

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
