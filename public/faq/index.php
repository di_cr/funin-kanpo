<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>よくある質問｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="よくある質問のページです。堀江薬局に寄せられる様々なご質問にお答えします。直接のご相談は滞在型セミナーの「子宝リトリート」にて承っております。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/faq/">
<meta property="og:type" content="article">
<meta property="og:title" content="よくある質門｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="よくある質問のページです。堀江薬局に寄せられる様々なご質問にお答えします。直接のご相談は滞在型セミナーの「子宝リトリート」にて承っております。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/faq/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/category.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/faq.css" media="all">

<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/topics/",
					"name": "トピックス一覧"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "よくある質問"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g06">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/topics/">トピックス一覧</a></li>
			<li>よくある質問</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
		<div class="h1_basic01 inner01">
			<h1>よくある質問</h1>
			<span>FAQ</span>
		<!-- /.h1_basic01 --></div>

		<div class="topics_wrap01 inner01">
			<div class="txt_lead01 mb30">
				<p>よくお問い合わせを受ける質問と回答を掲載しています。</p>
			<!-- /.txt_lead01 --></div>
			
			<div class="anchor01 mb40">
				<ul>
					<li><a href="#faq01">不妊治療について</a></li>
					<li><a href="#faq02">体について</a></li>
					<li><a href="#faq03">流産について</a></li>
					<li><a href="#faq04">漢方・薬膳茶について</a></li>
					<li><a href="#faq05">セミナー・リトリートについて</a></li>
					<li><a href="#faq06">お買い物方法について</a></li>
				</ul>
			<!-- /.anchor01 --></div>
			
			<section id="faq01" class="faq01">
				<h2 class="h2_faq01"><a href="#">不妊治療について</a></h2>
				<ul class="faq_list01">
					<li><a href="#"><span>Q.</span> 何度も治療してるのに成功しないんです・・・</a></li>
					<li><a href="#"><span>Q.</span> 二人目がなかなか授からなんですが・・・</a></li>
					<li><a href="#"><span>Q.</span> 年をとっていくと妊娠できなくなるようで不安です。</a></li>
				</ul>
			</section>
			
			<section id="faq02" class="faq01">
				<h2 class="h2_faq01"><a href="#">体について</a></h2>
				<ul class="faq_list01">
					<li><a href="#"><span>Q.</span> 生理痛やレバー状の塊があるんですが・・・</a></li>
					<li><a href="#"><span>Q.</span> 基礎体温って何？</a></li>
				</ul>
			</section>
			
			<section id="faq03" class="faq01">
				<h2 class="h2_faq01"><a href="#">流産について</a></h2>
				<ul class="faq_list01">
					<li><a href="#"><span>Q.</span> 生理予定日から1週間たたず（5日目でした）に妊娠検査薬を使ったら、薄く線が出ました。これは陽性反応なのでしょうか？</a></li>
					<li><a href="#"><span>Q.</span> 妊娠反応が陽性になって喜んでいたら、出血がありました。 生理ほどでの血の量ではないのですが・・・。 これは流産でしょうか？</a></li>
					<li><a href="#"><span>Q.</span> 流産後、次の妊娠まではどのくらいあけたらいいの？</a></li>
					<li><a href="#"><span>Q.</span> 流産後、生理がなかなかきません。</a></li>
					<li><a href="#"><span>Q.</span> 1人こどもはいるのですが、その後流産が続いています。 なぜでしょうか？</a></li>
					<li><a href="#"><span>Q.</span> 子宮の形が悪いと言われました。流産の原因になりますか？</a></li>
					<li><a href="#"><span>Q.</span> 流産が起きるのは、いつ頃が多いですか？</a></li>
					<li><a href="#"><span>Q.</span> 流産予防のための漢方が、ありますか？</a></li>
				</ul>
			</section>
			
			<section id="faq04" class="faq01">
				<h2 class="h2_faq01"><a href="#">漢方・薬膳茶について</a></h2>
				<ul class="faq_list01">
					<li><a href="#"><span>Q.</span> 健康のために飲んでもいいものでしょうか・・・</a></li>
					<li><a href="#"><span>Q.</span> 薬膳にはどのような効果が期待できますか？</a></li>
					<li><a href="#"><span>Q.</span> どんな頻度で飲んだらいいですか？</a></li>
					<li><a href="#"><span>Q.</span> 苦いですか？</a></li>
				</ul>
			</section>
			
			<section id="faq05" class="faq01">
				<h2 class="h2_faq01"><a href="#">セミナー・リトリートについて</a></h2>
				<ul class="faq_list01">
					<li><a href="#"><span>Q.</span> セミナー中、堀江先生に相談することはできますか？</a></li>
					<li><a href="#"><span>Q.</span> 一人での参加が不安なのですが…</a></li>
					<li><a href="#"><span>Q.</span> 先払いですか？現地払いですか？</a></li>
				</ul>
			</section>
			
			<section id="faq05" class="faq01">
				<h2 class="h2_faq01"><a href="#">お買い物方法について</a></h2>
				<ul class="faq_list01">
					<li><a href="#"><span>Q.</span> 支払い方法は何がありますか？</a></li>
					<li><a href="#"><span>Q.</span> 注文からどのくらいで届きますか？</a></li>
					<li><a href="#"><span>Q.</span> 漢方・薬膳茶の定期購入は停止・休止できますか？</a></li>
				</ul>
			</section>
			
			<div class="page-load-status">
				<div class="loader-ellips infinite-scroll-request">読み込み中</div>
				<p class="infinite-scroll-error">記事が読み込めませんでした</p>
			<!-- /.page-load-status --></div>
		<!-- .topics_wrap01 --></div>
	</main>
<!-- /#wrapper --></div>


<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<script type="text/javascript" src="/common/js/infinite-scroll.pkgd.min.js"></script>
<script type="text/javascript" src="/common/js/infinite_setting.js"></script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
