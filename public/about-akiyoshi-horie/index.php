<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>堀江昭佳について｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="漢方薬局「堀江薬局」代表薬剤師・不妊カウンセラーの堀江昭佳のご紹介です。堀江昭佳は婦人科系、特に不妊症を専門とする漢方薬剤師です。西洋医学、漢方医学、心理学からの総合的なアプローチに定評があり、多数の実績を有しています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/about-akiyoshi-horie/">
<meta property="og:type" content="article">
<meta property="og:title" content="堀江昭佳について｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="漢方薬局「堀江薬局」代表薬剤師・不妊カウンセラーの堀江昭佳のご紹介です。堀江昭佳は婦人科系、特に不妊症を専門とする漢方薬剤師です。西洋医学、漢方医学、心理学からの総合的なアプローチに定評があり、多数の実績を有しています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/about-akiyoshi-horie/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/about.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "堀江昭佳について"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g00">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li>堀江昭佳について</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
		<div class="h1_basic01 inner01 no_border">
			<h1>堀江 昭佳</h1>
			<span>Akiyoshi Horie</span>
		<!-- /.h1_basic01 --></div>

		<div class="about_mainimg01">
			<section class="inner01">
				<h2>赤ちゃんを授からないかもしれない。<br>そんな不安を抱えていませんか？</h2>
				<p>今回もダメかもしれない。そんなふうに思いながら不妊治療をしていませんか？授からない自分は女性失格。そうやって自分を追い込んでいませんか？</p>
				<p>大丈夫です。まずは、あなたのこころに安心と自信の火を灯しましょう。体を整えることで、心は整っていきます。心が整うと、あなたの夢や目標もかなっていきます。冷えや生理痛、生理不順。あるいは、子宮内膜症や子宮筋腫、排卵障害、多嚢胞性卵巣症候群。そんなトラブルを癒やし、妊娠しやすい体づくりをしていくことで、体はどんどん楽になっていきます。そして、体が楽になると、自然と心も楽になる。その向こうに、きっとあなたのしあわせな妊娠が待っています。</p>
				<p class="about_mainimg_sign01"><img src="/common/img/about/txt_sign01.png" alt="堀江 昭佳" width="184" height="55"></p>
			</section>
		<!-- /.about_mainimg01 --></div>

		<div class="inner02 about_contents01">
			<div class="anchor01">
				<ul>
					<li><a href="#message">メッセージ</a></li>
					<li><a href="#profile">プロフィール</a></li>
					<li><a href="#book">著書</a></li>
					<li><a href="#media">出演情報</a></li>
				</ul>
			<!-- /.anchor01 --></div>

			<section id="message">
				<div class="h2_basic01">
					<h2>メッセージ</h2>
					<span>Message</span>
				<!-- /.h2_basic01 --></div>

				<div class="box_notebook01 mt40">
					<div class="lead_concept01">
						<p>本来、人間には誰しも<br>妊娠するちからが備わっています。</p>
					<!-- /.lead_concept01 --></div>
					<p>もしかすると、いまはそのことが信じられないかもしれませんが、必ず備わっています。</p>
					<p>不妊という状態は、本来備わっているはずの妊娠力が下がってしまって、妊娠できない状態になっているにすぎません。</p>
					<p>下がってしまったあなたの妊娠力を妊娠できるラインとの間にすき間ができています。<br>そのすき間を埋めてあげる必要があります。</p>
					<p>例えば、生理痛、生理不順、冷え症といったような不調があったり、あるいは、子宮内膜症や子宮筋腫、排卵障害、多嚢胞性卵巣症候群といった病名がついている場合もあるかもしれません。</p>
					<p>これは、妊娠をゴールとしたマラソンに例えると、とても重いおもりを背負いながら走っているのと同じことです。<br>他のひとよりもしんどくて当然ですし、途中で疲れてリタイアすることになってしまう。</p>
					<p>そして、これはとても大切なことですが、生理痛やひどい冷え症がある場合などは、そもそもスタートラインにつけていません。<br>スタートラインのずっと手前で、障害物競走をしているような状況です。</p>
					<p>マラソン選手は、本番に備えて準備をします。<br>>いきなり42.195kmを走ることはできません。<br>不妊治療も同じです。</p>
					<p class="img_message01"><img src="/common/img/about/img_message01.png" alt="妊娠できない。赤ちゃんが来てくれない。"></p>
					<p>それはあなたが悪いのではありません。<br>ただ単に体の準備ができていないというだけの可能性も高いのです。</p>
					<p>自分のことを大切にせずに、不妊治療をするのはやめませんか？</p>
					<p>まず、体を整えましょう。<br>トラブルを減らしましょう。<br>ゼロにならなくてもいい、いまよりも軽くしてあげるだけぐっと妊娠力は高まります。</p>
					<p>不調やトラブルが軽くなると、みなさん表情や言葉が変わります。<br>考えてみるとあたりまえのことかもしれません。</p>
					<p class="img_message02"><img src="/common/img/about/img_message02.png" alt="朝気持ちよく起きたら、1日がハッピーにはじまる。体調がよかったら、前向きな気持で行動できる。生理痛がなくなったら、生理が嫌でなくなる。"></p>
					<p>すると、少しずつ自分の体に対して、自分自身に対して自信がもてるようになってきます。<br>いつもネガティブに考えていたことが、だんだんと前向きにとらえられるようになる。</p>
					<p>妊娠しやすい体に整えてあげると、閉じていた心もひらいて、心の面からも赤ちゃんを迎えやすくなります。</p>
					<section>
						<div class="h3_basic03"><h3>どうか自分を責めないでください。<br>大丈夫です。</h3></div>
						<p>あなたには妊娠できるちからが生まれながら備わっています。<br>ただ単に、準備ができていなかっただけに過ぎません。<br>体と心を整えて、赤ちゃんを迎えてあげてください。</p>
					</section>
				<!-- /.box_notebook01 --></div>
			</section>

			<section id="profile">
				<div class="h2_basic01">
					<h2>プロフィール</h2>
					<span>Profile</span>
				<!-- /.h2_basic01 --></div>
				<div class="box_basic01">
					<div class="box_col_2">
						<div class="img">
							<img src="/common/img/about/img_about_profile01.png" alt="堀江 昭佳" width="351" height="242">
						<!-- /.img --></div>
						<div class="table_basic01">
							<dl>
								<dt>本名</dt>
								<dd>堀江 昭佳（ほりえ あきよし）</dd>
							</dl>
							<dl>
								<dt>生まれ</dt>
								<dd>1974年生まれ</dd>
							</dl>
							<dl>
								<dt>出身</dt>
								<dd>島根県 出雲市出身</dd>
							</dl>
							<dl>
								<dt>役職</dt>
								<dd>
									<ul>
										<li>漢方薬剤師、不妊カウンセラー</li>
										<li>一般社团法人日本漠方薬膳協会 代表理事</li>
										<li>有限会社堀江薬局代表</li>
									</ul>
								</dd>
							</dl>
						<!-- /.table_basic01 --></div>
					<!-- /.box_col_2 --></div>
				<!-- /.box_basic01 --></div>
				<p>1974年生まれ、出雲市出身。出雲大社参道で90年以上続く老舗漢方薬局の4代目。薬学部を卒業後、薬剤師となったのち対症療法中心の西洋医学とは違う、東洋医学・漢方の根本療法に魅力を感じ、方向転換する。本場中国の漢方医から学ぶ中、不妊に悩む友人の相談を受けたところ、漢方で妊娠したことに感動し、婦人科系の分野、なかでも不妊症を専門とするようになる。体の不調の解消だけではなく、本人の抱えている常識や執着といった束縛からの「心の解放」を終着点としている唯一の漢方薬剤師。</p>
				<p>血流を中心にすえた西洋医学、漢方医学、心理学の3つの視点からの総合的なアプロー チは評判を呼び、自身の薬局で扱ってきた不妊、うつ、ダイエット、自律神経失調症 など心と体の悩みは5万件を超える。地元島根はもとより全国、海外からも相談がある。 不妊相談では9割が病院での不妊治療がうまくいかず、来局されるケースであるものの、寄せられる妊娠報告は数多い。日本漢方薬膳協会の代表理事にも就任し、広く漢方薬膳の知識を広め、より多くの女性に幸せと笑顔を届けるために奮闘中である。</p>
				<p>また、2016年3月に出版した著書「血流がすべて解決する」(サンマーク出版)は20万部を突破、テレビ、雑誌等各種メディアで取り上げられるなど話題になっている。</p>
			</section>

			<section id="book">
				<div class="h2_basic01">
					<h2>著作</h2>
					<span>Book</span>
				<!-- /.h2_basic01 --></div>
				<div class="box_col_2">
					<div class="img">
						<img src="/common/img/about/img_about_book01.png" alt="血流がすべて解決する" width="200" height="296">
					<!-- /.img --></div>
					<div class="txt">
						<section>
							<div class="h3_basic01">
								<h3>血流がすべて解決する</h3>
							</div>
							<p>堀江昭佳 著</p>
							<div class="btn_amazon01"><a href="http://www.amazon.co.jp/%E8%A1%80%E6%B5%81%E3%81%8C%E3%81%99%E3%81%B9%E3%81%A6%E8%A7%A3%E6%B1%BA%E3%81%99%E3%82%8B-%E5%A0%80%E6%B1%9F%E6%98%AD%E4%BD%B3/dp/4763135368" target="_blank"><img src="/common/img/about/btn_amazon01.png" alt="Amazonで買う" width="203" height="42"></a></div>
							<div><a href="https://www.amazon.co.jp/%E8%A1%80%E6%B5%81%E3%81%8C%E3%81%99%E3%81%B9%E3%81%A6%E8%A7%A3%E6%B1%BA%E3%81%99%E3%82%8B-%E5%A0%80%E6%B1%9F%E6%98%AD%E4%BD%B3/product-reviews/4763135368/ref=cm_cr_dp_d_show_all_top?ie=UTF8&reviewerType=all_reviews" target="_blank" class="link_basic01">Amazonのレビューを見る・書く</a></div>
							<div class="book_data01">
								<dl>
									<dt>単行本（ソフトカバー）：</dt>
									<dd>267ページ</dd>
								</dl>
								<dl>
									<dt>出版社：</dt>
									<dd>サンマーク出版</dd>
								</dl>
								<dl>
									<dt>発売日：</dt>
									<dd>2016/3/11</dd>
								</dl>
								<dl>
									<dt>ISBN-10：</dt>
									<dd>4763135368</dd>
								</dl>
								<dl>
									<dt>ISBN-13：</dt>
									<dd>978-4763135360</dd>
								</dl>
							<!-- /.book_data --></div>
						</section>
					<!-- /.txt --></div>
				<!-- /.box_col_2 --></div>
				<p>この本は、「血流を増やして、心と体のすべての悩みを解決する方法」を書いた本です。<br>大げさだなあと思われるかもしれませんが、けっしてそんなことはありません。</p>
				<p>血流は、全身にある60兆個の細胞に酸素や栄養を届けるという大切な役割を担っています。<br>さらに、幸せホルモンややる気ホルモンと呼ばれる脳内の神経伝達物質の働きも、血流によって左右されています。</p>
				<p>この人間の心と体を支えている大切な血流が悪くなってしまうと、心身ともに不調が出てしまうのは当然ともいえるのです。<br>逆にいうと、血流を改善すれば、心の悩みも体の悩みもおもしろいように解決していくのです!</p>
				<p>血流をよくするというと、「血液サラサラ」を思い浮かべるひとが多いと思いますが、とくに女性の場合、そもそも血がつくれず、不足しているために血流がよくないというひとがほとんどです。まずは、血をつくれるようにし、血を増やし、その結果血流をよくしていくことが必要です。</p>
			</section>

			<section id="media">
				<div class="h2_basic01">
					<h2>出演情報</h2>
					<span>Media</span>
				<!-- /.h2_basic01 --></div>
				<div class="topics_wrap01">
					<div class="post_items02">
						<div class="post_item02">
						<a href="http://www.otogaku.com/item/868/14.html" target="_blank">
							<p class="post_thumb"><img src="/common/img/about/img_media01.png" alt=""></p>
							<div class="post_meta">
								<div class="cat_label label_media">ゲスト講師としてDVD出演</div>
								<dl>
									<dt class="post_tit">《DVD》心屋塾 Beトレ vol.34「自分を大切にする」</dt>
									<dd class="post_txt">治療をがんばることが必要な時期もあるけれど、一度立ち止まって、自分を大切にすることを、考えてみませんか？ 心屋仁之助さんの心屋塾BeトレDVD 「自分を大切にする」 堀江がゲスト講師としてお話しています。</dd>
								</dl>
								<div class="post_link"><p class="link_basic01"><span>《DVD》心屋塾 Beトレ vol.34「自分を大切にする」を</span>詳しくみる</p></div>
							<!-- /.post_meta --></div>
						</a>
						<!-- /.post_item02 --></div>

					<!-- /.post_items01 col_2 --></div>
				<!-- /.topics_wrap01 --></div>
			</section>
		<!-- /.inner02 --></div>

		<div id="bnr_area">
			<div class="inner02">
				<aside class="bnr_area_recommend">
					<div class="h2_basic01">
						<h2>おすすめコンテンツ</h2>
						<span>Recommend contents</span>
					<!-- /.h2_basic01 --></div>
					<ul>
						<li><a href="/yakuzencha/"><img src="/common/img/bnr_cv01.png" alt="「血流がすべて解決する」著者 堀江昭佳 プロデュース 【漢方・薬膳茶 ご縁授茶（ごえんさずかりちゃ）】 お申し込みはこちら"></a></li>
						<li><a href="/kodakararetreat/"><img src="/common/img/bnr_cv02.png" alt="相談セミナー「子宝リトリート&reg;」 お申し込みはこちら"></a></li>
					</ul>
					<ul>
						<li><div class="btn_recommend01 btn_media01"><a href="/media/">メディア掲載はこちら</a></div></li>
						<li><div class="btn_recommend01 btn_column01"><a href="/column/">コラム記事一覧はこちら</a></div></li>
					</ul>
				</aside>
				<aside class="bnr_area_sns">
					<div class="h2_basic01">
						<h2>関連するSNS</h2>
						<span>Social Networking Service</span>
					<!-- /.h2_basic01 --></div>
					<ul>
						<li><div class="btn_sns01 btn_blog01"><a href="http://ameblo.jp/crouching-tiger/" target="_blank">公式Blogを見る</a></div></li>
						<li><div class="btn_sns01 btn_instagram01"><a href="#" target="_blank">公式Instagramを見る</a></div></li>
						<li><div class="btn_sns01 btn_fb01"><a href="#" target="_blank">公式Facebookを見る</a></div></li>
					</ul>
				</aside>
			<!-- /.inner02 --></div>
		<!-- /#bnr_area --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
