<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>{記事タイトル}｜{カテゴリ名}｜体験談｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="堀江薬局からのお知らせ「{記事タイトル名}」のページです。{記事・リード文70字程度}堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/experiences/category/yyyyMMddHHmmss.php">
<meta property="og:type" content="article">
<meta property="og:title" content="{記事タイトル}｜{カテゴリ名}｜体験談｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/upload/img_post_mainimg.png">
<meta property="og:description" content="堀江薬局からのお知らせ「{記事タイトル名}」のページです。{記事・リード文70字程度}堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/experiences/category/yyyyMMddHHmmss.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/detail.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/experiences.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/topics/",
					"name": "トピックス一覧"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/experiences/",
					"name": "体験談"
				}
			},
			{
				"@type": "ListItem",
				"position": 4,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/experiences/category/",
					"name": "漢方相談"
				}
			},
			{
				"@type": "ListItem",
				"position": 5,
				"item":
				{
					"name": "赤ちゃんを抱っこできて、本当にうれしい！"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g05">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/topics/">トピックス一覧</a></li>
			<li><a href="/experiences/">体験談</a></li>
			<li><a href="/experiences/category/">漢方相談</a></li>
			<li>赤ちゃんを抱っこできて、本当にうれしい！</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<div class="inner01 two_column">
		<div id="contents" class="post_wrap01">
			<main id="main" role="main">
				<div class="h1_basic02 post_exp_title01">
					<div class="cat_label label_kanpo">漢方相談</div>
					<h1>赤ちゃんを抱っこできて、本当にうれしい！</h1>
				<!-- /.h1_basic02 --></div>
				<div class="post_mainimg">
					<img src="/common/img/upload/img_post_mainimg02.jpg" alt="" width="720" height="400">
				<!-- /.post_mainimg --></div>
				<div class="post_header">
					<div class="post_exp_info">
						<ul class="post_exp_data">
							<li>30歳</li>
							<li>東京都</li>
							<li>●●様</li>
						</ul>
						<p class="post_exp_copy">子宮筋腫と闘いながら、元気な女の子を出産。</p>
					<!-- /.post_exp_info --></div>
					<div class="post_exp_lead">
						<p>H.Mさんには、「自分と同じように赤ちゃんができにくく悩んでいる人や、生理痛、生理不順、子宮内膜症・筋腫などの婦人科トラブルでお困りの方に一人でも多く体質改善の大切さを知って欲しい」ということで、体験談の掲載をご了承いただいております。ありがとうございました。</p>
					<!-- /.post_exp_lead --></div>
				<!-- /.post_header --></div>
				<div class="post_contents">
					<section>
						<h2>H2 相談時の状況</h2>
						<p>以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。<br>
深夜11時、12時の残業は当たり前。<br>
食事もお昼に定食やパスタを食べ、あとは、チョコやパン・ドリンク剤で栄養補給。<br>
不眠にも悩まされていました。</p>
						<p><span class="gray">健康診断時、子宮筋腫がみつかり、子宮の傾き具合も奇形があると言われました。</span><br>
もともと不順で生理痛もひどく、冷え性で貧血がちでした。 母も数年前に子宮筋腫で全摘出し、姉も卵巣が交互に腫れたりしていたので、私も婦人系等にネックがあるんだなと感じました。<br>
翌年の健診時には、筋腫が3つに増えていました。<br>
その半年後、生理でもないのに出血し、排卵がうまくできていないと言われました。<br>
そして筋腫が4つになり、うち1つは4㎝大になっていると・・・<br>
年々増える筋腫、そして拡大に不安を感じました。</p>
					</section>
					
					<section>
						<h2>妊活中の嬉しい変化</h2>
						<p>どうしようかと悩みながら、堀江さんのカウンセリングを受け、体質や基礎体温表を見てもらったりして、いくつかの漢方を朝晩服用。<br>
元来、薬嫌いでサプリメントなども続かない私でしたが、黒蜜のように甘く飲みやすかったので無理なく続ける事ができました。</p>
						<p>自分の体質にあわせたアドバイスを受けながら、食事や生活にも気をつけるように心がけると、<span class="yellow">いつも冷たかった手足が気にならなくなり、貧血も改善されてきました。</span></p>
					</section>
					
					<section class="mb50">
						<h2>妊娠・出産</h2>
						<p>そして、 1年くらいたって、不順だった生理も規則正しくなってきた頃、生理が10日程遅れていることに気がつきました。<span class="red">病院に行くと妊娠6週目に入ったところでした。</span><br>
驚きと同時に、とても嬉しかったです。</p>
						<p>もちろん、筋腫があることによって、個人病院での出産ができず、総合病院での出産となりました。<br>
途中、羊水過多・切迫早産になり病院の薬を服用。<br>
後期には貧血で鉄剤を服用しました。<br>
<span class="red">予定日より早まりそうだと言われ続けていたものの、予定日を過ぎ入院。<br>
41週、誘発剤を使うも児心音が弱まり、帝王切開にて出産。</span></p>
						<p>もともと子宮筋腫のこともあり、手術に必要な検査・貯血はしていたのでスムーズに手術へ進みました。<br>
ピンポン玉大の筋腫すれすれに開腹できた為、出血も許容範囲だったようです。<br>
以前、主治医から妊娠しにくいと言われ落ち込んだことがありました。<br>
しかし、今回の出産で、｢子宮筋腫がたくさんあるけど、今回妊娠できたということは、また妊娠できる可能性があるということです。｣と言われ嬉しかったです。</p>
						<p><span class="yellow">あの時、自分の生活をみつめ直すことができて良かった。<br>
そして、漢方を続けて良かったと思いました。</span><br>
今はもうすぐ1歳の娘と家族3人で、幸せで充実した毎日を過ごしています。</p>
					</section>
					
					<div class="box_basic01">
						<section>
							<div class="h2_comment_title01"><h2>堀江昭佳のコメント</h2></div>							<p>コメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメント。</p>
							<p>コメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメントコメント。</p>
						</section>
					<!-- /.box_basic01 --></div>
					
					<section class="box_bnr01 mt40">
						<div class="box_bnr_title01">
							<h2>あなたも、<br>漢方・薬膳茶「ご縁授茶」<br>はじめてみませんか？</h2>
						<!-- /.exp_bnr_title01 --></div>
						<div class="mb60"><a href="#"><img src="/common/img/bnr_goen_tea01.jpg" alt="「血流がすべてを解決する」著者 堀江昭佳プロデュース 漢方・薬膳茶 ご縁授茶（ごえんさずかりちゃ）お申し込みはこちら"></a></div>
						<div>
							<p class="box_bnr_title02">ご相談希望の方はこちら</p>
							<a href="#"><img src="/common/img/bnr_semminor01.jpg" alt="セミナー「子宝リトリート&reg;」お申し込みはこちら"></a>
							</div>
					</section>

				<!-- /.post_contents --></div>
				
				<aside id="related">
					<div class="h2_basic03">
						<h2>関連する体験談</h2>
					<!-- /.h2_basic03 --></div>
					<div class="topics_wrap01">
						<div class="post_items01 col_3">
							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post02.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_experiences">体験談</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
								<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post02.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_experiences">体験談</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
								<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post02.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_experiences">体験談</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
								<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post02.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_experiences">体験談</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
								<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post02.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_experiences">体験談</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
								<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post02.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_experiences">体験談</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
								<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
							</a>
							<!-- post_item01 --></div>
						<!-- /.post_items01 --></div>
					<!-- /.topics_wrap01 --></div>
				<!-- /#related --></aside>
				
				<div class="exp_category_link01 line_top">
				<ul>
					<li><a href="#"><img src="/common/img/experiences/btn_experience01.png" alt="漢方・薬膳茶の体験談" width="180" height="100"></a></li>
					<li><a href="#"><img src="/common/img/experiences/btn_experience02.png" alt="セミナー・リトリートの体験談" width="180" height="100"></a></li>
					<li><a href="#"><img src="/common/img/experiences/btn_experience03.png" alt="漢方相談の体験談" width="180" height="100"></a></li>
				</ul>
			<!-- /.exp_category_link01 --></div>
			
			</main>
		<!-- /#contents --></div>
		<div id="side">
			<aside class="side_popular01">
				<h2>人気の記事</h2>
				<div class="topics_wrap01">
					<div class="post_items01">
						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post01.png" alt="">
								<span class="label_new"><img src="/common/img/label_new01.png" alt="NEW"></span>
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_column">コラム</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">月のリズムと生理周期。月のリズムと生理周期。月のリズムと生…</h3>
							<div class="post_txt">月と女性、そして生理のリズムには不思議な関係があります。日々のくらしの中で新月や満…</div>
						</a>
						<!-- post_item01 --></div>

						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post02.png" alt="">
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_experiences">体験談</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
							<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
						</a>
						<!-- post_item01 --></div>

						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post03.png" alt="">
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_media">メディア掲載</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">LEE 2017．1月号掲載</h3>
							<div class="post_txt">この不調、貧血ですか？〜くすみ、下半身太り、疲れやすい〜6ページに渡って、医師の方との特…</div>
						</a>
						<!-- post_item01 --></div>

						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post04.png" alt="">
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_info">お知らせ</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">『血流』＆『ほめ達』コラボ講演会開催…</h3>
							<div class="post_txt">ストレスをチカラに変える。「からだ」と「心」が軽くなる『血流』＆『ほめ達』コラボ講…</div>
						</a>
						<!-- post_item01 --></div>

						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post05.png" alt="">
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_faq">よくある質問</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">何度も治療してるのに成功しないんです・・・</h3>
							<div class="post_txt">実は不妊治療は、決して成功率の高い治療ではありません。一般に人工授精で１０％、体外…</div>
						</a>
						<!-- post_item01 --></div>
					<!-- /.post_items01 --></div>
				<!-- /.topics_wrap01 --></div>
			</aside>

			<aside class="side_list01">
				<h2>トピックス一覧</h2>
				<ul>
					<li><a href="#">コラム</a></li>
					<li><a href="/info/">お知らせ</a></li>
					<li><a href="#">メディア掲載</a></li>
					<li class="current"><a href="#">体験談</a></li>
					<li><a href="#">よくある質問</a></li>
				</ul>
			</aside>
		<!-- /#side --></div>
	<!-- /.inner01 --></div>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<!-- LINEで送るボタン -->
<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
<!-- Tweetボタン -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
