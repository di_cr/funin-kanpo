<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>不妊漢方体験談｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="堀江薬局を訪れた方々の体験談をご紹介しています。堀江薬局では880人以上の方から妊娠のご報告をいただき、その中から一部の体験談を掲載しています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/experiences/">
<meta property="og:type" content="article">
<meta property="og:title" content="不妊漢方体験談｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="堀江薬局を訪れた方々の体験談をご紹介しています。堀江薬局では880人以上の方から妊娠のご報告をいただき、その中から一部の体験談を掲載しています。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/experiences/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/category.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/experiences.css" media="all">
<link rel="next" href="http://www.funin-kanpo.com/experiences/index_2.php">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/topics/",
					"name": "トピックス一覧"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "体験談"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g05">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/topics/">トピックス一覧</a></li>
			<li>体験談</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
		<div class="h1_basic01 inner01">
			<h1>体験談</h1>
			<span>Experiences</span>
		<!-- /.h1_basic01 --></div>

		<div class="topics_wrap01 inner01">
			<div class="txt_lead01 exp_lead01 mb30">
				<p>みなさんから届いた喜びの声を掲載しています。</p>
				<p class="exp_txt_entry01"><a href="#" class="link_basic01">体験談の投稿はこちら</a></p>
			<!-- /.txt_lead01 --></div>

			<div class="exp_category_link01">
				<ul>
					<li><a href="#"><img src="/common/img/experiences/btn_experience01.png" alt="漢方・薬膳茶の体験談" width="180" height="100"></a></li>
					<li><a href="#"><img src="/common/img/experiences/btn_experience02.png" alt="セミナー・リトリートの体験談" width="180" height="100"></a></li>
					<li><a href="#"><img src="/common/img/experiences/btn_experience03.png" alt="漢方相談の体験談" width="180" height="100"></a></li>
				</ul>
			<!-- /.exp_category_link01 --></div>

			<div class="post_exp_items01">
				<article class="post_exp_item01">
				<a href="#">
					<div class="post_exp_thumb">
						<img src="/common/img/upload/img_post06.png" alt="">
					<!-- /.post_exp_thumb --></div>
					<div class="post_exp_body">
						<div class="post_exp_meta">
							<div class="cat_label label_kanpo-tea">漢方・薬膳茶</div>
						<!-- /.post_exp_meta --></div>
						<h2 class="post_exp_tit">2袋飲み終わる頃、授かることができました！</h2>
						<ul class="post_exp_data">
							<li>30歳</li>
							<li>東京都</li>
							<li>Ｈ.Ｍ様</li>
						</ul>
						<div class="post_exp_txt">
							<p><span class="yellow">赤ちゃんを授かりたく、色々なことを行ったりサプリメントを飲んでました。</span><br>
		知人より「出雲大社の近くに子宝のお茶が売ってるよ！」と教えていただき、インターネットで見つけて購入できました★とても美味しく、ほっとする味です。</p>
						<!-- /.post_exp_txt --></div>
					<!-- /.post_exp_body --></div>
				</a>
				</article>

				<article class="post_exp_item01">
				<a href="#">
					<div class="post_exp_thumb">
						<img src="/common/img/upload/img_post07.png" alt="">
					<!-- /.post_exp_thumb --></div>
					<div class="post_exp_body">
						<div class="post_exp_meta">
							<div class="cat_label label_seminar">セミナー・リトリート</div>
						<!-- /.post_exp_meta --></div>
						<h2 class="post_exp_tit">早くだんなさんと会いたいと思いました（笑） 来て良かったです。</h2>
						<ul class="post_exp_data">
							<li>●●歳</li>
							<li>●●県</li>
							<li>Y.F様</li>
						</ul>
						<div class="post_exp_txt">
							<p><span class="yellow">子宮筋腫と闘いながら、元気な女の子を出産。</span><br>
		赤ちゃんを授かりたく、色々なことを行ったりサプリメントを飲んでました。<br>
		知人より「出雲大社の近くに子宝のお茶が売ってるよ！」と教えていただき、インターネットで見つけて購入できました★とても美味しく、ほっとする味です。</p>
						<!-- /.post_exp_txt --></div>
					<!-- /.post_exp_body --></div>
				</a>
				</article>

				<article class="post_exp_item01">
				<a href="#">
					<div class="post_exp_thumb">
						<img src="/common/img/upload/img_post02.png" alt="">
					<!-- /.post_exp_thumb --></div>
					<div class="post_exp_body">
						<div class="post_exp_meta">
							<div class="cat_label label_kanpo">漢方相談</div>
						<!-- /.post_exp_meta --></div>
						<h2 class="post_exp_tit">赤ちゃんを抱っこできて、本当にうれしい！</h2>
						<ul class="post_exp_data">
							<li>30歳</li>
							<li>東京都</li>
							<li>●●様</li>
						</ul>
						<div class="post_exp_txt">
							<p><span class="yellow">子宮筋腫と闘いながら、元気な女の子を出産。</span><br>
		以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。 深夜11時、12時の残業は当たり前。 食事もお昼に定食やパスタを食べ、あとは、チョコやパン・ドリンク剤で栄養補給。 不眠にも悩まされていました。</p>
						<!-- /.post_exp_txt --></div>
					<!-- /.post_exp_body --></div>
				</a>
				</article>
			<!-- /.post_exp_items01 --></div>

			<div class="btn_exp_more">
				<p class="btn_basic01"><a href="index_2.php" class="pagination__next">さらに体験談を見る</a></p>
			<!-- /.btn_exp_more --></div>

			<div class="page-load-status">
				<div class="loader-ellips infinite-scroll-request">読み込み中</div>
				<p class="infinite-scroll-error">記事が読み込めませんでした</p>
			<!-- /.page-load-status --></div>
		<!-- .topics_wrap01 --></div>
	</main>
<!-- /#wrapper --></div>



<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<script type="text/javascript" src="/common/js/infinite-scroll.pkgd.min.js"></script>
<script type="text/javascript" src="/common/js/infinite_setting.js"></script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
