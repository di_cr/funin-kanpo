<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>お問合せ｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="お問い合わせについてのページです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/contact/">
<meta property="og:type" content="article">
<meta property="og:title" content="お問合せ｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="お問い合わせについてのページです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/contact/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/contact.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "お問合せ"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li>お問合せ</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
		<div class="h1_basic01 inner01">
			<h1>お問合せ</h1>
			<span>Contact</span>
		<!-- /.h1_basic01 --></div>

		<div class="inner02">
			<section class="sec_notice01">
				<h2>お問合せ受付中止のお詫び</h2>
				<p>大変申し訳ございませんが、現在、堀江昭佳による<span class="gray">漢方相談は希望者多数のため、キャンセル待ちを含めて受付を中止</span>しております。また、個別の体調、症状についてのお問い合わせについて、回答を行っておりませんので、ご了承くださいませ。</p>
			</section>

			<section class="sec_notice02">
				<h2>よくある質問もご覧ください</h2>
				<p>よくお問い合わせを受ける質問への回答を掲載しておりますので是非ご覧ください。<br><a href="/faq/" class="link_basic01">よくある質問はこちら</a></p>
			</section>

			<section>
				<div class="h2_basic02">
					<h2>直接会って相談したい方へ</h2>
					<p>セミナー「子宝リトリート」に参加しませんか？<br>子宝リトリートとは、「仕事や家庭 などの日常生活を離れ、自分だけの時間や人間関係にひたる場所」のこと。</p>
					<p>話を聞くだけでなく、五感で体験することで、すこやかな心と体を手に入れる。<br>たくさんの学びと体験をひとつにした 滞在型セミナーです。</p>
					<p class="taC"><a href="/kodakararetreat/"><img src="/common/img/contact/bnr_seminar01.png" alt="セミナー「子宝リトリート&reg;」に参加しませんか？ お申し込みはこちら" width="660" height="200"></a></p>
				<!-- /.h2_basic03 --></div>
			</section>

			<section>
				<div class="h2_basic02">
					<h2>お電話での来店予約</h2>
					<p><em>当社スタッフがお受けします。<br>初回のご相談は、電話では行っておりません。</em></p>
					<p>遠方の方の場合、2回目以降のご相談は電話にて行っています。<br>以下の電話番号よりお問合せください。</p>
					<p>電話番号：0853-25-8704<br>受付時間：10:00&#xFF5E;17:00（日曜・祝日定休）</p>
				<!-- /.h2_basic03 --></div>
			</section>

		<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<script type="text/javascript" src="/common/js/map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCe-6qErsU9FfCecbcK6rgpd8Et5UyfS4A&callback=initMap1"></script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
