<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>3.次の妊娠へ 流産の予防法・流産を乗り越えて次に妊娠する方法｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="赤ちゃんを失った悲しみやつらさは、簡単に癒やされるものではないでしょう。しかし、流産には様々なケースがあり、妊娠したことのある女性の約40％が流産を経験しているという調査結果もあるという事実を知っていただけたらと思います。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/ryuzan/prevent-miscarriage.php">
<meta property="og:type" content="article">
<meta property="og:title" content="3.次の妊娠へ 流産の予防法・流産を乗り越えて次に妊娠する方法｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="赤ちゃんを失った悲しみやつらさは、簡単に癒やされるものではないでしょう。しかし、流産には様々なケースがあり、妊娠したことのある女性の約40％が流産を経験しているという調査結果もあるという事実を知っていただけたらと思います。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/ryuzan/prevent-miscarriage.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "流産について"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "3.次の妊娠へ 流産の予防法・流産を乗り越えて次に妊娠する方法"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g04">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/ryuzan/">流産について</a></li>
			<li>3.次の妊娠へ 流産の予防法・流産を乗り越えて次に妊娠する方法</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1><span class="heading">3.次の妊娠へ</span>「流産を乗り越え授かる方法」と<br>「流産の予防法」</h1>
		<!-- /.h1_basic01 --></div>

		<div id="prevent-miscarriage">
			<p>いま、これを読まれている時は、そんな気持ちになれないかもしれませんが、このことは、最初に言っておきます。</p>
			<p><span class="yellow">「あなたは妊娠できる」</span></p>
			<p>そのことに自信をもってください。流産して、赤ちゃんは天使になって空に帰って行ったかもしれません。でも、あなたに「妊娠できるよ」って教えてくれているのです。だから、自分に自信をもってくださいね。そのうえで、次の妊娠のために体を整える方法をお伝えします。</p>
			<div class="list_basic01 list_num01">
				<ul>
					<li>あたためる</li>
					<li>血を増やす</li>
				</ul>
			<!-- /.list_num01 --></div>
			<p class="mt15">この2つです。</p>
			<p>基本的には、流産後のケア、養生法と同じです。（<a href="/ryuzan/prevent-miscarriage.php" class="link_basic01">流産後のケア詳細はこちら</a>）ただ、流産が体にダメージを与えているのは間違いないので、早く体をもとに戻し、元気な赤ちゃんを授かっていただくためにも、できれば漢方や薬膳を利用してもらえたらと思います。自分のこころとからだをケアしていくことは、自分を大切にすることなのです。</p>

			<section>
				<div class="h2_basic01">
					<h2>流産・不育症のための<br>体質改善・妊娠しやすい体づくり</h2>
				<!-- /.h2_basic01 --></div>
				<p>現代医学では、まだまだ解明されていないことの多い習慣性流産・不育症です。しかし、漢方では古来より、体質改善をすることによって流産を防ぐ方法が伝えられています。<br><span class="yellow">流産・不育症のための体質改善には、『冷え』、『血流』、『育てる力』という3つのポイントがあります。</span></p>
				<section>
					<div class="h3_basic03">
						<h3>『冷え』</h3>
					<!-- /.h3_basic03 --></div>
					<p>おなかを触った時に冷たい。おしりや太もも、腰回りが冷えている。<br>そんな場合は、子宮が冷えている可能性が高くなります。<br>人間の体は冷えると働きが低下しますが、子宮も同じです。冷えによって、本来の赤ちゃんを育てる力が低下してしまいます。<br>昔から、妊娠した時には、おなかを冷やさないようにと言われるのは、こうした理由があるのです。</p>
					<p>『冷え』の改善には補陽薬とよばれる漢方を中心に使います。<br><span class="yellow">漢方では、温める力のことを『陽』といいます。体を温めることで、子宮や卵巣を元気にしていきます。温かい子宮は、赤ちゃんにとって心地よいベッドなのです。</span></p>
				</section>
				<section>
					<div class="h3_basic03">
						<h3>『血流』</h3>
					<!-- /.h3_basic03 --></div>
					<p>血がしっかりと流れていることは、子宮・卵巣系にとって非常に重要です。<br>赤ちゃんのベッドである子宮内膜は、毛細血管の集まりですし、卵巣は、もともと非常に血行がわるい臓器なのです。<br>また、不育症・習慣性流産の50％近くを占める、抗リン脂質抗体や凝固系異常では、血流が悪くなり赤ちゃんとお母さんをつなぐ血管がつまってしまうことが、流産の直接的な原因となっています。</p>
					<p>漢方では、血の流れをよくすることを活血といい、血を補うことを補血といい、『血流』に改善するには活血薬・補血薬とよばれる漢方を中心に使います。<br>血には、赤ちゃんに、栄養をしっかりと届ける働きがあります。<br><span class="yellow">「子宮は血の海」という言葉が漢方にはあり、血がたくさん届くことで、子宮・卵巣に力を与え、妊娠をする、そして無事な出産へ導くとされています。</span></p>
				</section>
				<section>
					<div class="h3_basic03">
						<h3>『育てる力』</h3>
					<!-- /.h3_basic03 --></div>
					<p>漢方では、育てる力を『腎』といい、妊娠力のひとつとして非常に重視しています。<br>ホルモンや免疫系のはたらきとも深く関わっています。特に、不育症・習慣性流産では、自己免疫の異常が非常に大きく、影響します。</p>
					<p>『育てる力』を高めるには補腎薬とよばれる漢方を中心に使います。<br>漢方でいう「腎」は、生殖、ホルモン、免疫という働きを意味します。そう。流産・不育症に深く関わることばかりなのです。<br><span class="yellow">漢方では、「赤ちゃんは腎の糸で引っ張られている」という言葉があります。</span><br><span class="yellow">腎の力が、流産しないように、赤ちゃんをおなかの中にとどめてくれているという意味です。</span></p>
				</section>
			</section>

			<section>
				<div class="h2_basic01">
					<h2>あたためる理由</h2>
				<!-- /.h2_basic01 --></div>
				<p>子宮。</p>
				<p>そこは、精子と卵子が出会って受精したり、赤ちゃんが育つ10ヶ月をすごす大切な場所です。</p>
				<p>実は、体の中で非常に冷えやすい場所でもあります。<br><span class="gray">熱というのは、上へ上へと昇っていく性質がありますが、子宮は内臓の中の一番下にあるためです。足で血液は冷やされて、冷やされた血液は、大腿静脈となって子宮のすぐ側を通ります。そのため、足が冷たい人は、子宮も冷えやすいのです。</span></p>
				<p>人間の体は、体温が1度下がると</p>
				<div class="list_basic01 mt15">
					<ul>
						<li>基礎代謝　約12％下がる</li>
						<li>免疫力　約30％下がる</li>
					</ul>
				<!-- /.ul_basic01 --></div>
				<p class="mt15">と言われています。</p>
				<p>受精卵が、精子と卵子が出会うと受精して、子宮の内側の子宮内膜に着床します。<br>これが妊娠。</p>
				<p>でも、着床するときに冷えてると着床しにくいのです。<br>基礎体温に高温期があるのは、そのためです。<br>高温期は36.7度必要です。</p>
				<p>体をあたためることは、とても大切なのです。</p>
				<p>（注意！　流産後に体温が高くなってしまっている人は、必ずしもあたためることがよくない場合もあるので、ご相談ください）</p>
			</section>

			<section>
				<div class="h2_basic01">
					<h2>血を増やす理由</h2>
				<!-- /.h2_basic01 --></div>
				<p>「妊娠力＝血」です。</p>
				<p>これは、東洋医学で妊娠を考える時に最も大切なことです。<br>東洋医学での血は、血液だけでなく、栄養やホルモンも含んだものを指します。<br>「子宮は血の海」とも言われるほど、血は、妊娠力そのものなのです。</p>
				<p>流産＝血のダメージであり、血が作りにくくなる不足する流れがわるくなるということが引き起こされます。<br>これが、流産後の心や体の不調や不妊を招いてしまうのです。</p>
				<p>そのために、血の状態を回復させることを最優先していきます。</p>

				<section>
					<div class="h3_basic03">
						<h3>基礎体温、症状からみる流産</h3>
					<!-- /.h3_basic03 --></div>
					<p>漢方では流産しやすい体質を、基礎体温表の形や症状からも判断することができます。<br>ほとんどの流産は偶然ですが、自分の体質を知って、流産をしないように、なるべく予防していきましょう。</p>
					<section class="box_description01">
						<h4 class="tit">A. 低温期が長い</h4>
						<p><img src="/common/img/ryuzan/img_prevent01.png" alt="月経周期に応じた基礎体温のグラフ（低温期が長い場合）" width="660" height="240"></p>
						<p class="mt15">低温期は、卵胞が育つ時期。通常は生理初日から14日目くらいで高温期になります。<br>漢方では、低温期が長いということは、「育てる力（陽）」が足りていないと判断します。卵胞の発育に時間がかかっている状態です。クロミフェン（クロミッド、セロフェン）やhMG注射などを用いても、なかなか卵が育たない人も多く、この場合は、特に育てる力が低下していることが考えられます。</p>
						<p>漢方では、育てる力は、「腎」と「血」。このふたつは、妊娠してからも、元気におなかの赤ちゃんを育てていくのに欠かせない力です。習慣性流産・不育症の方は、特に補っておきたい要素です。</p>
						<p>漢方では、補腎陽薬や補血薬を補うことで、「育てる力（陽）」を高めていきます。</p>
					</section>
					<section class="box_description01">
						<h4 class="tit">B. 基礎体温の波動が激しい</h4>
						<p><img src="/common/img/ryuzan/img_prevent02.png" alt="月経周期に応じた基礎体温のグラフ（基礎体温の波動が激しい場合）" width="660" height="240"></p>
						<p class="mt15">ストレスが多く、自律神経が不安定な方に多い形です。また、ホルモンバランスが不安定な方も多く、高プロラクチン血症や多嚢胞性卵巣症候群（PCOS）のひとにもよく見られます。<br>ストレスが多く、自律神経が不安定な場合、不安を感じやすい人が少なくありません。特に、習慣性流産・不育症の方は、「また、流産するのではないか」という不安から、妊娠後、極度な緊張を感じやすくなります。緊張を感じると血管が収縮して、赤ちゃんへの血行が悪くなるだけでなく、子宮の収縮にもつながりかねません。<br>漢方では、あたため、血を増やしていくとともに、寧心安神（ねいしんあんじん）といって、緊張を和らげて、ここをを落ち着け、自律神経を安定させていくサポートを行います。</p>
					</section>
					<section class="box_description01">
						<h4 class="tit">C. 全体的に体温が高い、周期が短い</h4>
						<p><img src="/common/img/ryuzan/img_prevent03.png" alt="月経周期に応じた基礎体温のグラフ（全体的に体温が高い、周期が短い場合）" width="660" height="240"></p>
						<p class="mt15">体温が高いことは、よいことだと思われがちですが、低温期が36.5°、高温期が37°をいつも超えている場合は、注意が必要です。<br>卵子が大きくなる低温期には適温があります。</p>
						<p>にわとりが卵をあたためるのに例えると、「36°を下回るのは卵が冷蔵庫に入っている状態、36.5°を越えるのは、ゆで卵になっている」ようなものです。<br>漢方では、低温期を陰、高温期を陽ととらえます。<br>からだの「育てる力（陰）」が不足すると、全体的に体温が高くなってしまうのです。<br>特に気をつけたいのは、流産を機に体温が高くなってしまったケースです。流産の影響で「育てる力（陰）」が失われてしまった影響と捉えます。不育症・習慣性流産などで、流産をくりかえされた経験がある場合、「育てる力（陰）」の消耗が強くなるので、特に気をつけましょう。</p>
						<p>漢方では、補腎陰薬をもちいることで、「育てる力（陰）」を補っていきます。</p>
					</section>
					<section class="box_description01">
						<h4 class="tit">D. 不正出血が多い、おりものが多い</h4>
						<p><img src="/common/img/ryuzan/img_prevent04.png" alt="月経周期に応じた基礎体温のグラフ（不正出血が多い、おりものが多い場合）" width="660" height="280"></p>
						<p class="mt15">不正出血や、おりものが多い人は、「守る力」が足りていない状態です。本来、体の中にとどめておきたいものが、外にでてしまっている状態です。妊娠した時にも同じことが言え、「守る力」が弱い人は、切迫流産になるケースが少なくありません。<br>胃腸が弱く、やせている人は「守る力」が弱い傾向があるので注意が必要です。</p>
						<p>漢方では、補気安胎（ほきあんたい）といって、胎児をおなかのなかにとどめておく力を高めます。<br>この補気安胎は、体質を問わず流産予防の方法として、昔からよく漢方では使われてきました。</p>
					</section>
				</section>
			</section>
		<!-- /#prevent-miscarriage --></div>

		<div class="category_link01">
			<ul>
				<li><a href="/ryuzan/no-worries.php">1.  はじめに 流産は決して特別なことではありません。</a></li>
				<li><a href="/ryuzan/right-knowledge.php">2. 流産のタイプ 流産には、さまざまなタイプがあります。</a></li>
				<li><a href="/ryuzan/prevent-miscarriage.php">3. 次の妊娠へ 「流産を乗り越え授かる方法」と「流産の予防法」</a></li>
				<li><a href="/ryuzan/after-care.php">4. 流産後のケア もし流産をしたら、その後の手当てが大切です。</a></li>
			</ul>
		</div>

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
