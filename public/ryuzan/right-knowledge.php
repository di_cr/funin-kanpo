<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>2.流産のタイプ｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="赤ちゃんを失った悲しみやつらさは、簡単に癒やされるものではないでしょう。しかし、流産には様々なケースがあり、妊娠したことのある女性の約40％が流産を経験しているという調査結果もあるという事実を知っていただけたらと思います。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/ryuzan/right-knowledge.php">
<meta property="og:type" content="article">
<meta property="og:title" content="2.流産のタイプ｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="赤ちゃんを失った悲しみやつらさは、簡単に癒やされるものではないでしょう。しかし、流産には様々なケースがあり、妊娠したことのある女性の約40％が流産を経験しているという調査結果もあるという事実を知っていただけたらと思います。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/ryuzan/right-knowledge.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "流産について"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "2.流産のタイプ"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g04">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/ryuzan/">流産について</a></li>
			<li>2.流産のタイプ</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1><span class="heading">2.流産のタイプ</span>流産には、さまざまなタイプがあります。</h1>
		<!-- /.h1_basic01 --></div>

		<div id="right-knowledge">
			<p>正しい知識を知ってください。知ることは安心につながり、次に進んで行く力になります。</p>
			<section>
				<div class="h2_basic01">
					<h2>流産とは</h2>
				<!-- /.h2_basic01 --></div>
				<div class="box_quote02">
					<p>妊娠したにもかかわらず、妊娠22週未満より前に、妊娠が終わることを「流産」と言います。（日本産婦人科学会の定義による）<br>23週目以降の場合は、「死産」といいます。</p>
				<!-- /.box_description02 --></div>
				<p>流産と聞くと特別な事のように感じるかもしれませんが、<span class="gray">すべての妊娠のうち約15％が流産するといわれ、妊娠したことのある女性の約40％が流産を経験しているという厚生労働省の調査結果もあります。</span></p>
				<p>これは、妊娠が確定診断された後の流産ですので、自覚しないままに流産に至っているものも含めるとかなりの割合になると推定されています。<br>ですので、<span class="yellow">流産は決して特別なことではありませんが、デリケートな問題でもあり、口にする人が多くはないので、あまり耳にすることがないのが実情です。</span></p>
				<p>また一方で、流産を経験している人は多い。<br>「特別なことではない。」と聞かされても、赤ちゃんを失った悲しみや、つらさは、簡単に癒やされるものでもないでしょう。</p>
				<p>ただ、その事実は、知っておいていただけたらと思います。</p>
			</section>

			<section>
				<div class="h2_basic01">
					<h2>原因はなに？</h2>
				<!-- /.h2_basic01 --></div>
				<p><span class="gray">ほとんどの場合は、「偶然」です。</span></p>
				<p>特に早期の流産の大部分は、卵子や精子の染色体異常が原因とされます。<br>これも、夫婦すべての精子と卵子に異常があるというわけではなく、たくさんある精子と卵子のうち、その時に出会った1つの卵子、精子に、たまたま異常があったという意味です。</p>
				<p>妊娠初期のお母さんの仕事や運動などが原因で流産することは、ほとんどありません。<br>お母さんが、自分を責める必要はないのです。</p>
				<section class="box_description02">
					<h3 class="tit">〈染色体異常について〉</h3>
					<p>この染色体異常ですが、実は、排卵した卵子のうち4個に1個（約25％）は染色体異常があるといわれています。この染色体異常は、年齢とともに上昇します。<br>精子のほうは、約10％に染色体異常があるといわれています。</p>
					<p>そして、全く問題ない精子と卵子が出会い、受精してからも、染色体の異常が起きるといわれています。</p>
				</section>
				<p class="mt15">「異常」というと、なんだか特別の事のように聞こえますが、受精卵の染色体異常率は、なんと40％と言われます。<br>誰にでも起きている、一般的なことなのです。</p>
				<p>一方で、これだけ染色体などの異常があるにもかかわらず、新生児のうち、なんらかの染色体異常を持つ赤ちゃんは0.6％といわれています。</p>
				<p>なぜなのでしょう？</p>
				<p><span class="yellow">実は、流産とは、赤ちゃんとお母さんを守るための自然の仕組みでもあります。</span></p>
				<p><span class="yellow">重大な異常がある場合。<br>赤ちゃんは、自らの意志で育つのをやめて、流れていくのです。</span></p>
			</section>

			<section>
				<div class="h2_basic01">
					<h2>流産の種類</h2>
				<!-- /.h2_basic01 --></div>
				<p>流産には時期や状態などによって、それぞれに名前があります。</p>
				<section class="box_description02">
					<h3 class="tit">〈時期による分類〉</h3>
					<div class="dl_basic01">
						<dl>
							<dt>初期流産：</dt>
							<dd>妊娠12週未満で赤ちゃんがまた人間の形をしていない胎芽のころに起こります。<br>流産のおよそ90～95％がこの時期に起きるといわれます。<br>ほとんどが赤ちゃんの側に原因があります。</dd>

							<dt>後期流産：</dt>
							<dd>妊娠12週以降22週未満の時期の流産です。中期流産とも言います。</dd>
						</dl>
					<!-- /.dl_basic01 --></div>
				</section>
				<section class="box_description02">
					<h3 class="tit">〈症状による分類〉</h3>
					<div class="dl_basic01">
						<dl>
							<dt>稽留流産：</dt>
							<dd>すでに子宮の中で赤ちゃんが亡くなっていて、出血・腹痛などの症状がない状態です。<br>自覚症状がないため、超音波による検査で初めて確認されます。<br>いつ出血が始まり、進行流産になるかわからない状態です。</dd>

							<dt>進行流産：</dt>
							<dd>出血や腹痛といった症状が出て、子宮が収縮をはじめて、赤ちゃんが外に出てきている状態です。<br>残念ながら、この状態を止める方法はありません。</dd>

							<dt>完全流産：</dt>
							<dd>子宮の中にあった赤ちゃんや胎嚢が、すべて外に出た状態です。<br>出血、腹痛などはおさまっていることが多く、経過観察となることが多いようです。</dd>

							<dt>不全流産：</dt>
							<dd>赤ちゃんや胎嚢が、完全には出ずに、子宮の中に残っている状態です。<br>出血、腹痛がつついていることが多く、手術をして、子宮の中に残っている組織を取り除きます。</dd>

							<dt>切迫流産：</dt>
							<dd>赤ちゃんが子宮の中にとどまっていて、流産の手前の状態です。<br>出血や腹痛という流産のような症状がありますが、必ずしも流産に進んでしまうわけではありませんが、妊娠の状態が不安定になっています。<br>妊娠12週までの初期では、有効な薬剤はないとされていて、経過観察で様子をみます。</dd>

							<dt>感染流産：</dt>
							<dd>細菌などの感染を伴った流産です。</dd>
						</dl>
					<!-- /.dl_basic01 --></div>
				</section>
				<section class="box_description02">
					<h3 class="tit">〈その他の流産など〉</h3>
					<div class="dl_basic01">
						<dl>
							<dt>習慣流産：</dt>
							<dd>3回以上流産を繰り返す場合を指します。<br>この場合は、なんらかの原因があることも考えられるので、病院で検査をすることをおすすめします。<br>ただ、実際に原因がはっきりとわかるのは、半数以下です。</dd>

							<dt>不育症：</dt>
							<dd>原因は特定できないけれども、流産を繰り返したり、子宮内で赤ちゃんが亡くなってしまって、生きた赤ちゃんを出産できないことをいいます。</dd>

							<dt>化学流産：</dt>
							<dd>
								<p class="mt00">妊娠反応で陽性は出ても、胎嚢が確認できない時期に流産してしまうものです。<br>一般的な流産には含まれません。<br>生理予定日前にフライングで検査をすることによって生じます。<br>統計などはありませんが、かなり頻繁に起きています。</p>
								<p>それは、着床後、2～4週の早期の流産は非常に多く、この時期の流産を含めると流産率は50％にもなると推定されるからです。</p>
								<p>昔は、生理が遅れたかな？<br>くらいの認識であったのが、妊娠検査薬の感度が高くなって、早い時期に陽性反応を出すことができるようになったことも、化学流産を自覚される方が増える一因になっています。</p>
							</dd>

							<dt>胞状奇胎：</dt>
							<dd>受精卵から胎盤になるはずだった絨毛組織（じゅうもうそしき）が、正常に発育せずに異常増殖して、ぶどうの粒のようになってしまう病気です。</dd>

							<dt>子宮外妊娠：</dt>
							<dd>受精卵が子宮内腔以外の場所に着床して、発育することを子宮外妊娠といいます。<br>妊娠全体の約1％に起きるといわれます。<br>子宮外妊娠がわかったら、早急に処置が必要になります。<br>通常、流産には含まれませんが、術後は、流産と同じような養生やケアが必要です。</dd>
						</dl>
					<!-- /.dl_basic01 --></div>
				</section>
			</section>
		<!-- /#right-knowledge --></div>

		<div class="category_link01">
			<ul>
				<li><a href="/ryuzan/no-worries.php">1.  はじめに 流産は決して特別なことではありません。</a></li>
				<li><a href="/ryuzan/right-knowledge.php">2. 流産のタイプ 流産には、さまざまなタイプがあります。</a></li>
				<li><a href="/ryuzan/prevent-miscarriage.php">3. 次の妊娠へ 「流産を乗り越え授かる方法」と「流産の予防法」</a></li>
				<li><a href="/ryuzan/after-care.php">4. 流産後のケア もし流産をしたら、その後の手当てが大切です。</a></li>
			</ul>
		</div>

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
