<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>流産について｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="「流産について」のページです。妊娠する力はあるのに、流産後の体の回復が足りないことで、授かりにくくなってしまうことがあります。妊娠しやすい体づくりをして、不育症や習慣性流産を乗り越えていく力をつけていきましょう。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/ryuzan/">
<meta property="og:type" content="article">
<meta property="og:title" content="流産について｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="「流産について」のページです。妊娠する力はあるのに、流産後の体の回復が足りないことで、授かりにくくなってしまうことがあります。妊娠しやすい体づくりをして、不育症や習慣性流産を乗り越えていく力をつけていきましょう。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/ryuzan/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "流産について"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g04">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li>流産について</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1>流産について</h1>
			<span>About Misbirth</span>
		<!-- /.h1_basic01 --></div>

		<div id="ryuzan">
			<div class="txt_lead02">
				<p>子宝を中心に婦人科専門の漢方相談を受けているのですが、最近、増えているのが流産（稽留流産、切迫流産など）や不育症の相談です。</p>
			<!-- /.txt_lead02 --></div>
			<p>一度授かった命を失うことが、どれだけつらいことか・・・</p>
			<p>妊娠する力はあるのに、<span class="gray">流産後の体の回復をしっかりとしないことで、授かりにくくなってしまう。</span><br>そんな女性をたくさんみてきました。</p>
			<p>少しでも多くの方が、自分自身の体についてもっと深く知っていただきたいと思います。<br>西洋医学的な原因のある不育症や習慣性流産などは、病院での治療も必要ですが、<span class="yellow">同時に、赤ちゃんを授かる環境をしっかりと整えて行くことも大切です。</span><br>妊娠しやすい体づくりをすることで、不育症や習慣性流産を乗り越えていく力をつけていきましょう。</p>

			<section class="box_basic01">
				<div class="contents_link01">
					<h2>CONTENTS</h2>
					<ul>
						<li><a href="/ryuzan/no-worries.php"><span>1. はじめに</span> 流産は決して特別なことでは<br class="pc">ありません。</a></li>
						<li><a href="/ryuzan/right-knowledge.php"><span>2. 流産のタイプ</span> 流産には、<br class="pc">さまざまなタイプがあります。</a></li>
						<li><a href="/ryuzan/prevent-miscarriage.php"><span>3. 次の妊娠へ</span> 「流産を乗り越え授かる方法」と<br class="pc">「流産の予防法」</a></li>
						<li><a href="/ryuzan/after-care.php"><span>4. 流産後のケア</span> もし流産をしたら、<br class="pc">その後の手当てが大切です。</a></li>
					</ul>
				<!-- /.contents_link01 --></div>
			</section>

			<section class="sec_angel01">
				<h2>天使になった赤ちゃん</h2>
				<div class="angel_inner01">
					<p>授かった新しい生命。<br>その生命が失われてしまった時。</p>
					<p>言いようのない悲しみに襲われたり、<br>自分を責めてしまったり<br>もしかすると、また…<br>という不安に包まれていたりするかもしれません。</p>
					<p>まず、知ってほしいのは、<br>流産は決して特別なことではないということです。</p>
				<!-- /.angel_inner01 --></div>
			<!-- /.sec_angel01 --></section>

			<section class="box_description02">
				<h2 class="tit">流産について</h2>
				<p>妊娠反応が陽性で、子宮内に胎児または胎嚢が確認された後、その成長が停止した状態、つまり胎児が死亡した状態で、妊娠22週までに赤ちゃんが母体から外に出てしまうことをいいます。自然流産は、全妊娠の約10&#xFF5E;15％にみられます。</p>
			<!-- /.box_description02 --></section>

			<section>
				<div class="h2_basic01">
					<h2>流産・不育症の原因</h2>
				<!-- /.h2_basic01 --></div>
				<section>
					<div class="h3_basic03">
						<h3>習慣性流産・不育症には、原因がいくつかあります。</h3>
					<!-- /.h3_basic03 --></div>
					<div class="dl_basic01">
						<dl>
							<dt>1. 血液が固まりやすい（抗リン脂質抗体、凝固系異常）</dt>
							<dd>お母さんと赤ちゃんをつなぐ血管が、血が固まることによって詰まってしまいます。<br>そのために、酸素・栄養が届かず流産に至ります。</dd>
						</dl>
						<dl>
							<dt>2. 内分泌系の異常（甲状腺ホルモン、血糖値など）</dt>
							<dd>ホルモンバランスが崩れているために、流産を引き起こしてしまいます。</dd>
						</dl>
						<dl>
							<dt>3. 子宮の奇形</dt>
							<dd>流産を引き起こす子宮奇形には、中隔子宮、双角子宮などがあります。<br>子宮形成の手術を行うことで、流産を防ぎ、妊娠・出産の可能性を高めることができます。</dd>
						</dl>
						<dl>
							<dt>4. 染色体異常</dt>
							<dd>夫婦どちからに、転座と呼ばれる染色体異常がある場合は、高い確率で流産が起こりやすくなります。<br>残念ながら根本的な治療法はありませんが、着床前診断という方法が広まりつつあります。</dd>
						</dl>
						<dl>
							<dt>5. 原因不明</dt>
							<dd>これが圧倒的に多く、全体の65％を占めます。<br>つまり、現代医学では、習慣性流産・不育症は、まだまだ未知の領域なのです。</dd>
						</dl>
					<!-- /.dl_basic01 --></div>
				</section>
			</section>

			<section>
				<div class="h2_basic01">
					<h2>流産・不育症に漢方をおすすめする理由</h2>
				<!-- /.h2_basic02 --></div>
				<section>
					<div class="h3_basic03">
						<h3>漢方での対応</h3>
					<!-- /.h3_basic03 --></div>
					<p>漢方では、大切な受精卵や赤ちゃんをお母さんの子宮で育て、守ってくれるのは『気』と『血』の力であると考えます。そのため、気と血を増やす漢方を使っていきます。また、習慣性流産の一部には、血流が大切であるタイプもあり、血の流れをよくしていく対応を取る場合もあります。</p>
					<p>基本的には、</p>
					<div class="list_basic01 mt00">
						<ul>
							<li>気血不足（体のエネルギーと血液、栄養、ホルモンなどの不足）</li>
							<li>腎虚（赤ちゃんを育てる力、生命力の不足）</li>
						</ul>
					<!-- /.list_basic01 --></div>
					<p class="mt15">の改善が重要であると考えます。</p>
					<p>あわせて</p>
					<div class="list_basic01 mt00">
						<ul>
							<li>脾虚（暴飲暴食や不摂生などから消化器をつかさどる「脾」が弱くなっている）</li>
							<li>瘀血（血のめぐりが悪くなっている）</li>
							<li>血熱（慢性病や疲労などにより血がめぐらず、悪い熱がこもっている）</li>
							<li>湿熱（不摂生、ストレス等により、体に悪いものがたまり、熱がこもっている）</li>
						</ul>
					<!-- /.list_basic01 --></div>
					<p class="mt15">などの改善も大切です。</p>
				</section>
				<section>
					<div class="h3_basic03">
						<h3>漢方のメリット</h3>
					<!-- /.h3_basic03 --></div>
					<p>漢方で、体質改善をすることには、多くのメリットがあります。<br>主なところですと、</p>
					<div class="list_basic01 mt00">
						<ul>
							<li>妊娠への障害となる婦人科トラブルもあわせて改善できる</li>
							<li>妊活と流産を防ぐ体づくりが一石二鳥でできる</li>
							<li>体調がよくなるので、毎日が楽に生活できる</li>
							<li>体だけでなく、心や精神状態も穏やかに、前向きに妊活できる</li>
							<li>飲むだけなので、自分で手軽に取り組むことができる</li>
							<li>一般的な医薬品に比べ、副作用が少ない</li>
							<li>自然の生薬が中心なので安心度が高い</li>
							<li>病院での治療や薬との併用することができる（飲み合わせ等ご相談ください）</li>
						</ul>
					<!-- /.list_basic01 --></div>
					<p class="mt15">などがあります。<br>中でも大きいのは、妊活と流産を防ぐ体づくりが、同じ漢方でできることです。<br>妊娠するための体質改善と流産を防ぐ体づくりは、漢方では同じ体質が原因であることがほとんどであるため、同じ漢方で、それぞれの目的をかなえることができるのです。</p>
					<p>漢方は、続けていくことで体質を改善します。<br>そして、母体が健康になればなるほど、妊娠する力、育てる力は高まっていくのです。</p>
				</section>
			</section>

			<section class="box_description01">
				<h2 class="tit">体験談</h2>
				<ul>
					<li><a href="#" class="link_basic01">「自分のせいだと思って、ずっと怖かった。」</a></li>
					<li><a href="#" class="link_basic01">「妊娠の予感。流れた子が近くにいるような感じ。」</a></li>
					<li><a href="#" class="link_basic01">「おくる気持ち」</a></li>
					<li><a href="#" class="link_basic01">「響きあう、やさしさ。」</a></li>
					<li><a href="#" class="link_basic01">「もっと喜べばよかった。」</a></li>
				</ul>
			</section>

			<section class="box_description02">
				<h2 class="tit">西洋医学的解説</h2>
				<dl class="dl_basic01">
					<dt>切迫流産：</dt>
					<dd>流産が差し迫っている状態。出血、下腹部痛などはあるが、妊娠継続の可能性がある。</dd>

					<dt>進行流産：</dt>
					<dd>出血、下腹部痛が強くなり、もはや妊娠の中絶が避けられない状態まで進行している。</dd>

					<dt>稽留流産：</dt>
					<dd>子宮内で胎児（胎芽）が死亡しているにもかかわらず、出血、下腹部痛がなく、子宮内に停滞している状態。</dd>

					<dt>不全流産：</dt>
					<dd>胎児や胎盤などの一部が子宮外に排出されているが、一部が子宮内に残った状態。出血、下腹部痛を伴う。</dd>

					<dt>完全流産：</dt>
					<dd>胎児や胎盤などのすべてが子宮外に排出された状態。</dd>

					<dt>感染流産：</dt>
					<dd>性器の感染により起きた流産をいうが、通常は子宮内に残った胎児や胎盤に細菌感染が起こり、発熱した状態。</dd>

					<dt>習慣性流産：</dt>
					<dd>自然流産や早産を3回以上繰り返す場合で、その50％は原因不明。</dd>
				</dl>
				<p class="fzSS">※一般的な説明を記載しています。病状により治療法などは異なりますので、詳しくは主治医の先生にご確認ください。</p>
			</section>
		<!-- /#ryuzan --></div>

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
							<span class="label_new"><img src="/common/img/label_new01.png" alt="NEW"></span>
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
