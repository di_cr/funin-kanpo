<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>4.流産後のケアについて｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="流産後のケアに関するページです。流産は小さなお産であるともいえ、目には見えなくても体はダメージを受けています。体を妊娠できる状態まで回復させてあげることは、とても大切なことです。しっかりと心と体のケアに取り組みましょう。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/ryuzan/after-care.php">
<meta property="og:type" content="article">
<meta property="og:title" content="4.流産後のケアについて｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="流産後のケアに関するページです。流産は小さなお産であるともいえ、目には見えなくても体はダメージを受けています。体を妊娠できる状態まで回復させてあげることは、とても大切なことです。しっかりと心と体のケアに取り組みましょう。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/ryuzan/after-care.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "流産について"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "4.流産後のケアについて"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g04">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/ryuzan/">流産について</a></li>
			<li>4.流産後のケアについて</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1><span class="heading">4. 流産後のケア</span>もし流産をしたら、<br>その後の手当てが大切です。</h1>
		<!-- /.h1_basic01 --></div>

		<div id="after-care">
			<p><span class="gray">流産の中で最も多いのは、妊娠初期6&#xFF5E;9週目に起こる稽留流産です。</span>稽留流産では、子宮内に胎児や胎盤が残っているため、掻爬（そうは）手術を行われることが多くあります。<br>掻爬手術を受けた際には、どうしても子宮内部に傷が生じることが多いため、しっかりと子宮の回復をさせる期間が必要です。<br><span class="yellow">流産手術後は、3ヶ月待ってから妊娠するのが理想的です。</span></p>
			<p>流産は、病気ということではありませんが、一度、妊娠状態になった女性の体は、大きく変化します。</p>
			<p>漢方の世界では、出産のことを大産、流産のことを小産ともいいます。<br>流産といっても、小さな出産と同じで、体に大きな負担をかけているからです。</p>
			<p>目には見えなくても、体はダメージを受けています。<br><span class="yellow">しっかりと体を回復し、妊娠できる状態にきちんと戻してあげることは、とても大切なことです。<br>ぜひ、しっかりと心と体のケアに取り組みましょう。</span></p>

			<div class="care_list01">
				<dl>
					<dt>Ⅰ. <br>流産後の生活</dt>
					<dd>
						<ul class="list_basic01">
							<li>処置された日&#xFF5E;1週間</li>
							<li>&#xFF5E;1ヶ月</li>
							<li>&#xFF5E;100日</li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt>Ⅱ. <br>流産後の養生法</dt>
					<dd>
						<ul class="list_basic01">
							<li>あたためる</li>
							<li>睡眠・休息</li>
							<li>食べ物</li>
						</ul>
					</dd>
				</dl>
				<dl>
					<dt>Ⅲ. <br>流産後のケアが<br>大切な理由</dt>
					<dd>
						<ul class="list_basic01">
							<li>体の症状からチェック</li>
							<li>基礎体温からチェック</li>
							<li>心の状態からチェック</li>
						</ul>
					</dd>
				</dl>
			<!-- /.care_list01 --></div>

			<section>
				<div class="h2_basic01">
					<h2>Ⅰ. 流産後の生活</h2>
				<!-- /.h2_basic01 --></div>
				<p>流産後のケアは、体力の回復だけでなく、その後の妊娠にとっても、非常に大切です。<br><span class="yellow">流産後1ヶ月から100日までは、「流産後の養生」が欠かせません。</span></p>
				<p><span class="gray">特に流産後1ヶ月以内にトラブルが出た場合は、100日までに治さないと、その影響は次の出産まで続くとも言われますし、この時期の回復が不十分だと妊娠しにくい体になってしまいます。</span></p>
				<p>とにかく流産後の無理は禁物です。</p>
				<p>初期流産の場合、妊娠したことを会社などに話していないので、<br>休みにくかったり・・・<br>周囲に気づかれないように気を使ったり・・・<br>気を紛らわしたくて普段以上に仕事をしたり・・・<br>そんな場合もあるかもしれませんが、できるだけ、休みましょう。<br>流産や処置からの期間での目安をまとめておきます。</p>

				<section>
					<div class="h3_basic03">
						<h3>処置された日&#xFF5E;1週間</h3>
					<!-- /.h3_basic03 --></div>
					<p>一般的には、流産の処置をされてから1週間後に診察を受けるようになりますが、ここまでの期間は、<span class="yellow">とにかく無理は禁物です。仕事や家事もできるだけ控えてください。</span></p>
					<p>処置を受けた場合、どうしても子宮腔内に傷がついてしまいます。<br>無理をすると炎症が起こる場合があり、それが原因で、癒着などを引き起こすこともあります。</p>
					<p>入浴は控え、シャワーのみにしましょう。</p>
					<p>出血は、流産してから1週間&#xFF5E;10日以内でおさまってきます。<br>もしも、10日以上出血が続く場合は、診察を受けることをおすすめします。</p>
				</section>
				<section>
					<div class="h3_basic03">
						<h3>&#xFF5E;1ヶ月</h3>
					<!-- /.h3_basic03 --></div>
					<p>流産や処置の1ヶ月くらいで次の生理が来ます。<br>それまでの間は、<span class="yellow">激しいスポーツは厳禁です。遠方への旅行、夫婦生活なども、控えたほうが良いでしょう。家事や仕事も無理をせず、だんだんと慣らしていくほうが体にとっても楽です。</span></p>
					<p>もしも、2ヶ月たっても生理が来ないようなら、一度、診察を受けてみてください。</p>
				</section>
				<section>
					<div class="h3_basic03">
						<h3>&#xFF5E;100日</h3>
					<!-- /.h3_basic03 --></div>
					<p>生理を2、3回見送った後から、妊娠してもよいと言われるドクターが多いようです。</p>
					<p>夫婦生活は、1ヶ月以降は構いませんが、100日くらいまでは、あえてタイミングをあわせたりせずに、自然な感じですごすほうがよいでしょう。</p>
					<p>漢方的には、<span class="yellow">流産後100日までは妊娠を避け、しっかりと子宮を回復させることが大切です。</span></p>
					<p>それまでの期間で妊娠しても、過度に心配する必要はありません。</p>
				</section>
			</section>

			<section>
				<div class="h2_basic01">
					<h2>Ⅱ. 流産後の養生法</h2>
				<!-- /.h2_basic01 --></div>
				<p>流産後の生活で大切なのは、<em>あたためること、睡眠・休息、食べ物</em>です。</p>

				<section>
					<div class="h3_basic03">
						<h3>あたためる</h3>
					<!-- /.h3_basic03 --></div>
					<p><span class="gray">冷えは流産後の体に、大きなダメージを与えます。</span><br>また、<span class="gray">トラブルを引き起こす原因</span>になってしまうので、くれぐれも、しっかりとあたためるようにしましょう。</p>

					<section class="box_description01">
						<h3 class="tit">服装</h3>
						<p>腹巻きは必須です。毛糸のパンツなどもおすすめ。<br>首、手首、足首をあたためると体温を保つことができます。ストールやレッグウォーマーなどを活用しましょう。<br>丈の短いスカート類は避け、なるべくパンツスタイルで。<br>夏場だと油断しがちですが、エアコンなどで逆に冷えるので気をつけて。</p>
					</section>
					<section class="box_description01">
						<h3 class="tit">ナプキン</h3>
						<p>できれば、布ナプキンをおすすめします。<br>通常のナプキンには、経血を吸収するための高分子吸収体が使われています。<br>この高分子吸収体が液体（経血）を含むと、冷えピタと同じ状態になって子宮を冷やしてしまうからです。<br>可能であれば、布ナプキンに変えてみましょう。</p>
					</section>
					<section class="box_description01">
						<h3 class="tit">カイロ</h3>
						<p>ホッカイロは、手軽で直接下腹部をあたためてくれます。<br>暑いのに無理やり温める必要はないですが、冷える感じがする時は、ぜひ活用しましょう。<br>貼る場所は、おへその下辺りと腰。<br>前後からカイロのサンドイッチで温めると効果的です。</p>
					</section>
				</section>

				<section>
					<div class="h3_basic03">
						<h3>睡眠・休息</h3>
					<!-- /.h3_basic03 --></div>
					<p><em>体を回復させてくれる最高の治療は睡眠です。</em></p>
					<ul class="list_basic01">
						<li>流産後1週間までは10時までに寝る。</li>
						<li>1ヶ月までは11時までに寝る。</li>
					</ul>
					<p class="mt15">眠っている間に人間の体は、回復します。<br><em>特に、東洋医学で内臓の回復時間となるのが夜11時&#xFF5E;午前3時の4時間。<br>この4時間をしっかりと眠るようにしましょう。</em></p>
					<p>ただ、流産後のダメージで「血」が不足してくると眠りにくくなったり、途中で目が覚めて熟睡できなくなることもすくなくありません。</p>
					<p>そんな時は、横になっているだけでも構いません。<br>呼吸に意識を向けてみると、気持ちも穏やかに瞑想効果も得られます。</p>
				</section>

				<section>
					<div class="h3_basic03">
						<h3>食べ物</h3>
					<!-- /.h3_basic03 --></div>
					<p>からだをあたためたり、滋養をつける食べ物を積極的に取りましょう。<br>流産後1ヶ月は冷たいものは避けてください。</p>
					<p><span class="gray">特に、流産後1週間は、脂っこいもの、香辛料などの刺激の強いものは避けてください。<br>消化の良いものを取るようにしましょう。</span></p>
					<p><span class="yellow">どんどん食べてほしいのは、鶏肉。</span><br>できれば骨付きのものをスープ仕立てにしたものがおすすめです。</p>
					<p>「子宮は血の海」と漢方ではいいます。<br>流産では、この「血」が大きく損なわれます。<br>鶏肉をたくさん食べて、血をしっかりと回復させていきましょう。</p>
					<div class="box_col_2">
						<div class="box_description01">
							<dl>
								<dt class="tit">〈体を温める食べ物〉</dt>
								<dd>
									<ul>
										<li>鶏肉</li>
										<li>レバー</li>
										<li>ねぎ</li>
										<li>しょうが</li>
										<li>やまいも</li>
										<li>はちみつ</li>
										<li>黒糖</li>
										<li>など</li>
									</ul>
								</dd>
							</dl>
						<!-- /.box_description01 --></div>
						<div class="box_description02">
							<dl>
								<dt class="tit">〈体を冷やす食べ物〉</dt>
								<dd>
									<ul>
										<li>生野菜</li>
										<li>夏の野菜（キュウリ、レタス、トマトなど）</li>
										<li>南の果物（パパイヤ、マンゴーなど）</li>
										<li>清涼飲料水</li>
										<li>白砂糖</li>
										<li>など</li>
									</ul>
								</dd>
							</dl>
						<!-- /.box_description02 --></div>
					<!-- /.box_col_2 --></div>
				</section>
			</section>

			<section>
				<div class="h2_basic01">
					<h2>Ⅲ. 流産後のケアが大切な理由</h2>
				<!-- /.h2_basic01 --></div>
				<p>流産後のこころやからだの不調は、体がダメージを受けているために起きる一時的なものです。<br>ただしこれをそのままにしておくと、そのまま自分の体質になってしまいます。<br>流産後の不調は早めに解決することが大切です。</p>

				<section>
					<div class="h3_basic03">
						<h3>体の症状からチェック</h3>
					<!-- /.h3_basic03 --></div>
					<div class="box_description01">
						<dl>
							<dt class="tit">体の症状：10のチェック項目</dt>
							<dd>
								<ul class="check_list01">
									<li>便秘</li>
									<li>のぼせ</li>
									<li>めまい</li>
									<li>生理痛</li>
									<li>立ちくらみ</li>
									<li>生理不順</li>
									<li>血色が悪い</li>
									<li>経血量が減った</li>
									<li>冷え</li>
									<li>おりものが減った</li>
								</ul>
							</dd>
						</dl>
					<!-- /.box_description01 --></div>
					<p class="mt15">上記のトラブルが流産後に出ている場合は、体がダメージを受けているサインです。<br>なるべく早く改善しましょう。</p>
					<p>妊娠力の中心は、「血」です。<br>東洋医学でいう「血」とは、血液、栄養、ホルモンをまとめたもので、子宮・卵巣系の力の源です。<br><span class="gray">流産では、この血が大きなダメージを受けていて、トラブルや、不妊を招く原因になります。</span><br><span class="yellow">『「血」を補い、めぐりをよくする』このことが、流産後に、また新しい命を授かる体に戻るためのとても大切なことです。</span></p>
					<p>体の不調も、心の不調も血の状態を良くすれば、しっかりと回復して、本来のあなたらしい状態を取り戻していきましょう。</p>
					<p>血を回復させるために必要なことは、流産後の養生法そのものです。（養生法は上記です）<br>ただ、『早く回復させたい』、『しっかりと回復させたい』、『次の流産はできるだけ避けたい』そんなふうにお考えの場合は、漢方や薬膳をおすすめします。</p>
				</section>
				<section>
					<div class="h3_basic03">
						<h3>基礎体温からチェック</h3>
					<!-- /.h3_basic03 --></div>
					<p>流産後、最初の生理がくるまでは、基礎体温を付ける必要はありません。<br>また、気持ち的に少し妊娠のことを考えたくない場合も同様です。</p>
					<p>自分の気持ちが「赤ちゃんを授かりたいなぁ」そんなふうに、自然と考えられるようになった時がタイミングです。それから体温を測るようにすればOKです。</p>
					<p>基礎体温を測定を再開した時に大切なポイントがあります。<br>それは、<em>流産前後の基礎体温の比較</em>です。</p>
					<section class="box_description01">
						<h4 class="tit">A. 基礎体温の変化なし</h4>
						<p>それまでと、基礎体温に大きな変化がない場合は、特に心配しなくても大丈夫です。妊娠できたことは間違いないわけですから、自信をもって、次の妊娠に向かって行きましょう。</p>
					</section>
					<section class="box_description02">
						<h4 class="tit">B. 基礎体温が高くなる</h4>
						<p>流産後に不妊で悩まれる方に多いタイプです。<br>流産前に比べて体温が高くなっている場合、特に、低温期が36.5度を上回るようになった場合は、注意が必要です。</p>
						<p>低温期の体温は、通常36.2&#xFF5E;3度。<br>この体温だと卵胞が育ちやすいと言われています。<br>イメージとしては、『36度を切ると、たまごが冷蔵庫で冷えてる状態』『36.5度を越えるとゆでたまごになっている状態』を想像してもらうと、わかりやすいかもしれません。</p>
					</section>
					<p class="mt15">流産後に不妊で相談に来られる方で多いのが、体温が上がってしまう方です。<br>流産後のケアでは、血流をよくすることが大切なのですが、それが不十分な場合によく見られます。</p>
					<p>漢方的には、余分な悪い熱が出てしまって「育てる力」が不足していると考えます。<br>冷えとか、生理不順といった自覚症状があまりない事が多く、本人の意識が体に向きにくいのも特徴です。この場合は、体から悪い熱を追い出した上で、育てる力を補う必要があります。</p>
					<p>漢方的対応としては、<span class="yellow">『血流をよくする』、『育てる力を補う』</span>ということを行います。</p>
					<section class="box_description02">
						<h4 class="tit">C. 基礎体温が低くなる</h4>
						<p>流産後の体のダメージが取れていない方によく見られます。<br>特に、低温期が36.0度、高温期が36.5度を下回ってしまう場合は要注意です。</p>
						<p>流産前に比べて経血量が減った・冷えがひどくなった・貧血・生理不順になったなど、婦人科系の不調がみられることもすくなくありません。<br>子宮・卵巣の力のもとであり、妊娠力そのものともいえる「血」が不足している状態です。</p>
					</section>
					<p class="mt15">ポイントとなるのは3つ。<br><span class="yellow">『温めること。』『睡眠をとること。』『栄養をとること。』。</span></p>
					<p>毎日の生活を大切にして過ごすと血が増えて妊娠力がしっかりと高まってきます。<br>早め、早めに対処していきましょう。</p>
				</section>
				<section>
					<div class="h3_basic03">
						<h3>心の状態からチェック</h3>
					<!-- /.h3_basic03 --></div>
					<p>流産の時に感じた、喪失感・悲しみ・つらさ・怒り・不安・自分を責める気持ち・・・。<br>そういった湧き上がる感情は、自然なもので、その時の感情を素直に解放することはとても大切なことです。</p>
					<ul class="list_basic01">
						<li>泣いてもいい</li>
						<li>ひとに感情をぶつけてもいい</li>
						<li>落ち込んでもいい</li>
						<li>ぼーっとしてもいい</li>
					</ul>
					<p class="mt15">そんなふうに、自分が感じていること、思っていることをゆるしてあげてください。<br>そして、気づいたこと感じたことを受け取り手放してみましょう。<br>流産のことは、忘れなくてもいいし、悪いことでもありません。</p>
					<p><span class="yellow">あなたが、あなたであること。</span><br>そして、あなたらしくあることを空に天使となって帰って行った赤ちゃんもきっと望んでいます。<br>赤ちゃんにとってお母さんのしあわせや笑顔こそがすべてなのですから。</p>
					<section class="box_description01">
						<h4 class="tit">時間とともにいろんな気持ちを感じられるでしょう。</h4>
						<p>ずっと気になってしまって、このままでいいのか心配になったり<br>早く立ち直らないといけないと焦ってしまったり・・・<br>よくあることと聞いてはみてもどうして自分がと納得いかなかったり・・・<br>偶然とはいわれても、やっぱりなにか悪かったのではと自分を責めてしまったり・・・<br>理屈などではなく、時間が必要な時もあります。</p>
					</section>
					<section class="box_description01">
						<h4 class="tit">気持ちを伝えてみる</h4>
						<p>流産について話したり、相談したりするのが難しいのも事実です。<br>心のなかに押し込んだままにして、ずっとしんどいままになってしまう人も少なくありません。<br>誰かに伝える。<br>聞いてもらう。<br>そのことで気持ちが楽になる方もとても多いのです。<br>なかなか身近な人には話しにくかったりもします。</p>
						<p>専門のサイトやカウンセリングを使ってみることもおすすめです。</p>
					</section>
					<section class="box_description01">
						<h4 class="tit">体から心を楽にする</h4>
						<p>こころは、体の影響を大きく受けています。<br>特に、流産後に出やすい体質の影響で、「気持ちが前向きにならない（無気力）」「心配や、不安がおさまらない（不安）」「自律神経が不安定で感情がゆれる（イライラ、不安定）」の3つの感情が出やすくなります。<br>本来の自分の感情が、体質のせいで不調になっていることがとても多いのです。<br>もしも、こころの状態をまわりから言われたり、自分でもおかしいなと感じたら、一度、体質をチェックしてしっかりとからだを回復させることも大切です。<br>「本当の自分とは違う」<br>そう感じたら、体のケアに目を向けましょう。</p>
						<p>同時に妊娠力のアップにもなり、こころとからだの両方が良い方向へ進んでいきます。</p>
					</section>
				</section>
			</section>
		<!-- /#after-care --></div>

		<div class="category_link01">
			<ul>
				<li><a href="/ryuzan/no-worries.php">1.  はじめに 流産は決して特別なことではありません。</a></li>
				<li><a href="/ryuzan/right-knowledge.php">2. 流産のタイプ 流産には、さまざまなタイプがあります。</a></li>
				<li><a href="/ryuzan/prevent-miscarriage.php">3. 次の妊娠へ 「流産を乗り越え授かる方法」と「流産の予防法」</a></li>
				<li><a href="/ryuzan/after-care.php">4. 流産後のケア もし流産をしたら、その後の手当てが大切です。</a></li>
			</ul>
		</div>

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
