<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>1.はじめに 流産は決して特別なことではありません｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="まず知ってほしいのは、流産は決して特別なことではないということです。また、ほとんどの流産は『たまたま』起きます。自分を責めないでください。なによりも大切なことは、あなたはまた『妊娠できる』ということです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/ryuzan/no-worries.php">
<meta property="og:type" content="article">
<meta property="og:title" content="1.はじめに 流産は決して特別なことではありません｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="まず知ってほしいのは、流産は決して特別なことではないということです。また、ほとんどの流産は『たまたま』起きます。自分を責めないでください。なによりも大切なことは、あなたはまた『妊娠できる』ということです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/ryuzan/no-worries.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/page.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "流産について"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"name": "1.はじめに 流産は決して特別なことではありません"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g04">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/ryuzan/">流産について</a></li>
			<li>1.はじめに 流産は決して特別なことではありません</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
	<div class="inner02">
		<div class="h1_basic01">
			<h1><span class="heading">1.はじめに</span>流産は決して特別なことではありません。</h1>
		<!-- /.h1_basic01 --></div>

		<div id="no-worries">
			<p>流産は、とても悲しいことです。<br>ただ、妊娠経験のある女性の40％が流産経験するとも言われています。<br>ですので、決して特別なことだったり、あなたに何かの責任があるわけではありません。<br>まずそのことは知っておいてください。</p>
			<p>たとえ流産をしたとしても、妊娠したのは事実です。<br>あなたには、『赤ちゃんを授かる力』が100％備わっているということなのです。<br>ぜひ、自分の体を信じてあげてください。<br>そして、次に妊娠した時に、赤ちゃんをしっかりと育てる力をつけていきましょう。</p>

			<section class="box_notebook01">
				<h2>はじめに</h2>
				<div class="notebook_inner01">
					<p>授かった新しい生命。<br>その生命が失われてしまった時。<br>言いようのない悲しみに襲われたり　自分を責めてしまったり　もしかすると、また・・・という不安に包まれていたりするかもしれません。<br><span class="gray">まず、知ってほしいのは、流産は決して特別なことではないということです。</span></p>
				<!-- /.notebook_inner01 --></div>
				<div class="box_quote02">
					<p>流産の確率は15％（約7人に1人）、厚生労働省の調査でも、妊娠したことがある女性の40％が流産経験があるといわれています。</p>
				<!-- /.box_quote02 --></div>
				<div class="notebook_inner01">
					<p><span class="gray">また、ほとんどの流産は『たまたま』起きます。</span><br>あなたが何をしたから、あるいは何かをしなかったから起きたわけではありません。<br>たまたま、偶然に流産が起きてしまうことがほとんどです。<br>なので、自分を責めないでほしいのです。<br>そして、なによりも大切なことは、あなたはまた『妊娠できる』ということです。<br>天使になって帰って行った赤ちゃんは、お母さんのおなかにやってくることで、『妊娠できるよ』って教えてくれています。<br>悲しくてつらい時は、すぐにそう思えないかもしれないけど、<span class="yellow">『あなたは妊娠できる』</span><br><span class="yellow">そう、自信をもっていただけたらと思います。</span></p>
				<!-- /.notebook_inner01 --></div>
			</section>

			<section>
				<div class="h2_basic01">
					<h2>不安をなくすために、大切なのは、<br class="pc">まず、正しい知識を『知る』ことです。</h2>
				<!-- /.h2_basic --></div>
				<p>縁結び出雲の子宝漢方カウンセラーとして、女性の相談に関わって10年以上も婦人科専門の薬剤として活動しています。<br>不妊カウンセラーでもあり、自身の薬局の子宝相談では、これまで100人の方に妊娠していただいています。<br>相談にいらっしゃる9割以上の方が病院で治療をしてもなかなか赤ちゃんを授かれない方です。<br>長い間がんばって、いろいろな気持ちや、たくさんの努力を積み重ねて、念願の赤ちゃんを授かられる。『妊娠しました！』その喜びの報告を聞くことがとてもうれしいくて、この仕事を続けさせていただいます。</p>
				<p>その一方で、やっとの思いで授かった赤ちゃんを流産される方も少なくありません。<br>流産された時の、悲しみ、つらさ、やり場のない気持ち。<br>データや理屈を示されて、頭ではわかっても<span class="gray">『どうして私が・・・』</span>という納得の行かない思い。<br><span class="gray">『あの時、仕事をしたから、無理に動いたから・・・』</span>そんなふうに自分を責めたり・・・、流産を経験してから、相談に来られる方も多くいらっしゃいます。</p>
				<p>そんな日々の子宝相談の中で感じたのは、流産に対する正しいケアや知識が非常に少ないという現実です。ネットで簡単に情報は手に入りますが、不安を煽ってしまうような無記名の書き込みや体験談が少なくありません。<br>そして、医療関係者が記名のもとで発信している情報はあまり多くないのが現状です。<br><span class="gray">不安をなくすために大切なのは、いまの自分の状況を知り、わからないこと、先が見えないことを減らすことです。</span></p>
				<p>視界がひらけてくると、未来が見え、不安が減っていきます。<br>気持ちを受け止めるための時間も大切。<br>その中で、正しい知識を得ていただきたい。<br><span class="yellow">知ることは、安心につながり、自分の気持ちを受け止め、次に進んでいく、力になります。</span><br>このサイトがその一助になれば、これほどうれしいことはありません。<br>ぜひ、お役立てください。<br>大丈夫。<br>自分の持つ力を信じましょう。<br>あなたが、新しい一歩を踏み出せることを、心より願っています。</p>
			</section>

			<section class="sec_about01">
				<h2>このサイトの内容について</h2>
				<p>西洋医学を学んだ薬剤師という医療の視点。<br>東洋医学・漢方の専門家である国際中医師の視点。<br>不妊相談の現場にいるカウンセラーの視点。<br>その3つの視点から、さまざまな流産に関する情報を提供しています。<br>科学的なデータに基づいたものだけでなく、精神世界的なものまで含めて、流産された方が必要とされている情報・知っておいていただきたい情報や考え方をまとめています。流産時は、病医院での治療が大切な時期でもあります。<br>当ホームページも含み、インターネット上の情報を参考にされる際は、主治医の先生とも、よくご相談ください。</p>
			</section>
		<!-- /#no-worries --></div>

		<div class="category_link01">
			<ul>
				<li><a href="/ryuzan/no-worries.php">1.  はじめに 流産は決して特別なことではありません。</a></li>
				<li><a href="/ryuzan/right-knowledge.php">2. 流産のタイプ 流産には、さまざまなタイプがあります。</a></li>
				<li><a href="/ryuzan/prevent-miscarriage.php">3. 次の妊娠へ 「流産を乗り越え授かる方法」と「流産の予防法」</a></li>
				<li><a href="/ryuzan/after-care.php">4. 流産後のケア もし流産をしたら、その後の手当てが大切です。</a></li>
			</ul>
		</div>

		<aside id="related">
			<div class="h2_basic03">
				<h2>おすすめのコラム</h2>
			<!-- /.h2_basic03 --></div>
			<div class="topics_wrap01">
				<div class="post_items01 col_3">
					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>

					<div class="post_item01">
					<a href="#">
						<div class="post_thumb">
							<img src="/common/img/upload/img_post01.png" alt="">
						<!-- /.post_thumb --></div>
						<div class="post_meta">
							<div class="cat_label label_column">コラム</div>
							<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
						<!-- /.post_meta --></div>
						<div class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</div>
						<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
					</a>
					<!-- post_item01 --></div>
				<!-- /.post_items01 --></div>
				<div class="more01">
					<div class="btn_basic01">
						<a href="/column/">コラム一覧へ</a>
					<!-- /.btn_basic01 --></div>
				<!-- /.more01 --></div>
			<!-- /.topics_wrap01 --></div>
		<!-- /#related --></aside>
	<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
