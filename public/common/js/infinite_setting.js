/*
 * infinite_setting.js
 */
(function($) {
jQuery(document).ready(function($) {
	//お知らせ
	var $container = $(".post_items01");

	$container.infiniteScroll({
		debug: true,
		path: '.pagination__next',
		append: '.post_item01',
		button: '.more01',
		scrollThreshold: false,
		status: '.page-load-status',
		history: 'push'
	});
	$container.on( "append.infiniteScroll", function( event, response, path, items ) {
		$("body").height("auto");
	});

	//体験談
	var $container02 = $(".post_exp_items01");

	$container02.infiniteScroll({
		debug: true,
		path: '.pagination__next',
		append: '.post_exp_item01',
		button: '.btn_exp_more',
		scrollThreshold: false,
		status: '.page-load-status',
		history: 'push'
	});
	$container02.on( "append.infiniteScroll", function( event, response, path, items ) {
		$("body").height("auto");
	});
});
})(jQuery);
