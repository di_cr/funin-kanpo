/*
 * slider_setting.js
 */
(function($) {
jQuery(document).ready(function($) {
/* ==========================================================
!top
========================================================== */
/* mainimg
------------------------------------ */
	$('.slider').slick({
		dots: false,
		infinite: true,
		speed: 1500,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: false,
		fade: true,
		pauseOnHover: false
	});

/* kodakararetreat
------------------------------------ */
	$('.program_slider').slick({
		dots: false,
		centerMode: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		centerPadding: '0',
		variableWidth: true,
		infinite: true,
		speed: 1500,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [
			{
				//640以下
				breakpoint: 640,
				settings: {
					centerMode: true,
					autoplay: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					variableWidth: false,
				}
			}
		]
	});
});
})(jQuery);
