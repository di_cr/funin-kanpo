/*
 * common.js
 */
(function($) {
jQuery(document).ready(function($) {
/* ==========================================================
!stack
========================================================== */
/* 関数の読み込み
------------------------------------ */
	cvFixed();
	pageTopFixed();
	pageScroll();
	Accordion();
	tabMenu();

/* ==========================================================
!外部JS(プラグイン)の個別記述はここに記述する
========================================================== */

/* window.matchMedia
------------------------------------ */
	var sp_menu_gnavi = $("#gnavi");
	var sp_menu_trigger = $("#gnavi .menu_trigger");

	var spNavi = function(){
		sp_menu_trigger.on("click keyup", function() {
			$(this).toggleClass('open');
			sp_menu_gnavi.toggleClass('open');
		});
	};

	if(window.matchMedia("(max-width:767px)").matches){
		// (1)windowの読み込みが完了したときに、767px以下の場合に行いたいもの
		// メニュー
		spNavi();
	}
	if(window.matchMedia("(min-width:1200px)").matches){
		// (2)windowの読み込みが完了したときに、1200px以上の場合に行いたいもの
		window.onload = function() {
			var bgPosition = $("body").height() / 10;
			$("#bg1").attr('data-end', 'background-position:left '+ -bgPosition +'px');
			$("#bg2").attr('data-end', 'background-position:right '+ -bgPosition +'px');

			// 背景パララックス skrollr.min.js
			var timer = null;
			clearTimeout( timer );
			timer = setTimeout(function() {
				var s = skrollr.init();
			}, 200);
		};
	}

	function matchFunction(){
		// (3)windowサイズを変更して、767px以下になったら発火するイベント
		// ハンバーガーメニュー
		spNavi();
	}
	window.matchMedia("(max-width:768px)").addListener(matchFunction);

/* URLをコピー clipboard.min.js
------------------------------------ */
	var clipboard = new Clipboard('.copy_trigger');
	clipboard.on('success', function(e) { //成功時の処理
		alert("リンクをコピーしました");
	});
	clipboard.on('error', function(e) { //失敗時の処理
		alert("リンクをコピーできませんでした。お使いの端末はこの機能に対応していない可能性があります。");
	});

/* 画像リンクにアイコンをつけない
------------------------------------ */
	$('a:has(img)').addClass("no_ico");

/* チェックボックス入力数カウント
------------------------------------ */
	$('.check_inner01 input:checkbox').change(function() {
		var cnt = $('.check_inner01 input:checkbox:checked').length;
		$('.check_count_num01').text(cnt);
	}).trigger('change');


});

/* ==========================================================
!fixed cv area
========================================================== */
var cvFixed = function(){
	var cvFixYakuzencha = $(".cv_fix_yakuzencha");
	var cvFixKodakara = $(".cv_fix_kodakara");

	$(window).bind("scroll", function() {
		scrollHeight = $(document).height();
		scrollPosition = $(window).height() + $(window).scrollTop();

		//ページトップから150px以上スクロール＆フッターより上の位置までは表示させる（/kodakararetreat/）
		footHeight = $("footer").innerHeight();
		if ($(this).scrollTop() > 150 && scrollHeight - scrollPosition  >= footHeight) {
			cvFixKodakara.fadeIn();
		} else {
			cvFixKodakara.fadeOut();
		}
		//ページトップから150px以上スクロール＆料金エリアより上の位置までは表示させる（/yakuzencha/）
		priceHeight = $("#price").innerHeight() + $("#delivery").innerHeight() + $("footer").innerHeight();
		if ($(this).scrollTop() > 150 && scrollHeight - scrollPosition  >= priceHeight) {
			cvFixYakuzencha.fadeIn();
		} else {
			cvFixYakuzencha.fadeOut();
		}
	});
};

/* ==========================================================
!fixed pagetop scroll
========================================================== */
var pageTopFixed = function(){
	var pageTopBox = $(".pagetop_fixed");
	var pageTopLink = $(".pagetop_fixed p");

	pageTopLink.click(function() {
		$("html, body").animate({scrollTop: 0}, 1000, 'easeInOutCubic');
		return false;
	});

	$(window).bind("scroll", function() {
		if ($(this).scrollTop() > 150) {
			pageTopBox.fadeIn();
		} else {
			pageTopBox.fadeOut();
		}

		linkHeight = pageTopLink.height();
		linkBottom = linkHeight/2.5;
		scrollHeight = $(document).height();
		scrollPosition = $(window).height() + $(window).scrollTop();
		footHeight = $("#ft_links01").innerHeight() - linkBottom;

		if ( scrollHeight - scrollPosition  <= footHeight ) {
			pageTopBox.removeClass("scroll");
			pageTopLink.css({"bottom": - linkBottom});
			//pageTopLink.css({"position":"absolute","bottom": - linkBottom});
		} else {
			pageTopBox.addClass("scroll");
			pageTopLink.css({"bottom": 0});
			//pageTopLink.css({"position":"fixed","bottom": "0px","margin":"0px"});
		}
	});
};
/* ==========================================================
!pageScroll
========================================================== */
var pageScroll = function(){
	jQuery.easing.easeInOutCubic = function (x, t, b, c, d) {
		if ((t/=d/2) < 1) return c/2*t*t*t + b;
		return c/2*((t-=2)*t*t + 2) + b;
	};
	$("a[href*='#'], .scroll").click(function(){
		$('html, body').animate({
			scrollTop: $($.attr(this, 'href') ).offset().top - 20 // - 70 等といれるとそのピクセル数分増えた状態でスクロールが止まる(ヘッダー固定の時に利用)
		}, 500);
	});
};
/* ==========================================================
!Accordion
========================================================== */
var Accordion = (function(){
	var accordion01 = $(".accordion dt");
	accordion01.on("click", function() {
	$(this).next().slideToggle();
	});
});
/* ==========================================================
!tab
========================================================== */
var tabMenu = (function(){
	var tabLink = $(".topics_navi01 li");
	var tabBox = $(".topics_box01");
	tabLink.on("click", function() {
		if($(this).not('active')){
			// タブメニュー
			$(this).addClass('active').siblings('li').removeClass('active');
			// タブの中身
			var index = tabLink.index(this);
			tabBox.eq(index).addClass('active').siblings('div').removeClass('active');
		}
		$("body").css("height","auto");
	});
});
})(jQuery);
