if(window.matchMedia("(max-width:767px)").matches){
    var zoomSize = 15;
} else {
    var zoomSize = 16;
}

function initMap1() {
    //tokyo
    var map_tokyo = new google.maps.LatLng(35.394086,132.686691);
    var opts_tokyo = {
      zoom: zoomSize,
      center: new google.maps.LatLng(35.397711,132.686404),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"), opts_tokyo);

    var marker = new google.maps.Marker({
    position: map_tokyo,
    map: map,
    icon: '/common/img/com/ico_map01.png',
    title: '堀江薬局'
    });

    /*スタイルのカスタマイズ*/
    var styleOptions =
    [
    {
      "stylers": [
        { 'hue': '#deae8c' },
        { 'lightness': 10 },
        { 'gamma': 0.9 },
        { 'saturation': -20 }
      ]
    },{
       featureType: 'landscape.man_made',
       elementType: 'geometry',
       stylers: [{
           color: '#ebe3cd'
       }]
    }, {
        featureType: 'water', //水
        elementType: 'geometry',
        stylers: [{
            color: '#b9d3c2'
        }]
    }, {
        featureType: 'landscape.natural', //自然物
        elementType: 'geometry',
        stylers: [{
            color: '#dfd2ae'
        }]
    }, {
        featureType: 'poi', //すべての有名スポット
        elementType: 'geometry',
        stylers: [{
            color: '#dfd2ae'
        }]
    }, {
        featureType: 'poi.park', //公園
        elementType: 'geometry',
        stylers: [{
            color: '#a5b076'
        }]
    }, {
        featureType: 'road.highway', //公道
        elementType: 'geometry',
        stylers: [{
            color: '#f8c967'
        }]
    }, {
        featureType: 'road', //道路
        elementType: 'labels.icon',
        stylers: [{
            'hue': '#3776c6'
        }]
    }, {
        featureType: 'transit', //駅
        elementType: 'labels.icon',
        stylers: [{
            'hue': '#00b0ff'
        }]
    }, {
        featureType: 'transit', //駅
        elementType: 'labels.text.fill',
        stylers: [{
            color: '#503834'
        }]
    }
    ];

    var styledMapOptions = { name: '堀江薬局' };
    var OHtype = new google.maps.StyledMapType(styleOptions, styledMapOptions);
    map.mapTypes.set('blue', OHtype);
    map.setMapTypeId('blue');
}
