<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.9&appId=339681446457421";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<header id="header" role="banner">
	<div id="hd_inner">
		<h1 id="hd_logo">
			<a href="/">
				<img src="/common/img/logo01.svg" width="1200" alt="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
				<span><img src="/common/img/ill_bird01.png" width="67" alt=""></span>
			</a>
		</h1>
		<nav id="gnavi" role="navigation">
			<div class="menu_trigger" title="MENU"><span>MENU</span></div>
			<ul>
				<li id="gnav00"><a href="/about-akiyoshi-horie/">堀江昭佳</a></li>
				<li id="gnav01"><a href="/yakuzencha/">漢方・薬膳茶</a></li>
				<li id="gnav02"><a href="/kodakararetreat/">セミナー・リトリート</a></li>
				<li id="gnav03"><a href="/before-infertility-treatment/">不妊治療について</a></li>
				<li id="gnav04"><a href="/ryuzan/">流産について</a></li>
				<li id="gnav05"><a href="/experiences/">体験談</a></li>
				<li id="gnav06"><a href="/faq/">よくある質問</a></li>
			</ul>
		</nav>
	<!-- /#hd_inner --></div>
</header>
<?php include(__DIR__ .'/custom.php'); ?>
