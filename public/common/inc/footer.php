<div id="bg1" data-0="background-position:left 0px;"></div>
<div id="bg2" data-0="background-position:right 0px;"></div>

<footer role="contentinfo">
	<div id="ft_navi01">
		<div class="inner01">
			<ul>
				<li><a href="/about-akiyoshi-horie/">堀江昭佳</a></li>
				<li><a href="/yakuzencha/">漢方・薬膳茶</a></li>
				<li><a href="/kodakararetreat/">セミナー・リトリート</a></li>
				<li><a href="/before-infertility-treatment/">不妊治療について</a></li>
				<li><a href="/ryuzan/">流産について</a></li>
				<li><a href="/experiences/">体験談</a></li>
				<li><a href="/faq/">よくある質問</a></li>
			</ul>
			<span class="ft_cloud bg_cloud01"><img src="/common/img/ill_footer_cloud01.png" width="35" alt=""></span>
			<span class="ft_cloud bg_cloud02"><img src="/common/img/ill_footer_cloud01.png" width="35" alt=""></span>
		<!-- /.inner01 --></div>
	<!-- #ft_navi01 --></div>

	<div class="pagetop_fixed">
		<p><img src="/common/img/pagetop_<?php echo $season; ?>01.png" alt="ページの先頭へ"></p>
	<!-- /.pagetop_fixed --></div>

	<div id="ft_links01">
		<div class="inner01">
			<div id="ft_logo"><img src="/common/img/logo01.svg" width="1200" alt="不妊漢方 縁結び出雲の子宝サイト 堀江 昭佳 official website"></div>
			<span class="bg_gate01"><img src="/common/img/ill_footer_gate01.png" width="58" alt=""></span>
			<div id="ft_links_inner01">
				<ul>
					<li><a href="/contact/">お問い合わせ</a></li>
					<li><a href="/com/">会社概要</a></li>
					<li><a href="/privacy/">個人情報の保護について</a></li>
				</ul>
				<p>キャラクターデザイン<a href="http://ameblo.jp/saoring3063/" target="_blank">&copy;日野さおり</a>  このサイトに掲載のイラスト・写真・文章の無断転載を禁じます。</p>
				<p id="ft_copyright">
					<small>&copy; Copyright 2008-2017 Horie Drug All Rights Reserved.</small>
				</p>
			<!-- /#ft_links_inner01 --></div>
			<span class="ft_cloud bg_cloud03"><img src="/common/img/ill_footer_cloud01.png" width="35" alt=""></span>
			<span class="ft_cloud bg_cloud04"><img src="/common/img/ill_footer_cloud01.png" width="35" alt=""></span>
		<!-- /.inner01 --></div>
	<!-- /#ft_links01 --></div>
</footer>
