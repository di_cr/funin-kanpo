<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>会社概要｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="堀江薬局の会社概要ページです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/com/">
<meta property="og:type" content="article">
<meta property="og:title" content="会社概要｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="堀江薬局の会社概要ページです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/com/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/com.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "会社概要"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li>会社概要</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
		<div class="h1_basic01 inner01">
			<h1>会社概要</h1>
			<span>Company</span>
		<!-- /.h1_basic01 --></div>

		<div class="inner02">
			<div class="txt_lead01">
				<p>堀江 昭佳が代表を務める堀江薬局の創業は大正十三年。<br>本店は、縁結びで有名な出雲大社の表参道である神門通りにあります、婦人科専門の漢方薬局です。</p>
			<!-- /.lead01 --></div>

			<div class="table_basic02">
				<dl>
					<dt>会社名</dt>
					<dd>有限会社 堀江薬局</dd>
				</dl>
				<dl>
					<dt>住所</dt>
					<dd>〒699-0711　島根県出雲市大社町杵築南1370-2　<br class="sp"><a href="https://www.google.com/maps/place/%EF%BC%88%E6%9C%89%EF%BC%89%E5%A0%80%E6%B1%9F%E8%96%AC%E5%B1%80/@35.3938353,132.6861851,16z/data=!4m8!1m2!2m1!1z5aCA5rGf6Jas5bGA!3m4!1s0x35575c98ecf0c64b:0x9fa67e743b5d2a04!8m2!3d35.393884!4d132.6867018?hl=ja" class="link_basic01" target="_blank">詳しい地図を見る</a></dd>
				</dl>
				<div id="map"></div>
				<dl>
					<dt>電話番号</dt>
					<dd>0853-25-8704</dd>
				</dl>
				<dl>
					<dt>創業</dt>
					<dd>大正十三年</dd>
				</dl>
				<dl>
					<dt>代表取締役</dt>
					<dd>堀江 昭佳</dd>
				</dl>
				<dl>
					<dt>店舗</dt>
					<dd>
						<ul>
							<li>堀江薬局 本店 艸楽<br>〒699-0711　出雲市大社町杵築南1370-2<br>営業時間：9:00&#xFF5E;17:00　定休日：月・火曜日（祝日の場合は営業）<br>TEL：0853-53-2226</li>
							<li>堀江薬房　エル店<br>〒699-0722　出雲市大社町北荒木625-2ショッピングタウン エル内<br>営業時間：10:00&#xFF5E;19:00　定休日：火曜日<br>TEL：0853-53-9039</li>
							<li>堀江薬房　吉祥堂<br>〒690-0058　松江市田和山町122 アダチビル2階<br>営業時間：火&#xFF5E;金曜日10:00&#xFF5E;19:00　土曜日8:30&#xFF5E;17:30　定休日：日・月<br>TEL：0852-33-7559</li>
						</ul>
					</dd>
				</dl>
			<!-- /.table_basic03 --></div>
		<!-- /.inner02 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<script type="text/javascript" src="/common/js/map.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCe-6qErsU9FfCecbcK6rgpd8Et5UyfS4A&callback=initMap1"></script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
