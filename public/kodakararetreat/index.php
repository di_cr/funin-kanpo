<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>堀江薬局の子宝リトリート（セミナー・リトリート）｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="「子宝リトリート」（堀江薬局の滞在型セミナー・相談会）をご紹介します。リトリートとは、日常生活を離れて自分だけの時間や人間関係にひたる場所のこと。たくさんの学びと体験をひとつにした滞在型セミナーです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/kodakararetreat/">
<meta property="og:type" content="article">
<meta property="og:title" content="堀江薬局の子宝リトリート（セミナー・リトリート）｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/ogp.png">
<meta property="og:description" content="「子宝リトリート」（堀江薬局の滞在型セミナー・相談会）をご紹介します。リトリートとは、日常生活を離れて自分だけの時間や人間関係にひたる場所のこと。たくさんの学びと体験をひとつにした滞在型セミナーです。堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/kodakararetreat/">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/slick.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/kodakara.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"name": "堀江薬局の子宝リトリート（セミナー・リトリート）"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body id="g02">
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li>堀江薬局の子宝リトリート（セミナー・リトリート）</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<main id="main" role="main">
		<div class="h1_basic01 inner01 no_border">
			<h1>セミナー・リトリート</h1>
			<span>Seminar ・ Retreat</span>
		<!-- /.h1_basic01 --></div>

		<div class="kodakara_mainimg01">
			<section class="inner01">
				<h2><img src="/common/img/kodakara/txt_mainimg01.png" alt="セミナー・リトリート 治療をがんばることが必要な時期もあるけれど、一度立ち止まって、自分を大切にすることを、考えてみませんか？" width="533" height="493"></h2>
			</section>
		<!-- /.kodakara_mainimg01 --></div>

		<div class="inner02">
			<p>堀江薬局主催の滞在型セミナー「子宝リトリート」を紹介します。リトリートとは、「仕事や家庭 などの日常生活を離れ、自分だけの時間や人間関係にひたる場所」のこと。<br>話を聞くだけでなく、五感で体験することで、すこやかな“こころ”と“からだ”を手に入れる。たくさんの学びと体験をひとつにした滞在型セミナーです。</p>

			<section id="application">
				<h2>開催日時・お申し込み</h2>
				<div class="application_inner01">
					<div class="txt">
						<dl>
							<dt>日程：</dt>
							<dd>2017/08/19（土）&#xFF5E;8/20（日）</dd>
						</dl>
						<dl>
							<dt>場所：</dt>
							<dd>出雲○○集合</dd>
						</dl>
						<dl>
							<dt>定員：</dt>
							<dd>30人</dd>
						</dl>
						<dl>
							<dt>締切：</dt>
							<dd>7/30（日）</dd>
						</dl>
					<!-- /.txt --></div>
					<div class="btn">
						<a href="#"><img src="/common/img/kodakara/btn_detail01.png" alt="詳しくはこちら" width="200" height="50"></a>
					<!-- /.btn --></div>
				<!-- /.application_inner01 --></div>
				<div class="application_inner01">
					<div class="txt">
						<dl>
							<dt>日程：</dt>
							<dd>2017/08/19（土）&#xFF5E;8/20（日）</dd>
						</dl>
						<dl>
							<dt>場所：</dt>
							<dd>出雲○○集合</dd>
						</dl>
						<dl>
							<dt>定員：</dt>
							<dd>30人</dd>
						</dl>
						<dl>
							<dt>締切：</dt>
							<dd>7/30（日）</dd>
						</dl>
					<!-- /.txt --></div>
					<div class="btn">
						<a href="#"><img src="/common/img/kodakara/btn_detail01.png" alt="詳しくはこちら" width="200" height="50"></a>
					<!-- /.btn --></div>
				<!-- /.application_inner01 --></div>
				<p class="taC">※申込先着順で定員になり次第締め切らせていただきます。</p>
			</section>

			<div class="anchor01">
				<ul>
					<li><a href="#reason">3つの理由</a></li>
					<li><a href="#program">プログラム</a></li>
					<li><a href="#instructor">講師</a></li>
					<li><a href="#voice">参加者の声</a></li>
				</ul>
			<!-- /.anchor01 --></div>

			<section id="reason">
				<div class="h2_basic01">
					<h2>子宝リトリートをすすめる3つの理由</h2>
					<span>Three Reasons</span>
				<!-- /.h2_basic01 --></div>
				<section class="reason_inner01">
					<div class="tit">
						<h3><span>1.</span>非日常的な空間で<br class="pc">本来の自分を解放させる</h3>
					<!-- /.tit --></div>
					<p>薬膳教室をしたり、ヨガをしたり、漢方教室をしたり、セミナーなどを開催していましたが、帰り際になるとどうしてもソワソワと時間が気になってしまいます。仕事や家庭から離れた場所だからこそ、“こころ”もリラックスしながら集中して学んでいただけるものだと思っています。</p>
				</section>
				<section class="reason_inner02">
					<div class="tit">
						<h3><span>2.</span>自ら行動を起こす</h3>
					<!-- /.tit --></div>
					<p>電話だけで相談される方と遠方からご来店される方とでは、妊娠率が三倍も違ってくることが判りました。忙しい中で、なんとか時間をやりくりしたり、お金も掛かることでしょう。でも、積極的な行動によって、今までの生活を見直すきっかけになった方。普段なかなか向き合って話せない「不妊治療」について話し合われる方。旅をきっかけに二人でこどもを授かるということについて、夢を語られる方。日常を飛び出して旅することで、“こころ”が軽くなられる方。一歩が、大きな波を起こしているように思えます。</p>
				</section>
				<section class="reason_inner03">
					<div class="tit">
						<h3><span>3.</span>友達には話しにくいことも<br class="pc">ここなら言える</h3>
					<!-- /.tit --></div>
					<p>不妊の問題は、長期に渡りやすく、そして、話しにくいですよね。仲がいい人には逆に言いにくいし、友だちにもこどもが居たり、独身だったりすると、そもそも話が噛み合わなかったり。<br>同じ不妊の悩みを持つもの同士で仲良くしていると、自分が妊娠した時、あるいは相手が妊娠した時、すなおに自分の妊娠を伝えられなかったり、相手の妊娠を祝福できなかったりしてしまう…<br>リトリートには全国から参加されます。お互い全然知らないもの同士です。<br>お互い知らない者同士だからこそ、普段言えなくて“こころ”の中にしまいこんでいたことも口にできてしまったり、それだけで楽になったり。<br>初めて会って、一緒にごはんを食べて、寝て起きて、そしてまた別れる。<br>たった二日間だけど、こんなに参加される方の表情が変わるんだ！ということに、いつもびっくりします。</p>
				</section>
				<div class="txt_important01">
					<p>でも、大変だからこそ、お越しください。<br>“こころ”も“からだ”も変わる、<br class="pc">縁結び子宝の旅へお越しいただきたいのです。</p>
				<!-- /.txt_important01 --></div>
			</section>
		<!-- /.inner02 --></div>

		<section id="program">
			<div class="inner02">
				<div class="h2_basic01">
					<h2>子宝リトリートの内容</h2>
					<span>Program</span>
				<!-- /.h2_basic01 --></div>
				<p>過去のセミナーをもとにした一例です。予定なく行程が変更となる場合があります。</p>
				<section class="example01">
					<h3>プログラム一例</h3>
					<ol>
						<li>自分の“こころ”と“からだ”の漢方のタイプを知る</li>
						<li>“こころ”と“からだ”のワークショップ</li>
						<li>マクロビ＆薬膳キッチン</li>
						<li>漢方ヨガ</li>
						<li>ゆるゆる温泉＆すっきりプチ断食</li>
					</ol>
					<p class="taR">…など</p>
				</section>
			<!-- /.inner02 --></div>
			<div class="program_slider">
				<div><img src="/common/img/kodakara/01.jpg" alt="子宝リトリートの写真1" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/02.jpg" alt="子宝リトリートの写真2" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/03.jpg" alt="子宝リトリートの写真3" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/04.jpg" alt="子宝リトリートの写真4" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/05.jpg" alt="子宝リトリートの写真5" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/06.jpg" alt="子宝リトリートの写真6" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/07.jpg" alt="子宝リトリートの写真7" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/08.jpg" alt="子宝リトリートの写真8" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/09.jpg" alt="子宝リトリートの写真9" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/10.jpg" alt="子宝リトリートの写真10" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/11.jpg" alt="子宝リトリートの写真11" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/12.jpg" alt="子宝リトリートの写真12" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/13.jpg" alt="子宝リトリートの写真13" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/14.jpg" alt="子宝リトリートの写真14" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/15.jpg" alt="子宝リトリートの写真15" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/16.jpg" alt="子宝リトリートの写真16" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/17.jpg" alt="子宝リトリートの写真17" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/18.jpg" alt="子宝リトリートの写真18" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/19.jpg" alt="子宝リトリートの写真19" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/20.jpg" alt="子宝リトリートの写真20" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/21.jpg" alt="子宝リトリートの写真21" width="600" height="400"></div>
				<div><img src="/common/img/kodakara/22.jpg" alt="子宝リトリートの写真22" width="600" height="400"></div>
			<!-- /.program_slider --></div>
		<!-- /#program --></section>

		<div class="inner02">
			<section id="instructor">
				<div class="h2_basic01">
					<h2>講師</h2>
					<span>Instructor</span>
				<!-- /.h2_basic01 --></div>
				<div class="instructor_inner01">
					<div class="img">
						<img src="/common/img/kodakara/img_horie01.png" alt="堀江 昭佳" width="373" height="360">
					<!-- /.img --></div>
					<div class="txt">
						<dl>
							<dt>堀江 昭佳<span>ほりえ あきよし</span></dt>
							<dd>薬剤師、認定不妊カウンセラー。これまでに<?php echo $report_num; ?>人以上を妊娠に導いてきたのは、“こころ”と“からだ”を総合的にみつめてきたアプローチだから。リトリートでは全体のコーディネート、漢方の座学、“こころ”のワークショップを担当します。五感でその真髄を体験してください。</dd>
						</dl>
					<!-- /.txt --></div>
				<!-- /.instructor_inner01 --></div>
				<div class="instructor_inner02">
					<div class="img">
						<img src="/common/img/kodakara/img_mina01.png" alt="皆 みなみ" width="208" height="214">
					<!-- /.img --></div>
					<div class="txt">
						<dl>
							<dt>皆 みなみ<span>みな みなみ</span></dt>
							<dd>心屋流カウンセラーティーセラピスト。<br>今回は、心屋式「言ってみるカウンセリング」で、“こころ”からのアプローチ、ふわんと軽くなる体験をしてみてくださいね。</dd>
						</dl>
					<!-- /.txt --></div>
				<!-- /.instructor_inner02 --></div>
				<div class="instructor_inner02">
					<div class="img">
						<img src="/common/img/kodakara/img_takasuga01.png" alt="高須賀 千江子" width="208" height="213">
					<!-- /.img --></div>
					<div class="txt">
						<dl>
							<dt>高須賀 千江子<span>たかすが ちえこ</span></dt>
							<dd>ヨガのスペシャリスト。 定期的にインドに渡り、パワーアップ。 とってもゆるゆるのヨガと瞑想で、心の底から楽になるお手伝いをします。また、出雲大社早朝参拝のガイドも務めます。</dd>
						</dl>
					<!-- /.txt --></div>
				<!-- /.instructor_inner02 --></div>
				<div class="instructor_inner02">
					<div class="img">
						<img src="/common/img/kodakara/img_taninoue01.png" alt="谷ノ上 朋美" width="208" height="207">
					<!-- /.img --></div>
					<div class="txt">
						<dl>
							<dt>谷ノ上 朋美<span>たにのうえ ともみ</span></dt>
							<dd>看護師、認定不妊カウンセラー。堀江薬局で、子宝相談を担当しています。クライアントさんからは、気持ちが明るくなると大きな人気を集めています！今回のリトリートでは、滞在中のみなさんのさまざまなお手伝いをします。</dd>
						</dl>
					<!-- /.txt --></div>
				<!-- /.instructor_inner02 --></div>
				<div class="instructor_inner02">
					<div class="img">
						<img src="/common/img/kodakara/img_sonoyama01.png" alt="園山 直子" width="208" height="207">
					<!-- /.img --></div>
					<div class="txt">
						<dl>
							<dt>園山 直子<span>そのやま なおこ</span></dt>
							<dd>マクロビ・ 薬膳セラピスト。料理の技術や知識ではなく、生きる基本である食を「ていねいに食事をすること」を通じて、“こころ”と“からだ”をみつめます。シンプルな食事を作り、食べるだけなのに、“こころ”の平安が訪れる不思議。マクロビ薬膳の料理実習講座を担当します。</dd>
						</dl>
					<!-- /.txt --></div>
				<!-- /.instructor_inner02 --></div>
				<div class="instructor_inner02">
					<div class="img">
						<img src="/common/img/kodakara/img_iwanari01.png" alt="岩成 桜" width="208" height="213">
					<!-- /.img --></div>
					<div class="txt">
						<dl>
							<dt>岩成 桜<span>いわなり さくら</span></dt>
							<dd>リラクセーションスペースゆるり、オーナーセラピスト。女性の身体と心をゆるめる専門家。自分を大切にいたわるアロマ・セルフケアの講座を担当します。</dd>
						</dl>
					<!-- /.txt --></div>
				<!-- /.instructor_inner02 --></div>
				<div class="instructor_inner02">
					<div class="img">
						<img src="/common/img/kodakara/img_kobayashi01.png" alt="小林 わこ" width="208" height="213">
					<!-- /.img --></div>
					<div class="txt">
						<dl>
							<dt>小林 わこ<span>こばやし わこ</span></dt>
							<dd>心屋リセットカウンセラー、ティーセラピスト。魔法の言葉とイメージワークでここことからだを緩めます。ゆったりと優しい時間となるような、グループカウンセリングを担当します。</dd>
						</dl>
					<!-- /.txt --></div>
				<!-- /.instructor_inner02 --></div>
			</section>

			<section id="voice">
				<div class="h2_basic01">
					<h2>参加者の声</h2>
					<span>Voice</span>
				<!-- /.h2_basic01 --></div>
				<div class="box_notebook02">
					<div class="notebook_inner01">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
						<p class="taR">40代・女性</p>
					<!-- /.notebook_inner01 --></div>
				<!-- /.box_notebook02 --></div>
				<div class="box_notebook02">
					<div class="notebook_inner01">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
						<p class="taR">40代・女性</p>
					<!-- /.notebook_inner01 --></div>
				<!-- /.box_notebook02 --></div>
				<div class="box_notebook02">
					<div class="notebook_inner01">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
						<p class="taR">40代・女性</p>
					<!-- /.notebook_inner01 --></div>
				<!-- /.box_notebook02 --></div>
			</section>
		<!-- /.inner02 --></div>

		<div id="cv_fix" class="cv_fix_kodakara">
			<div class="cv_fix_inner01">
				<div class="txt">
					<img src="/common/img/kodakara/cv_txt01.png" alt="学びと体験をひとつに セミナー「子宝リトリート」" width="318" height="53">
				<!-- /.txt --></div>
				<div class="btn">
					<a href="#application"><img src="/common/img/kodakara/cv_btn01.png" alt="開催日時・お申し込みはこちら" width="300" height="54"></a>
				<!-- /.img --></div>
			<!-- /.cv_fix_inner01 --></div>
		<!-- /.cv_fix01 --></div>
	</main>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<script type="text/javascript" src="/common/js/slick.min.js"></script>
<script type="text/javascript" src="/common/js/slider_setting.js"></script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
