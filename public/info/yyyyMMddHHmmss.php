<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article#">
<meta charset="utf-8">
<title>『血流』＆『ほめ達』コラボ講演会開催決定！｜不妊漢方 子宝リトリート 堀江薬局</title>
<meta name="description" content="堀江薬局からのお知らせ「{記事タイトル名}」のページです。{記事・リード文70字程度}堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta name="keywords" content="">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta property="og:url" content="http://www.funin-kanpo.com/horie/news/yyyyMMddHHmmss.php">
<meta property="og:type" content="article">
<meta property="og:title" content="『血流』＆『ほめ達』コラボ講演会開催決定！｜不妊漢方 子宝リトリート 堀江薬局">
<meta property="og:image" content="http://www.funin-kanpo.com/common/img/upload/img_post_mainimg.png">
<meta property="og:description" content="堀江薬局からのお知らせ「{記事タイトル名}」のページです。{記事・リード文70字程度}堀江昭佳オフィシャルサイト「縁結び出雲 不妊漢方 子宝リトリート」です。">
<meta property="og:site_name" content="縁結び出雲 不妊漢方 子宝リトリート｜堀江昭佳オフィシャルサイト">
<link rel="canonical" href="http://www.funin-kanpo.com/news/yyyyMMddHHmmss.php">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="mask-icon" href="/mask-icon.png" color="#000000">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<link rel="stylesheet" type="text/css" href="/common/css/import.css" media="all">
<link rel="stylesheet" type="text/css" href="/common/css/detail.css" media="all">
<script type="application/ld+json">
[
	{
		"@context": "http://schema.org",
		"@type": "Organization",
		"url": "http://www.funin-kanpo.com/",
		"logo": "http://www.funin-kanpo.com/common/img/logo01.svg"
	},
	{
		"@context": "http://schema.org",
		"@type": "BreadcrumbList",
		"itemListElement":
		[
			{
				"@type": "ListItem",
				"position": 1,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/",
					"name": "ホーム"
				}
			},
			{
				"@type": "ListItem",
				"position": 2,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/topics/",
					"name": "トピックス一覧"
				}
			},
			{
				"@type": "ListItem",
				"position": 3,
				"item":
				{
					"@id": "http://www.funin-kanpo.com/info/",
					"name": "お知らせ"
				}
			},
			{
				"@type": "ListItem",
				"position": 4,
				"item":
				{
					"name": "『血流』＆『ほめ達』コラボ講演会開催決定！"
				}
			}
		]
	}
]
</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/head_analytics.php'); ?>
</head>

<body>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/header.php'); ?>

<div id="wrapper">
	<div id="breadcrumb">
		<ol>
			<li class="home"><a href="/">トップ</a></li>
			<li><a href="/topics/">トピックス一覧</a></li>
			<li><a href="/info/">お知らせ</a></li>
			<li>『血流』＆『ほめ達』コラボ講演会開催決定！</li>
		</ol>
	<!-- /#breadcrumb --></div>

	<div class="inner01 two_column">
		<div id="contents" class="post_wrap01">
			<main id="main" role="main">
				<div class="h1_basic02">
					<h1>『血流』＆『ほめ達』コラボ講演会開催決定！</h1>
				<!-- /.h1_basic02 --></div>
				<div class="post_mainimg">
					<img src="/common/img/upload/img_post_mainimg.png" alt="" width="720" height="400">
				<!-- /.post_mainimg --></div>
				<div class="post_header">
					<div class="post_header_meta">
						<div class="date"><time datetime="2017-07-15">2017.07.15</time></div>
						<ul class="cat">
							<li><div class="cat_label label_info">お知らせ</div></li>
							<li>他カテゴリ</li>
						</ul>
					<!-- /.post_header_meta --></div>
					<div class="post_header_sns">
						<ul>
							<li class="line"><div class="line-it-button" style="display: none;" data-lang="ja" data-type="share-a" data-url="★★★記事URL★★★"></div></li>
							<li class="fb"><div class="fb-like" data-href="★★★記事URL★★★" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div></li>
							<li class="tw"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="ja">ツイート</a></li>
							<li class="copy"><div class="copy_trigger" data-clipboard-text="★★★記事タイトル★★★｜不妊漢方 子宝リトリート 堀江薬局　★★★記事URL★★★">リンクをコピー</div></li>
						</ul>
					<!-- /.post_header_sns --></div>
				<!-- /.post_header --></div>
				<div class="post_contents">
					<div class="box_basic01">
						<section>
							<h2>ストレスをチカラに変える。「からだ」と「心」が軽くなる『血流』＆『ほめ達』コラボ講演会』</h2>
							<div class="table_basic02">
								<dl>
									<dt>日時</dt>
									<dd>2017年7月5日（土）14時〜</dd>
								</dl>
								<dl>
									<dt>会場</dt>
									<dd>大阪・梅田　ブリーゼプラザ７階小ホール</dd>
								</dl>
								<dl>
									<dt>参加費</dt>
									<dd>一般　4,500円<br>早割　4,000円（5月31日までのお申込みの方）</dd>
								</dl>
								<dl>
									<dt>お申込み・詳細</dt>
									<dd>
										<div><a href="#" target="_blank"><img src="/common/img/upload/bnr_post.png" alt="" width="300" height="60"></a></div>
										<div><a href="#" target="_blank">『血流』＆『ほめ達』コラボ講演会申し込みサイト</a></div>
									</dd>
								</dl>
								<dl>
									<dt>主催・お問い合わせ</dt>
									<dd>一般社団法人　日本ほめる達人協会<br>電話06-6539-1950</dd>
								</dl>
							<!-- /.table_basic02 --></div>
						</section>
					<!-- /.box_basic01 --></div>
					<section>
						<h2>H2　講演プログラム</h2>
						<p>テキストテンプレートの用意。普通のボールドはこちら「<b>ボールド</b>」。<br>強調：ポジティブ2案。<span class="yellow">ポジティプな内容1</span>にオレンジを使用します。<span class="red">ポジティプな内容2</span>にこちらを使用します。<br>強調：ネガティブ1案。<span class="gray">ネガティブな内容</span>にこちらを使用します。</p>
						<section>
							<h3>H3　【第1部】堀江 昭佳 講演会</h3>
							<section>
								<h4>h4　テーマ：「血流」はすべてを解決する</h4>
								<p><b>（内　容）</b><br>体を整えることで、健康を手に入れるだけでなく、自分の心も解放できます。
これまで「血流」というと、血液サラサラばかりに注目が集まってきました。<br>しかし漢方 的視点で見ると、女性の血流が悪い原因の多くは、そもそも「血の不足」にあります。 血流改善のために、血液サラサラにしても効果が実感できないのは、流れる血そのものが 足りていないためなのです。</p>
								<p>そして、不妊、うつ、自律神経失調、ダイエットといった病気や症状はもちろん、恋人ができない、友達ができない、マイナス思考、嫉妬深いなどの心の悩みも、原因は「血流」 が深く関係しています。<br>西洋医学的に、貧血や血流と心理の関わりが実証されているのはもちろん。<br>漢方的にも血 流を悪くする体質が3つあり、その体質により無気力、不安、イライラという負の感情が 湧き出ます。</p>
								<p>血流という体の問題で、心が乱されているのです。</p>
								<p>出来事や事実自体が悩みなのではありません。思い込みや固定観念によって、出来事や事 実を「問題」と捉える感情こそが悩みを引き起こしています。<br>体を整えることで、思い込みや固定観念を乗り越えることができ、心を解放しやすくなるのです。<br>そして、心が整うことで、夢や目標もぐっと現実化していきます。</p>
								<p>単なる理論ではなく、4千年の伝統を持つ東洋医学と、実際にこれまでの5万件のカウンセリングの中で実践してきた再現性の高いものです。</p>
								<p>心と体は本来ひとつのもの。<br>やる気がない、不安、イライラ・・・そういった心の悩みもあなたが悪いわけではありません。<br>あなたの体が整っていないだけなのです。</p>
								<p>ぜひ、体から心を整えて、本当の自分らしさを手に入れていってください。</p>
							</section>
						</section>
						<section>
							<h3>H3　【第2部】西村 貴好 講演会</h3>
							<section>
								<h4>h4　テーマ：「ほめ達！」心に効く漢方薬</h4>
								<p><b>（内　容）</b><br>「ほめる」ことに即効性を感じる人、そうでない人。<br>その実感は様々ですが、「ほめる」ことを他人のコントロールには使えません。<br>「ほめる」ことにより、自分の心が整いだすのです。</p>
								<p>自分の心が整うことで相手との関係性が変わりだす。<br>その効果は、まさに漢方薬のようです。</p>
								<p>すぐにできる「ほめ達！」の実践を学び「心の体質改善」してみませんか。<br>心がホッコリ温まり、あなたの魅力が内面からにじみ出るようになります。</p>
							</section>
						</section>
						<section>
							<h3>H3　【第3部】対談</h3>
							<p>心と体のつながりと整え方をそれぞれの専門家がコラボして、化学反応を起こすように知識と知恵を引き出し合います。</p>
							<blockquote>
								<p>心と体のつながりと整え方をそれぞれの専門家がコラボして、化学反応を起こすように知識と知恵を引き出し合います。</p>
							</blockquote>
						</section>
						<section>
							<h3>H3　【第4部】懇親会</h3>
							<p>講演会後に懇親会を行います。</p>
							<dl>
								<dt>リスト見出し（テンプレート）</dt>
								<dd>
									<ul>
										<li>リスト１リスト１リスト１リスト１</li>
										<li>リスト2リスト2リスト2リスト2</li>
										<li>リスト3リスト3リスト3リスト3リスト3</li>
									</ul>
								</dd>
							</dl>
							<ul>
								<li>見出しなしリスト1見出しなしリスト1</li>
								<li>見出しなしリスト2見出しなしリスト2</li>
								<li>見出しなしリスト3見出しなしリスト3</li>
							</ul>
						</section>
					</section>

				<!-- /.post_contents --></div>
				<div class="post_footer">
					<ul class="post_footer_sns">
						<li class="line"><a href="https://timeline.line.me/social-plugin/share?url=http://xlowolx.com/api/201609/289" onclick="window.open(this.href, 'LINEwindow', 'width=650, height=450,personalbar=0,toolbar=0,scrollbars=1,resizable=1'); return false;">LINEで共有</a></li>
						<li class="tw"><a href="https://twitter.com/intent/tweet?text=★★★記事タイトル★★★｜不妊漢方 子宝リトリート 堀江薬局&url=★★★記事URL★★★" target="_blank">Twitterでつぶやく</a></li>
						<li class="fb"><a href="http://www.facebook.com/sharer/sharer.php?u=★★★記事URL★★★" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">Facebookでシェア</a></li>
						<li class="copy"><div class="copy_trigger" data-clipboard-text="★★★記事タイトル★★★｜不妊漢方 子宝リトリート 堀江薬局　★★★記事URL★★★">リンクをコピー</div></li>
					<!-- /.post_footer_sns --></ul>
				<!-- /.post_footer --></div>

				<aside id="related">
					<div class="h2_basic03">
						<h2>おすすめの記事</h2>
					<!-- /.h2_basic03 --></div>
					<div class="topics_wrap01">
						<div class="post_items01 col_3">
							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post01.png" alt="">
									<span class="label_new"><img src="/common/img/label_new01.png" alt="NEW"></span>
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_column">コラム</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">月のリズムと生理周期。月のリズムと生理周期。月のリズムと生…</h3>
								<div class="post_txt">月と女性、そして生理のリズムには不思議な関係があります。日々のくらしの中で新月や満…</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post02.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_experiences">体験談</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
								<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post03.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_media">メディア掲載</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">LEE 2017．1月号掲載</h3>
								<div class="post_txt">この不調、貧血ですか？〜くすみ、下半身太り、疲れやすい〜6ページに渡って、医師の方との特…</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post04.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_info">お知らせ</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">『血流』＆『ほめ達』コラボ講演会開催…</h3>
								<div class="post_txt">ストレスをチカラに変える。「からだ」と「心」が軽くなる『血流』＆『ほめ達』コラボ講…</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post05.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_faq">よくある質問</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">何度も治療してるのに成功しないんです・・・</h3>
								<div class="post_txt">実は不妊治療は、決して成功率の高い治療ではありません。一般に人工授精で１０％、体外…</div>
							</a>
							<!-- post_item01 --></div>

							<div class="post_item01">
							<a href="#">
								<div class="post_thumb">
									<img src="/common/img/upload/img_post01.png" alt="">
								<!-- /.post_thumb --></div>
								<div class="post_meta">
									<div class="cat_label label_column">コラム</div>
									<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
								<!-- /.post_meta --></div>
								<h3 class="post_tit">月のリズムと生理周期。月のリズムと生理周期。月のリズムと生…</h3>
								<div class="post_txt">月と女性、そして生理のリズムには不思議な関係があります。日々のくらしの中で新月や満…</div>
							</a>
							<!-- post_item01 --></div>
						<!-- /.post_items01 --></div>
					<!-- /.topics_wrap01 --></div>
				<!-- /#related --></aside>
			</main>
		<!-- /#contents --></div>
		<div id="side">
			<aside class="side_popular01">
				<h2>人気の記事</h2>
				<div class="topics_wrap01">
					<div class="post_items01">
						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post01.png" alt="">
								<span class="label_new"><img src="/common/img/label_new01.png" alt="NEW"></span>
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_column">コラム</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">月のリズムと生理周期。月のリズムと生理周期。月のリズムと生…</h3>
							<div class="post_txt">月と女性、そして生理のリズムには不思議な関係があります。日々のくらしの中で新月や満…</div>
						</a>
						<!-- post_item01 --></div>

						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post02.png" alt="">
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_experiences">体験談</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">赤ちゃんを抱っこできて、本当にうれしい！</h3>
							<div class="post_txt">以前の私は、不規則な生活・残業ばかりの仕事で疲労困憊の毎日でした。</div>
						</a>
						<!-- post_item01 --></div>

						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post03.png" alt="">
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_media">メディア掲載</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">LEE 2017．1月号掲載</h3>
							<div class="post_txt">この不調、貧血ですか？〜くすみ、下半身太り、疲れやすい〜6ページに渡って、医師の方との特…</div>
						</a>
						<!-- post_item01 --></div>

						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post04.png" alt="">
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_info">お知らせ</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">『血流』＆『ほめ達』コラボ講演会開催…</h3>
							<div class="post_txt">ストレスをチカラに変える。「からだ」と「心」が軽くなる『血流』＆『ほめ達』コラボ講…</div>
						</a>
						<!-- post_item01 --></div>

						<div class="post_item01">
						<a href="#">
							<div class="post_thumb">
								<img src="/common/img/upload/img_post05.png" alt="">
							<!-- /.post_thumb --></div>
							<div class="post_meta">
								<div class="cat_label label_faq">よくある質問</div>
								<div class="date"><time datetime="2017-04-01">2017.04.01</time></div>
							<!-- /.post_meta --></div>
							<h3 class="post_tit">何度も治療してるのに成功しないんです・・・</h3>
							<div class="post_txt">実は不妊治療は、決して成功率の高い治療ではありません。一般に人工授精で１０％、体外…</div>
						</a>
						<!-- post_item01 --></div>
					<!-- /.post_items01 --></div>
				<!-- /.topics_wrap01 --></div>
			</aside>

			<aside class="side_list01">
				<h2>トピックス一覧</h2>
				<ul>
					<li><a href="#">コラム</a></li>
					<li class="current"><a href="/info/">お知らせ</a></li>
					<li><a href="#">メディア掲載</a></li>
					<li><a href="#">体験談</a></li>
					<li><a href="#">よくある質問</a></li>
				</ul>
			</aside>
		<!-- /#side --></div>
	<!-- /.inner01 --></div>
<!-- /#wrapper --></div>

<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/footer.php'); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/js.php'); ?>
<!-- LINEで送るボタン -->
<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
<!-- Tweetボタン -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'/common/inc/foot_analytics.php'); ?>
</body>
</html>
